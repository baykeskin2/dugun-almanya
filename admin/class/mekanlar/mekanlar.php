<?php
class Modul
{
    public $tablo ="mekanlar";
    public $primary ="id";
    public $alanlar = array("kategori_id","sehir_id","baslik","lokasyon","kapasite","aciklama");
    public $ekstraalanlar = array("videolar","iletisim_bilgileri","resimler","genel_ozellikler","vitrin","fiyatlandirma","kayit_tarihi","url");
    public $requnedalanlar = array();
    public $ekstraalankarsilik = array();
    public $mesaj ="Mekan";
    public $yonlendir ="../../pages/mekanlar/mekanlar.php";
    public $breadcrumb=array(admin_breadcrumb_anasayfa,admin_breadcrumb_mekanlar,admin_breadcrumb_mekanlar_duzenle,admin_breadcrumb_mekanlar_ekle);
    public $pagetitle =admin_breadcrumb_mekanlar;
    public $pagealtitle =array(admin_breadcrumb_mekanlar,admin_breadcrumb_mekanlar_duzenle,admin_breadcrumb_mekanlar_ekle);
    function __construct()
    {
        global $db;
        global $yetkiler;
        if(!isset($yetkiler["mekanlar.php"]))
        {
            ?>
            <script>
                setTimeout(function() { document.location = '../../pages/editor-sayfasi/editor-sayfasi'; }, 0)
            </script>
            <?php
            exit();
        }
    }

    function resimYukle($resim,$genislik,$yukseklik)
    {
        $image = new Upload($resim);
        if ($image->uploaded)
        {
            $yenisim = rand(0,9999999);
            $image->file_new_name_body = $yenisim;
            $image->image_resize = true;
            $image->image_x = $genislik;
            $image->image_y = $yukseklik;
            $image->allowed = array ("image/*");
            $image->Process(realpath('../../../')."/images");
            if ($image->processed)
            {
                return "images/".$image->file_dst_name;
            }else
            {
                return false;
            }
        }

    }
    function All()
    {
        global $db;
        return $db->VeriOkuCoklu($this->tablo,array(),array(),"",$this->primary,"DESC","");
    }

    function Update()
    {
        global $db;
        $required = $this->requnedalanlar;
        $error = true;
        foreach($required as $field)
        {
            if (empty($_POST[trim($field)]))
            {
                $error =false;
            }
        }
        if($error==false)
        {
            $db->Basarisiz("Lütfen tüm alanları doldurunuz.");
        }else
        {

            $dosya_sayi=count($_FILES['resimler']['name']);

            if($dosya_sayi>0)
            {
                $resimler = array();
                foreach ($_FILES['resimler'] as $k => $l)
                {

                    foreach ($l as $i => $v)
                    {
                        if (!array_key_exists($i, $resimler))
                            $resimler[$i] = array();
                        $resimler[$i][$k] = $v;
                    }
                }
                $adresler = array();
                foreach($resimler as $resim)
                {
                    if($resim['name']!='')
                    {
                        $yeniadres = $this->resimYukle($resim,685,455);
                        $adresler[]=$yeniadres;
                    }

                }
            }


            $this->ekstraalankarsilik[]=json_encode($_POST["video"]);
            $iletisim_bilgileri = array('telefon'=>$_POST["telefon"],'adres'=>$_POST["adres"],'e_mail'=>$_POST["e_mail"],'harita'=>$_POST["e_mail"]);

            $this->ekstraalankarsilik[]=json_encode($iletisim_bilgileri);

            if(count($adresler)>0)
            {
                $this->ekstraalankarsilik[]=$db->VeriOkuTek($this->tablo,"resimler",$this->primary,$_GET["id"]).",".implode(",",$adresler);
            }else
            {
                $this->ekstraalankarsilik[]=$db->VeriOkuTek($this->tablo,"resimler",$this->primary,$_GET["id"]);
            }

            $this->ekstraalankarsilik[]=json_encode($_POST["ozellik"]);
            $this->ekstraalankarsilik[]=$_POST["vitrin"];
            $fiyat_i = 0;
            $fiyat_array=array();
            foreach($_POST["fiyat_baslik"] as $fb)
            {
                $fiyat_array[$fiyat_i]['baslik'] = $fb;
                $fiyat_array[$fiyat_i]['fiyat'] = $_POST["fiyat_baslik_fiyat"];
                $fiyat_i++;
            }
            $this->ekstraalankarsilik[]=json_encode($fiyat_array);
            $this->ekstraalankarsilik[]=$db->VeriOkuTek($this->tablo,"kayit_tarihi",$this->primary,$_GET["id"]);
            $this->ekstraalankarsilik[]=$db->urlYap($_POST["baslik"]);
            if($db->veriGuncelleParametresiz($this->tablo,$this->alanlar,$this->primary,$_GET["id"],$this->ekstraalanlar,$this->ekstraalankarsilik)>0)
            {

                $db->Basarili(admin_islem_basarili);

                $resimler = explode(",",$db->VeriOkuTek("mekanlar","resimler","id",$_GET["id"]));
                $yeniresimler =array();
                foreach($resimler as $r)
                {
                    if($r!='')
                    {
                        $yeniresimler[]=$r;
                    }
                }
            }else
            {
                $db->Basarisiz(admin_islem_basarisiz);
            }
        }
    }
    function Insert()
    {
        global $db;
        $required = $this->requnedalanlar;
        $error = true;
        foreach($required as $field)
        {
            if (empty($_POST[trim($field)]))
            {
                echo $field;
                $error =false;
            }
        }
        if($error==false)
        {
            $db->Basarisiz("Lütfen tüm alanları doldurunuz.");
        }else
        {
            $kacsoruisareti = count($this->alanlar)+count ($this->ekstraalanlar);
            $isaretler = array("NULL");
            for ($i =0; $i<$kacsoruisareti; $i++)
            {
                $isaretler[]="?";
            }
            $alankarsilik = array();
            foreach ($this->alanlar as $a)
            {
                $alankarsilik[]=$_POST[$a];
            }

            $dosya_sayi=count($_FILES['resimler']['name']);

            if($dosya_sayi>0)
            {
                $resimler = array();
                foreach ($_FILES['resimler'] as $k => $l)
                {

                    foreach ($l as $i => $v)
                    {
                        if (!array_key_exists($i, $resimler))
                            $resimler[$i] = array();
                        $resimler[$i][$k] = $v;
                    }
                }
                $adresler = array();
                foreach($resimler as $resim)
                {
                    if($resim['name']!='')
                    {
                        $yeniadres = $this->resimYukle($resim,685,455);
                        $adresler[]=$yeniadres;
                    }

                }
            }


            $alankarsilik[]=json_encode($_POST["video"]);
            $iletisim_bilgileri = array('telefon'=>$_POST["telefon"],'adres'=>$_POST["adres"],'e_mail'=>$_POST["e_mail"],'harita'=>$_POST["e_mail"]);

            $alankarsilik[]=json_encode($iletisim_bilgileri);

            if(count($adresler)>0)
            {
                $alankarsilik[]=implode(",",$adresler);
            }else
            {
                $alankarsilik[]='';
            }

            $alankarsilik[]=json_encode($_POST["ozellik"]);
            $alankarsilik[]=json_encode($_POST["ozellik"]);
            $alankarsilik[]=$_POST["vitrin"];

            $fiyat_i = 0;
            $fiyat_array=array();
            foreach($_POST["fiyat_baslik"] as $fb)
            {
                $fiyat_array[$fiyat_i]['baslik'] = $fb;
                $fiyat_array[$fiyat_i]['fiyat'] = $_POST["fiyat_baslik_fiyat"];
                $fiyat_i++;
            }
            $alankarsilik[]=json_encode($fiyat_array);


            $alankarsilik[]=date("Y-m-d");

            $alankarsilik[]=$db->urlYap($_POST["baslik"]);
            if($db->veriEkle($this->tablo,$isaretler,$alankarsilik)>0)
            {

                $db->Basarili(admin_islem_basarili);
            }else
            {
                $db->Basarisiz(admin_islem_basarisiz);
            }

        }

    }
    function Delete()
    {

        global $db;
        global $admin;
        $db->veriSil($this->tablo,array($this->primary),array($_GET["id"]),"=");


        echo "<script>setTimeout(function() { document.location = $this->yonlendir; }, 0)</script>";
    }
    function Delete_All()
    {
        global $db;
        global $admin;
        foreach($_POST["SilID"] as $id)
        {
            $db->veriSil($this->tablo,array($this->primary),array($id),"=");
        }
        echo "<script>setTimeout(function() { document.location = $this->yonlendir;}, 0)</script>";

    }

    function resimSil()
    {
        global $db;
        global $admin;
        $resimler = $db->VeriOkuTek("mekanlar","resimler","id",$_GET["id"]);
        $yeniresimler =explode(",",str_replace(array($_GET["resim"].",",$_GET["resim"]),array("",""),$resimler));
        $guncellenecekresimnler=array();
        foreach($yeniresimler as $y)
        {
            if($y!='')
            {
                $guncellenecekresimnler[]=$y;
            }
        }



        $db->veriGuncelle("mekanlar",array("resimler"),array(implode(",",$guncellenecekresimnler),$_GET["id"]),"id");
    }

}
?>
