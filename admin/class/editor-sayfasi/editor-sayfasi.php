<?php
class Modul
{
    public $tablo ="ayarlar";
    public $primary ="id";
    public $alanlar = array("sitebaslik","siteadresi","dizin","mail","telefon","adres","facebook","twitter","instagram","tiktok","youtube","linkedin","headerkod","footerkod");
    public $ekstraalanlar = array("logo","mobillogo","favicon");
    public $requnedalanlar = array();
    public $ekstraalankarsilik = array();
    public $mesaj ="Editör Sayfası";
    public $yonlendir ="../../pages/editor-sayfasi/pages/editor-sayfasi.php";
    public $breadcrumb=array(admin_breadcrumb_anasayfa,'Editör Sayfası','Editör Sayfası Düzenle');
    public $pagetitle ="Editör Sayfası";
    public $pagealtitle =array("Editör Sayfası","Editör Sayfası Düzenle","Editör Sayfası Ekle");


    function All()
    {

        global $db;
        if($db->veriSaydir($this->tablo)<1)
        {

            return false;
        }else
        {
            echo $db->VeriOkuCoklu($this->tablo,array(),array(),"",$this->primary,"DESC","");
            return true;
        }
    }

    function Update()
    {
        global $db;
        $required = $this->requnedalanlar;
        $error = true;
        foreach($required as $field)
        {
            if (empty($_POST[trim($field)]))
            {
                $error =false;
                echo $field;
            }
        }
        if($error==false)
        {
            $db->Basarisiz("Lütfen tüm alanları doldurunuz.");
        }else
        {

            if(!empty($_FILES["logo"]['name']))
            {
                $this->ekstraalankarsilik[] = $this->resimYukle($_FILES['logo'],380,136);
            }else
            {
                $this->ekstraalankarsilik[] = $db->VeriOkuTek($this->tablo,"logo",$this->primary,1);
            }
            if(!empty($_FILES["mobillogo"]['name']))
            {
                $this->ekstraalankarsilik[] = $this->resimYukle($_FILES['mobillogo'],380,136);
            }else
            {
                $this->ekstraalankarsilik[] = $db->VeriOkuTek($this->tablo,"mobillogo",$this->primary,1);
            }
            if(!empty($_FILES["favicon"]['name']))
            {
                $this->ekstraalankarsilik[] = $this->resimYukle($_FILES['favicon'],16,16);
            }else
            {
                $this->ekstraalankarsilik[] = $db->VeriOkuTek($this->tablo,"favicon",$this->primary,1);
            }
            if($db->veriGuncelleParametresiz($this->tablo,$this->alanlar,$this->primary,1,$this->ekstraalanlar,$this->ekstraalankarsilik)>0)
            {
                $db->Basarili($this->mesaj." düzenlendi");
            }else
            {
                $db->Basarisiz($this->mesaj."  düzenlenirken bir sorun oluştu");
            }
        }
    }
    function Insert()
    {
        global $db;
        $required = $this->requnedalanlar;
        $error = true;
        foreach($required as $field)
        {
            if (empty($_POST[trim($field)]))
            {
                $error =false;
            }
        }
        if($error==false)
        {
            $db->Basarisiz("Lütfen tüm alanları doldurunuz.");
        }else
        {
            $kacsoruisareti = count($this->alanlar)+count ($this->ekstraalanlar);
            $isaretler = array("NULL");
            for ($i =0; $i<$kacsoruisareti; $i++)
            {
                $isaretler[]="?";
            }
            $alankarsilik = array();
            foreach ($this->alanlar as $a)
            {
                $alankarsilik[]=$_POST[$a];
            }

            if($db->veriEkle($this->tablo,$isaretler,$alankarsilik)>0)
            {
                $db->Basarili($this->mesaj." eklendi");
            }else
            {
                $db->Basarisiz($this->mesaj."  eklenirken bir sorun oluştu");
            }

        }

    }
    function Delete()
    {

        global $db;
        global $admin;
        $db->veriSil($this->tablo,array($this->primary),array($_GET["id"]),"=");
        echo "<script>setTimeout(function() { document.location = $this->yonlendir; }, 0)</script>";
    }
    function Delete_All()
    {
        global $db;
        global $admin;
        foreach($_POST["SilID"] as $id)
        {
            $db->veriSil($this->tablo,array($this->primary),array($id),"=");
        }
        echo "<script>setTimeout(function() { document.location = $this->yonlendir;}, 0)</script>";

    }

}
?>
