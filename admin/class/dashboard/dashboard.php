<?php
class Modul
{
    public $tablo ="basvuru_formlari";
    public $primary ="id";
    public $alanlar = array("tekne_adi","baglama_limani_id","tam_boyu","en","su_cekimi","bayrak","cins","baglama_tarihi_min","baglama_tarihi_maks","baglama_yeri_id","ad_soyad_firma","vergi_dairesi","vergi_numarasi","adres","irtibat_numarasi","e_posta_adresi");
    public $ekstraalanlar = array("ip_adresi","sms_kodu","tutar","dokumanlar","tarih","durum");
    public $requnedalanlar = array();
    public $ekstraalankarsilik = array();
    public $mesaj ="Dashboard";
    public $yonlendir ="../../pages/dashboard/dashboard.php";
    public $breadcrumb=array(admin_breadcrumb_anasayfa,'Dashboard','Başvuru Düzenle','Başvuru Ekle');
    public $pagetitle ="Dashboard";
    public $pagealtitle =array("Dashboard","Başvuru Düzenle","Başvuru Ekle");
    function __construct()
    {
        global $db;
        global $yetkiler;
        if(!isset($yetkiler["raporlar.php"]))
        {
            ?>
            <script>
                setTimeout(function() { document.location = '../../pages/editor-sayfasi/editor-sayfasi'; }, 0)
            </script>
            <?php
            exit();
        }
    }

    function All()
    {
        global $db;

        if($_SESSION["kullanici_yetki"]=="admin")
        {
            $sorgu = "SELECT * FROM basvuru_formlari WHERE id!=0";
        }else
        {
            $ilceler = array();
            $sonuclar = $db->VeriOkuCoklu("kategori_yetkili",array("yetkili_id"),array($_SESSION["admin_kullanici_id"]));
            foreach($sonuclar as $s)
            {
                $ilceler[]=$s->kategori_id;
            }
            $ilcelerstring = implode(",",$ilceler);

            $sorgu = "SELECT * FROM basvuru_formlari  WHERE baglama_yeri_id IN($ilcelerstring)";

        }

        if(isset($_GET["baglama_limani_id"]) AND !empty($_GET["baglama_limani_id"]))
        {
            $sorgu.=" AND baglama_limani_id=$_GET[baglama_limani_id]";

        }
        if(isset($_GET["cins"]) AND !empty($_GET["cins"]))
        {
            $sorgu.=" AND cins='$_GET[cins]'";

        }
        if(isset($_GET["durum"]) AND !empty($_GET["durum"]))
        {
            $sorgu.=" AND durum=$_GET[durum]";

        }
        if(isset($_GET["baglama_tarihi_min"]) AND !empty($_GET["baglama_tarihi_min"]))
        {
            $sorgu.=" AND baglama_tarihi_min>='$_GET[baglama_tarihi_min]'";

        }
        if(isset($_GET["baglama_tarihi_maks"]) AND !empty($_GET["baglama_tarihi_maks"]))
        {
            $sorgu.=" AND baglama_tarihi_maks<='$_GET[baglama_tarihi_maks]'";

        }
        if(isset($_GET["min_boy"]) AND !empty($_GET["min_boy"]))
        {
            $sorgu.=" AND tam_boyu>='$_GET[min_boy]'";
        }
        if(isset($_GET["max_boy"]) AND !empty($_GET["max_boy"]))
        {
            $sorgu.=" AND tam_boyu<='$_GET[max_boy]'";
        }
        if(isset($_GET["min_en"]) AND !empty($_GET["min_en"]))
        {
            $sorgu.=" AND en>='$_GET[min_en]'";
        }
        if(isset($_GET["max_en"]) AND !empty($_GET["max_en"]))
        {
            $sorgu.=" AND en<='$_GET[max_en]'";
        }

        if(isset($_GET["min_su_cekimi"]) AND !empty($_GET["min_su_cekimi"]))
        {
            $sorgu.=" AND su_cekimi>='$_GET[min_su_cekimi]'";
        }
        if(isset($_GET["max_su_cekimi"]) AND !empty($_GET["max_su_cekimi"]))
        {
            $sorgu.=" AND su_cekimi<='$_GET[max_su_cekimi]'";
        }

        return $db->VeriOkuCokluSorgu($sorgu);
    }


    function Update()
    {
        global $db;
        $required = $this->requnedalanlar;
        $error = true;
        foreach($required as $field)
        {
            if (empty($_POST[trim($field)]))
            {
                $error =false;
            }
        }
        if($error==false)
        {
            $db->Basarisiz("Lütfen tüm alanları doldurunuz.");
        }else
        {

            $dosyalar = $db->VeriOkuTek("basvuru_formlari","dokumanlar","id",$_GET["id"]);

            $klasor = "../../../dosyalar";
            $dosya_sayi=count($_FILES['dosyalar']['name']);
            for($i=0;$i<$dosya_sayi;$i++)
            {
                $isim = rand(0,999999);

                $uzantiparcala = explode(".",$_FILES['dosyalar']['name'][$i]);
                $yeniisim =$isim.".".end($uzantiparcala);


                if(!empty($_FILES['dosyalar']['name'][$i]))
                {
                    $ekle = move_uploaded_file($_FILES['dosyalar']['tmp_name'][$i],$klasor."/".$yeniisim);
                    if($ekle)
                    {
                        $yeniadres = str_replace("../","",$klasor)."/".$yeniisim;
                        if($dosyalar=='')
                        {
                            $dosyalar.=$yeniadres;
                        }else
                        {
                            $dosyalar.=",".$yeniadres;
                        }
                    }
                }
            }






            $this->ekstraalankarsilik[]=$db->VeriOkuTek("basvuru_formlari","ip_adresi","id",$_GET["id"]);
            $this->ekstraalankarsilik[]=$db->VeriOkuTek("basvuru_formlari","sms_kodu","id",$_GET["id"]);

            $this->ekstraalankarsilik[]=$_POST["tutar"];
            $this->ekstraalankarsilik[]=$dosyalar;
            $this->ekstraalankarsilik[]=$db->VeriOkuTek("basvuru_formlari","tarih","id",$_GET["id"]);

            if($db->VeriOkuTek("basvuru_formlari","durum","id",$_GET["id"])==2)
            {
                if($_POST["tutar"]!='')
                {
                    $this->ekstraalankarsilik[]=3;
                    // Burada Sanal Pos Linki Gidecek
                }else
                {
                    $this->ekstraalankarsilik[]=$db->VeriOkuTek("basvuru_formlari","durum","id",$_GET["id"]);
                }
            }else
            {
                $this->ekstraalankarsilik[]=$_POST["tutar"];
                $this->ekstraalankarsilik[]=$db->VeriOkuTek("basvuru_formlari","durum","id",$_GET["id"]);
            }




            if($db->veriGuncelleParametresiz($this->tablo,$this->alanlar,$this->primary,$_GET["id"],$this->ekstraalanlar,$this->ekstraalankarsilik)>0)
            {
                if($_SESSION["ana_yetki"]==0)
                {
                    $kullanici =0;
                }else
                {
                    $kullanici =$_SESSION["admin_kullanici_id"];
                }
                $db->veriEkle("log_kayitlari",array("NULL","?","?","NOW()"),array($kullanici,"Başvuru Güncellendi: $_POST[tekne_adi]"));
                $db->Basarili($this->mesaj." düzenlendi");
            }else
            {
                $db->Basarisiz($this->mesaj."  düzenlenirken bir sorun oluştu");
            }
        }
    }
    function Insert()
    {
        global $db;
        $required = $this->requnedalanlar;
        $error = true;
        foreach($required as $field)
        {
            if (empty($_POST[trim($field)]))
            {
                echo $field;
                $error =false;
            }
        }
        if($error==false)
        {
            $db->Basarisiz("Lütfen tüm alanları doldurunuz.");
        }else
        {
            $kacsoruisareti = count($this->alanlar)+count ($this->ekstraalanlar);
            $isaretler = array("NULL");
            for ($i =0; $i<$kacsoruisareti; $i++)
            {
                $isaretler[]="?";
            }
            $alankarsilik = array();
            foreach ($this->alanlar as $a)
            {
                $alankarsilik[]=$_POST[$a];
            }
            $alankarsilik[]=$db->urlYap ($_POST["baslik"]);

            if($db->veriEkle($this->tablo,$isaretler,$alankarsilik)>0)
            {
                if($_SESSION["ana_yetki"]==0)
                {
                    $kullanici =0;
                }else
                {
                    $kullanici =$_SESSION["admin_kullanici_id"];
                }
                $db->veriEkle("log_kayitlari",array("NULL","?","?","NOW()"),array($kullanici,"Başvuru Eklendi: $_POST[baslik]"));
                $db->Basarili($this->mesaj." eklendi");
            }else
            {
                $db->Basarisiz($this->mesaj."  eklenirken bir sorun oluştu");
            }

        }

    }
    function Delete()
    {

        global $db;
        global $admin;
        $db->veriSil($this->tablo,array($this->primary),array($_GET["id"]),"=");
        if($_SESSION["ana_yetki"]==0)
        {
            $kullanici =0;
        }else
        {
            $kullanici =$_SESSION["admin_kullanici_id"];
        }
        $baslik = $db->VeriOkuTek($this->tablo,"tekne_adi","id",$_GET["id"]);
        $db->veriEkle("log_kayitlari",array("NULL","?","?","NOW()"),array($kullanici,"Başvuru silindi: $baslik"));
        echo "<script>setTimeout(function() { document.location = $this->yonlendir; }, 0)</script>";
    }
    function Delete_All()
    {
        global $db;
        global $admin;
        foreach($_POST["SilID"] as $id)
        {
            $db->veriSil($this->tablo,array($this->primary),array($id),"=");
            if($_SESSION["ana_yetki"]==0)
            {
                $kullanici =0;
            }else
            {
                $kullanici =$_SESSION["admin_kullanici_id"];
            }
            $baslik = $db->VeriOkuTek($this->tablo,"tekne_adi","id",$id);
            $db->veriEkle("log_kayitlari",array("NULL","?","?","NOW()"),array($kullanici,"Başvuru silindi: $baslik"));
        }
        echo "<script>setTimeout(function() { document.location = $this->yonlendir;}, 0)</script>";

    }

    function dosyaSil()
    {
        global $db;
        $explode= explode(",",$db->VeriOkuTek("basvuru_formlari","dokumanlar","id",$_GET["id"]));
        $dosyalar = array();
        foreach($explode as $e)
        {
            if($e!=$_GET["dosya"])
            {
                $dosyalar[]=$e;
            }
        }
        $db->veriGuncelle("basvuru_formlari",array("dokumanlar"),array(implode(",",$dosyalar),$_GET["id"]),"id");

        if($_SESSION["ana_yetki"]==0)
        {
            $kullanici =0;
        }else
        {
            $kullanici =$_SESSION["admin_kullanici_id"];
        }
        $baslik = $db->VeriOkuTek($this->tablo,"tekne_adi","id",$_GET["id"]);
        $db->veriEkle("log_kayitlari",array("NULL","?","?","NOW()"),array($kullanici,"Başvuruya eklenen dosya silindi: $baslik"));
    }
    function Onay()
    {

        global $db;
        global $admin;
        $db->veriGuncelle("basvuru_formlari",array("durum"),array(1,$_GET["id"]),"id");
        if($_SESSION["ana_yetki"]==0)
        {
            $kullanici =0;
        }else
        {
            $kullanici =$_SESSION["admin_kullanici_id"];
        }
        $baslik = $db->VeriOkuTek($this->tablo,"tekne_adi","id",$_GET["id"]);
        $db->veriEkle("log_kayitlari",array("NULL","?","?","NOW()"),array($kullanici,"Başvuru onaylandı: $baslik"));
        echo "<script>setTimeout(function() { document.location = $this->yonlendir; }, 0)</script>";
    }
    function Iptal()
    {

        global $db;
        global $admin;
        $db->veriGuncelle("basvuru_formlari",array("durum"),array(4,$_GET["id"]),"id");
        if($_SESSION["ana_yetki"]==0)
        {
            $kullanici =0;
        }else
        {
            $kullanici =$_SESSION["admin_kullanici_id"];
        }
        $baslik = $db->VeriOkuTek($this->tablo,"tekne_adi","id",$_GET["id"]);
        $db->veriEkle("log_kayitlari",array("NULL","?","?","NOW()"),array($kullanici,"Başvuru reddedildi: $baslik"));
        echo "<script>setTimeout(function() { document.location = $this->yonlendir; }, 0)</script>";
    }
}
?>
