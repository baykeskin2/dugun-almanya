<?php
class Modul
{
    public $tablo ="ayarlar";
    public $primary ="id";
    public $alanlar = array("sitebaslik","siteadresi","dizin","mail","telefon","adres","facebook","twitter","instagram","youtube","calisma_saatleri");
    public $ekstraalanlar = array("logo","favicon");
    public $requnedalanlar = array();
    public $ekstraalankarsilik = array();
    public $mesaj ="Genel Ayarlar";
    public $yonlendir ="../../pages/genelayarlar/genelayarlar.php";
    public $breadcrumb=array('Anasayfa','Genelayarlar','Genel Ayarları Düzenle');
    public $pagetitle ="Genel Ayarlar";
    public $pagealtitle =array("Genel Ayarlar","Genel Ayarları Düzenle","Genel Ayarlar Ekle");

    function resimYukle($resim,$genislik,$yukseklik)
    {
        $image = new Upload($resim);
        if ($image->uploaded)
        {
            $yenisim = rand(0,9999999);
            $image->file_new_name_body = $yenisim;
            $image->image_resize = true;
            $image->image_x = $genislik;
            $image->image_y = $yukseklik;
            $image->allowed = array ("image/*");
            $image->Process(realpath('../../../')."/images");
            if ($image->processed)
            {
                return "images/".$image->file_dst_name;
            }else
            {
                return false;
            }
        }

    }


    function All()
    {

        global $db;
        if($db->veriSaydir($this->tablo)<1)
        {

            return false;
        }else
        {
            echo $db->VeriOkuCoklu($this->tablo,array(),array(),"",$this->primary,"DESC","");
            return true;
        }
    }

    function Update()
    {
        global $db;
        if($_SESSION["kullanici_yetki"]=="hat")
        {
            $db->veriGuncelle("yetkililer",array("mail","sifre"),array($_POST["mail"],$_POST["sifre"],$_SESSION["admin_kullanici_id"]),"id");

        }else
        {
            $db->veriGuncelle("admin_kullanicilar",array("kullanici_adi","sifre"),array($_POST["kullanici_adi"],$_POST["sifre"],$_SESSION["admin_kullanici_id"]),"id");
        }
        $db->Basarili("Bilgileriniz güncellendi");
    }
    function Insert()
    {
        global $db;
        $required = $this->requnedalanlar;
        $error = true;
        foreach($required as $field)
        {
            if (empty($_POST[trim($field)]))
            {
                $error =false;
            }
        }
        if($error==false)
        {
            $db->Basarisiz("Lütfen tüm alanları doldurunuz.");
        }else
        {
            $kacsoruisareti = count($this->alanlar)+count ($this->ekstraalanlar);
            $isaretler = array("NULL");
            for ($i =0; $i<$kacsoruisareti; $i++)
            {
                $isaretler[]="?";
            }
            $alankarsilik = array();
            foreach ($this->alanlar as $a)
            {
                $alankarsilik[]=$_POST[$a];
            }

            if($db->veriEkle($this->tablo,$isaretler,$alankarsilik)>0)
            {
                $db->Basarili($this->mesaj." eklendi");
            }else
            {
                $db->Basarisiz($this->mesaj."  eklenirken bir sorun oluştu");
            }

        }

    }
    function Delete()
    {

        global $db;
        global $admin;
        $db->veriSil($this->tablo,array($this->primary),array($_GET["id"]),"=");
        echo "<script>setTimeout(function() { document.location = $this->yonlendir; }, 0)</script>";
    }
    function Delete_All()
    {
        global $db;
        global $admin;
        foreach($_POST["SilID"] as $id)
        {
            $db->veriSil($this->tablo,array($this->primary),array($id),"=");
        }
        echo "<script>setTimeout(function() { document.location = $this->yonlendir;}, 0)</script>";

    }

}
?>
