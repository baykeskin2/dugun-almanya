<?php
class Modul
{
    public $tablo ="firma_kategori";
    public $primary ="id";
    public $alanlar = array("baslik");
    public $ekstraalanlar = array();
    public $requnedalanlar = array();
    public $ekstraalankarsilik = array();
    public $mesaj =admin_breadcrumb_firma_kategorileri;
    public $yonlendir ="../../pages/firma-kategorileri/firma-kategorileri.php";
    public $breadcrumb=array(admin_breadcrumb_anasayfa,admin_breadcrumb_firma_kategorileri,admin_breadcrumb_firma_kategorileri_duzenle,admin_breadcrumb_firma_kategorileri_ekle);
    public $pagetitle =admin_breadcrumb_firma_kategorileri;
    public $pagealtitle =array(admin_breadcrumb_firma_kategorileri,admin_breadcrumb_firma_kategorileri_duzenle,admin_breadcrumb_firma_kategorileri_ekle);
    function __construct()
    {
        global $db;
        global $yetkiler;
        if(!isset($yetkiler["firma-kategorileri.php"]))
        {
            ?>
            <script>
                setTimeout(function() { document.location = '../../pages/editor-sayfasi/editor-sayfasi'; }, 0)
            </script>
            <?php
            exit();
        }
    }
    function All()
    {
        global $db;
        return $db->VeriOkuCoklu($this->tablo,array(),array(),"",$this->primary,"DESC","");
    }

    function Update()
    {
        global $db;
        $required = $this->requnedalanlar;
        $error = true;
        foreach($required as $field)
        {
            if (empty($_POST[trim($field)]))
            {
                $error =false;
            }
        }
        if($error==false)
        {
            $db->Basarisiz("Lütfen tüm alanları doldurunuz.");
        }else
        {
            if($db->veriGuncelleParametresiz($this->tablo,$this->alanlar,$this->primary,$_GET["id"],$this->ekstraalanlar,$this->ekstraalankarsilik)>0)
            {

                $db->Basarili(admin_islem_basarili);
            }else
            {
                $db->Basarisiz(admin_islem_basarisiz);
            }
        }
    }
    function Insert()
    {
        global $db;
        $required = $this->requnedalanlar;
        $error = true;
        foreach($required as $field)
        {
            if (empty($_POST[trim($field)]))
            {
                echo $field;
                $error =false;
            }
        }
        if($error==false)
        {
            $db->Basarisiz("Lütfen tüm alanları doldurunuz.");
        }else
        {
            $kacsoruisareti = count($this->alanlar)+count ($this->ekstraalanlar);
            $isaretler = array("NULL");
            for ($i =0; $i<$kacsoruisareti; $i++)
            {
                $isaretler[]="?";
            }
            $alankarsilik = array();
            foreach ($this->alanlar as $a)
            {
                $alankarsilik[]=$_POST[$a];
            }
            if($db->veriEkle($this->tablo,$isaretler,$alankarsilik)>0)
            {

                $db->Basarili(admin_islem_basarili);
            }else
            {
                $db->Basarisiz(admin_islem_basarisiz);
            }

        }

    }
    function Delete()
    {

        global $db;
        global $admin;
        $db->veriSil($this->tablo,array($this->primary),array($_GET["id"]),"=");
        echo "<script>setTimeout(function() { document.location = $this->yonlendir; }, 0)</script>";
    }
    function Delete_All()
    {
        global $db;
        global $admin;
        foreach($_POST["SilID"] as $id)
        {
            $db->veriSil($this->tablo,array($this->primary),array($id),"=");
        }
        echo "<script>setTimeout(function() { document.location = $this->yonlendir;}, 0)</script>";

    }

}
?>
