<?php
class Modul
{
    public $tablo ="admin";
    public $primary ="AdminID";
    public $alanlar = array("KullaniciADI","KullaniciSIFRE");
    public $ekstraalanlar = array();
    public $requnedalanlar = array("KullaniciADI","KullaniciSIFRE");
    public $ekstraalankarsilik = array();
    public $mesaj ="Bilgileriniz";
    public $yonlendir ="../../pages/bilgilerim/bilgilerimiduzenle.php";

    function Update()
    {
        global $db;
        $required = $this->requnedalanlar;
        $error = true;
        foreach($required as $field)
        {
            if (empty($_POST[trim($field)]))
            {
                $error =false;
            }
        }
        if($error==false)
        {
            $db->Basarisiz("Lütfen tüm alanları doldurunuz.");
        }else
        {
            if($db->veriGuncelleParametresiz($this->tablo,$this->alanlar,$this->primary,1,$this->ekstraalanlar,$this->ekstraalankarsilik)>0)
            {
                $db->Basarili($this->mesaj." düzenlendi");
            }else
            {
                $db->Basarisiz($this->mesaj."  düzenlenirken bir sorun oluştu");
            }
        }
    }

}
?>
