<?php
class Modul
{
    public $tablo ="ayarlar";
    public $primary ="id";
    public $alanlar = array("smtphost","smtpuser","smtpport","smtpguvenlik","smtpsifre");
    public $ekstraalanlar = array();
    public $requnedalanlar = array("smtphost","smtpuser","smtpport","smtpguvenlik","smtpsifre");
    public $ekstraalankarsilik = array();
    public $mesaj ="SMTP Ayarları";
    public $yonlendir ="../../pages/smtpayarlari/smtpayarlari.php";
    public $breadcrumb=array(admin_breadcrumb_anasayfa,admin_breadcrumb_smtp_ayarlari,admin_breadcrumb_smtp_ayarlari_duzenle);
    public $pagetitle =admin_breadcrumb_smtp_ayarlari;
    public $pagealtitle =array(admin_breadcrumb_smtp_ayarlari,admin_breadcrumb_smtp_ayarlari_duzenle,admin_breadcrumb_smtp_ayarlari_ekle);
    function __construct()
    {
        global $db;
        global $yetkiler;
        if(!isset($yetkiler["smtpayarlari.php"]))
        {
            ?>
            <script>
                setTimeout(function() { document.location = '../../pages/editor-sayfasi/editor-sayfasi.php'; }, 0)
            </script>
            <?php
            exit();
        }
    }
    function All()
    {

        global $db;
        if($db->veriSaydir($this->tablo)<1)
        {

            return false;
        }else
        {
            echo $db->VeriOkuCoklu($this->tablo,array(),array(),"",$this->primary,"DESC","");
            return true;
        }
    }

    function Update()
    {
        global $db;
        $required = $this->requnedalanlar;
        $error = true;
        foreach($required as $field)
        {
            if (empty($_POST[trim($field)]))
            {
                $error =false;
            }
        }
        if($error==false)
        {
            $db->Basarisiz("Lütfen tüm alanları doldurunuz.");
        }else
        {
            if($db->veriGuncelleParametresiz($this->tablo,$this->alanlar,$this->primary,1,$this->ekstraalanlar,$this->ekstraalankarsilik)>0)
            {

                $db->Basarili(admin_islem_basarili);
            }else
            {
                $db->Basarisiz(admin_islem_basarisiz);
            }
        }
    }
}
?>
