<?php
class Modul
{
    public $tablo ="api_bilgileri";
    public $primary ="id";
    public $alanlar = array("netgsm_header","netgsm_kullaniciadi","netgsm_sifre");
    public $ekstraalanlar = array();
    public $requnedalanlar = array();
    public $ekstraalankarsilik = array();
    public $mesaj =admin_breadcrumb_dil_ayarlari;
    public $yonlendir ="../../pages/dilayarlari/dilayarlari.php";
    public $breadcrumb=array(admin_breadcrumb_anasayfa,admin_breadcrumb_dil_ayarlari,admin_breadcrumb_dil_ayarlari_duzenle);
    public $pagetitle =admin_breadcrumb_dil_ayarlari;
    public $pagealtitle =array(admin_breadcrumb_dil_ayarlari,admin_breadcrumb_dil_ayarlari_duzenle,admin_breadcrumb_dil_ayarlari_ekle);
    function __construct()
    {
        global $db;
        global $yetkiler;
        if(!isset($yetkiler["dilayarlari.php"]))
        {
            ?>
            <script>
                setTimeout(function() { document.location = '../../pages/editor-sayfasi/editor-sayfasi'; }, 0)
            </script>
            <?php
            exit();
        }
    }
    function All()
    {

        global $db;
        if($db->veriSaydir($this->tablo)<1)
        {

            return false;
        }else
        {
            echo $db->VeriOkuCoklu($this->tablo,array(),array(),"",$this->primary,"DESC","");
            return true;
        }
    }

    function Update()
    {
        global $db;
        $i =0;
        foreach($_POST["dil_id"] as $d)
        {
            $db->veriGuncelle("dil_ayarlari",array("karsilik_$_GET[dil]"),array($_POST["karsilik"][$i],$d),"id");
            $i++;
        }
        $db->Basarili(admin_islem_basarili);
    }
}
?>
