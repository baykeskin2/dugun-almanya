<?php
class Modul
{
    public $tablo ="blog";
    public $primary ="id";
    public $alanlar = array("baslik","aciklama");
    public $ekstraalanlar = array("resim","url");
    public $requnedalanlar = array();
    public $ekstraalankarsilik = array();
    public $mesaj =admin_breadcrumb_blog;
    public $yonlendir ="../../pages/blog/blog.php";
    public $breadcrumb=array(admin_breadcrumb_anasayfa,admin_breadcrumb_blog,admin_breadcrumb_blog_duzenle,admin_breadcrumb_blog_ekle);
    public $pagetitle =admin_breadcrumb_blog;
    public $pagealtitle =array(admin_breadcrumb_blog,admin_breadcrumb_blog_duzenle,admin_breadcrumb_blog_ekle);
    function __construct()
    {
        global $db;
        global $yetkiler;
        if(!isset($yetkiler["blog.php"]))
        {
            ?>
            <script>
                setTimeout(function() { document.location = '../../pages/editor-sayfasi/editor-sayfasi'; }, 0)
            </script>
            <?php
            exit();
        }
    }

    function resimYukle($resim,$genislik,$yukseklik)
    {
        $image = new Upload($resim);
        if ($image->uploaded)
        {
            $yenisim = rand(0,9999999);
            $image->file_new_name_body = $yenisim;
            $image->image_resize = true;
            $image->image_x = $genislik;
            $image->image_y = $yukseklik;
            $image->allowed = array ("image/*");
            $image->Process(realpath('../../../')."/images");
            if ($image->processed)
            {
                return "images/".$image->file_dst_name;
            }else
            {
                return false;
            }
        }

    }
    function All()
    {
        global $db;
        return $db->VeriOkuCoklu($this->tablo,array(),array(),"",$this->primary,"DESC","");
    }

    function Update()
    {
        global $db;
        $required = $this->requnedalanlar;
        $error = true;
        foreach($required as $field)
        {
            if (empty($_POST[trim($field)]))
            {
                $error =false;
            }
        }
        if($error==false)
        {
            $db->Basarisiz("Lütfen tüm alanları doldurunuz.");
        }else
        {
            if(!empty($_FILES["resim"]['name']))
            {
                $this->ekstraalankarsilik[] = $this->resimYukle($_FILES['resim'],1920,1000);
            }else
            {
                $this->ekstraalankarsilik[] = $db->VeriOkuTek($this->tablo,"resim",$this->primary,1);
            }

            $this->ekstraalankarsilik[]=$db->urlYap($_POST["baslik"]);

            if($db->veriGuncelleParametresiz($this->tablo,$this->alanlar,$this->primary,$_GET["id"],$this->ekstraalanlar,$this->ekstraalankarsilik)>0)
            {

                $db->Basarili(admin_islem_basarili);
            }else
            {
                $db->Basarisiz(admin_islem_basarisiz);
            }
        }
    }
    function Insert()
    {
        global $db;
        $required = $this->requnedalanlar;
        $error = true;
        foreach($required as $field)
        {
            if (empty($_POST[trim($field)]))
            {
                echo $field;
                $error =false;
            }
        }
        if($error==false)
        {
            $db->Basarisiz("Lütfen tüm alanları doldurunuz.");
        }else
        {
            $kacsoruisareti = count($this->alanlar)+count ($this->ekstraalanlar);
            $isaretler = array("NULL");
            for ($i =0; $i<$kacsoruisareti; $i++)
            {
                $isaretler[]="?";
            }
            $alankarsilik = array();
            foreach ($this->alanlar as $a)
            {
                $alankarsilik[]=$_POST[$a];
            }

            if(!empty($_FILES["resim"]['name']))
            {
                $alankarsilik[] = $this->resimYukle($_FILES['resim'],1920,1000);
            }else
            {
                $alankarsilik[] = "";
            }
            $alankarsilik[]=$db->urlYap($_POST["baslik"]);
            if($db->veriEkle($this->tablo,$isaretler,$alankarsilik)>0)
            {

                $db->Basarili(admin_islem_basarili);
            }else
            {
                $db->Basarisiz(admin_islem_basarisiz);
            }

        }

    }
    function Delete()
    {

        global $db;
        global $admin;
        $db->veriSil($this->tablo,array($this->primary),array($_GET["id"]),"=");

        echo "<script>setTimeout(function() { document.location = $this->yonlendir; }, 0)</script>";
    }
    function Delete_All()
    {
        global $db;
        global $admin;
        foreach($_POST["SilID"] as $id)
        {
            $db->veriSil($this->tablo,array($this->primary),array($id),"=");

        }
        echo "<script>setTimeout(function() { document.location = $this->yonlendir;}, 0)</script>";

    }

}
?>
