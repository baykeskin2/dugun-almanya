<?php
class Modul
{
    public $tablo ="seo_ayarlari";
    public $primary ="id";
    public $alanlar = array("baslik","dosya","title","description","keywords");
    public $ekstraalanlar = array();
    public $requnedalanlar = array();
    public $ekstraalankarsilik = array();
    public $mesaj =admin_breadcrumb_seo_ayarlari;
    public $yonlendir ="../../pages/seo-ayarlari/seo-ayarlari.php";
    public $breadcrumb=array(admin_breadcrumb_anasayfa,admin_breadcrumb_seo_ayarlari,admin_breadcrumb_seo_ayarlari_duzenle,admin_breadcrumb_seo_ayarlari_ekle);
    public $pagetitle =admin_breadcrumb_seo_ayarlari;
    public $pagealtitle =array(admin_breadcrumb_seo_ayarlari,admin_breadcrumb_seo_ayarlari_duzenle,admin_breadcrumb_seo_ayarlari_ekle);
    function __construct()
    {
        global $db;
        global $yetkiler;
        if(!isset($yetkiler["seo-ayarlari.php"]))
        {
            ?>
            <script>
                setTimeout(function() { document.location = '../../pages/editor-sayfasi/editor-sayfasi'; }, 0)
            </script>
            <?php
            exit();
        }
    }
    function resimYukle($resim,$genislik,$yukseklik)
    {
        $image = new Upload($resim);
        if ($image->uploaded)
        {
            $yenisim = rand(0,9999999);
            $image->file_new_name_body = $yenisim;
            $image->image_resize = true;
            $image->image_x = $genislik;
            $image->image_y = $yukseklik;
            $image->allowed = array ("image/*");
            $image->Process(realpath('../../../')."/images");
            if ($image->processed)
            {
                return "images/".$image->file_dst_name;
            }else
            {
                return false;
            }
        }
    }
    function All()
    {
        global $db;
        return $db->VeriOkuCoklu($this->tablo,array(),array(),"",$this->primary,"DESC","");
    }

    function Update()
    {
        global $db;
        $required = $this->requnedalanlar;
        $error = true;
        foreach($required as $field)
        {
            if (empty($_POST[trim($field)]))
            {
                echo $field;
                $error =false;
            }
        }
        if($error==false)
        {
            $db->Basarisiz("Lütfen tüm alanları doldurunuz.");
        }else
        {
            if($db->veriGuncelleParametresiz($this->tablo,$this->alanlar,$this->primary,$_GET["id"],$this->ekstraalanlar,$this->ekstraalankarsilik)>0)
            {
                $db->Basarili(admin_islem_basarili);
            }else
            {
                $db->Basarisiz(admin_islem_basarisiz);
            }
        }
    }
    function Insert()
    {
        global $db;
        $required = $this->requnedalanlar;
        $error = true;
        foreach($required as $field)
        {
            if (empty($_POST[trim($field)]))
            {
                echo $field;
                $error =false;
            }
        }
        if($error==false)
        {
            $db->Basarisiz("Lütfen tüm alanları doldurunuz.");
        }else
        {
            $kacsoruisareti = count($this->alanlar)+count ($this->ekstraalanlar);
            $isaretler = array("NULL");
            for ($i =0; $i<$kacsoruisareti; $i++)
            {
                $isaretler[]="?";
            }
            $alankarsilik = array();
            foreach ($this->alanlar as $a)
            {
                $alankarsilik[]=$_POST[$a];
            }
            if($db->veriEkle($this->tablo,$isaretler,$alankarsilik)>0)
            {
                $db->Basarili(admin_islem_basarili);
            }else
            {
                $db->Basarisiz(admin_islem_basarisiz);
            }
        }
    }
    function Delete()
    {

        global $db;
        global $admin;
        $db->veriSil($this->tablo,array($this->primary),array($_GET["id"]),"=");
        echo "<script>setTimeout(function() { document.location = $this->yonlendir; }, 0)</script>";
    }
    function Delete_All()
    {
        global $db;
        global $admin;
        foreach($_POST["SilID"] as $id)
        {
            $db->veriSil($this->tablo,array($this->primary),array($id),"=");
        }
        echo "<script>setTimeout(function() { document.location = $this->yonlendir;}, 0)</script>";

    }

}
?>



