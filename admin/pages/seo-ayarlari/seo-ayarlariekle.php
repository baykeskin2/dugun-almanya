<?php
include"../../class/ayarlar.php";
?>
<!doctype html>
<html class="fixed">
<head>
    <meta charset="UTF-8">
    <title><?=$db->VeriOkuTek ("ayarlar","sitebaslik","id",1)?></title>
    <meta name="keywords" content="<?=$db->VeriOkuTek ("ayarlar","sitebaslik","id",1)?>" />
    <meta name="description" content="<?=$db->VeriOkuTek ("ayarlar","sitebaslik","id",1)?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">
    <!-- Vendor CSS -->
    <link rel="stylesheet" href="../../vendor/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="../../vendor/animate/animate.compat.css">
    <link rel="stylesheet" href="../../vendor/font-awesome/css/all.min.css" />
    <link rel="stylesheet" href="../../vendor/boxicons/css/boxicons.min.css" />
    <link rel="stylesheet" href="../../vendor/magnific-popup/magnific-popup.css" />
    <link rel="stylesheet" href="../../vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css" />
    <link rel="stylesheet" href="../../vendor/bootstrap-fileupload/bootstrap-fileupload.min.css" />
    <link rel="stylesheet" href="../../css/theme.css" />
    <link rel="stylesheet" href="../../css/custom.css">
    <script src="../../vendor/modernizr/modernizr.js"></script>
    <link rel="stylesheet" href="../../vendor/datatables/media/css/dataTables.bootstrap5.css" />
    <link rel="stylesheet" href="../../vendor/simple-line-icons/css/simple-line-icons.css" />
</head>
<body>
<section class="body">
    <?php
    include"../include/header.php";
    include"../../class/$class";
    $modul = new Modul();
    ?>
    <div class="inner-wrapper">
        <?php
        include"../include/menu.php";
        ?>

        <section role="main" class="content-body " style="padding: 7px">
            <header class="page-header">
                <h2><?=$modul->pagealtitle[0]?></h2>

                <div class="right-wrapper text-end">
                    <ol class="breadcrumbs">
                        <li><a href="<?=$siteURL?>"><i class="bx bx-home-alt"></i></a></li>
                        <li><span><?=$modul->breadcrumb[1]?></span></li>
                        <?php
                        if(isset($_GET["id"]))
                        {
                            ?>
                            <li><span><?=$modul->breadcrumb[2]?></span></li>
                            <?php
                        }else
                        {
                            ?>
                            <li><span><?=$modul->breadcrumb[3]?></span></li>
                            <?php
                        }
                        ?>


                    </ol>
                    <a class="sidebar-right-toggle" ><i class="fas fa-chevron-left"></i></a>
                </div>
            </header>

            <!-- start: page -->
            <div class="row">
                <div class="col">
                    <section class="card">
                        <div class="card-body">
                            <div class="col-lg-12">
                                <?php
                                if($_POST)
                                {
                                    if(isset($_GET["id"]))
                                    {
                                        $modul->Update ();
                                    }else
                                    {
                                        $modul->Insert ();
                                    }

                                }
                                if(isset($_GET["id"]))
                                {
                                    $liste = $db->VeriOkuCoklu ($modul->tablo,array($modul->primary),array($_GET["id"]));
                                }
                                ?>
                            </div>
                            <form class="ajaxFormFalse" method="POST" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-md-6  mb-3">
                                        <label class="form-label"><?=admin_modul_seo_ayarlari_baslik?></label>
                                        <input class="form-control vRequired" name="baslik" value="<?=isset($_GET["id"])?$liste[0]->baslik:''?>" />
                                    </div>
                                    <div class="col-md-6  mb-3">
                                        <label class="form-label"><?=admin_modul_seo_ayarlari_dosya?></label>
                                        <input class="form-control vRequired" name="dosya" value="<?=isset($_GET["id"])?$liste[0]->dosya:''?>" />
                                    </div>
                                    <div class="col-md-6  mb-3">
                                        <label class="form-label"><?=admin_modul_seo_ayarlari_seo_baslik?></label>
                                        <input class="form-control" name="title" value="<?=isset($_GET["id"])?$liste[0]->title:''?>" />
                                    </div>
                                    <div class="col-md-6  mb-3">
                                        <label class="form-label"><?=admin_modul_seo_ayarlari_aciklama?></label>
                                        <input class="form-control vRequired" name="description" value="<?=isset($_GET["id"])?$liste[0]->description:''?>" />
                                    </div>
                                    <div class="col-md-6  mb-3">
                                        <label class="form-label"><?=admin_modul_seo_ayarlari_anahtar_kelimeler?></label>
                                        <input class="form-control vRequired" name="keywords" value="<?=isset($_GET["id"])?$liste[0]->keywords:''?>" />
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary submitbtn"><?=admin_genel_gonder?></button>
                            </form>
                        </div>
                    </section>
                </div>
            </div>

        </section>
    </div>


</section>

<script src="../../vendor/jquery/jquery.js"></script>
<script src="../../vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
<script src="../../vendor/jquery-cookie/jquery.cookie.js"></script>
<script src="../../vendor/popper/umd/popper.min.js"></script>
<script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../../vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="../../vendor/common/common.js"></script>
<script src="../../vendor/nanoscroller/nanoscroller.js"></script>
<script src="../../vendor/magnific-popup/jquery.magnific-popup.js"></script>
<script src="../../vendor/jquery-placeholder/jquery.placeholder.js"></script>
<script src="../../vendor/autosize/autosize.js"></script>
<script src="../../vendor/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>


<script src="../../vendor/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="../../vendor/datatables/media/js/dataTables.bootstrap5.min.js"></script>
<script src="../../vendor/datatables/extras/TableTools/Buttons-1.4.2/js/dataTables.buttons.min.js"></script>
<script src="../../vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.bootstrap4.min.js"></script>
<script src="../../vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.html5.min.js"></script>
<script src="../../vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.print.min.js"></script>
<script src="../../vendor/datatables/extras/TableTools/JSZip-2.5.0/jszip.min.js"></script>
<script src="../../vendor/datatables/extras/TableTools/pdfmake-0.1.32/pdfmake.min.js"></script>
<script src="../../vendor/datatables/extras/TableTools/pdfmake-0.1.32/vfs_fonts.js"></script>

<script src="../../js/examples/examples.datatables.default.js"></script>
<script src="../../js/examples/examples.datatables.row.with.details.js"></script>
<script src="../../js/examples/examples.datatables.tabletools.js"></script>
<script src="../../vendor/select2/js/select2.js"></script>
<script src="../../vendor/bootstrapv5-multiselect/js/bootstrap-multiselect.js"></script>

<script src="../../js/theme.js"></script>
<script src="../../js/custom.js"></script>
<script src="../../js/theme.init.js"></script>
<script type="text/javascript" src="../../js/jquery/jquery-3.1.0.min.js"></script>
<script type="text/javascript" src="../../js/jquery.maskedinput.js"></script>
<script type="text/javascript">
    $( document ).ready(function( $ ) {
        $(".telefoninput").mask("(999) 999 99 99",{placeholder:"(___) ___ __ __"});
    });
</script>
<script type="text/javascript" src="../../js/validation_master.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.ajaxFormFalse').validationForm({'ajaxType':false});
        $('.ajaxFormTrue').validationForm({'ajaxType':true,'ajaxRefreshPage':true});
    })
</script>

<script type="text/javascript">
    $(document).ready(function(){
        $(".silbtn").click(function () {
            var href = $(this).attr("data-href");
            $(".mdlsilbtn").attr("href",href);
        })
    })
</script>
<script type="text/javascript">
    $(".selectable-all").click(function(){
        $('.selectable-item').not(this).prop('checked', this.checked);
    });
</script>

</body>
</html>