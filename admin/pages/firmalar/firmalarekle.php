<?php
include"../../class/ayarlar.php";
?>
<!doctype html>
<html class="fixed">
<head>
    <meta charset="UTF-8">
    <title><?=$db->VeriOkuTek ("ayarlar","sitebaslik","id",1)?></title>
    <meta name="keywords" content="<?=$db->VeriOkuTek ("ayarlar","sitebaslik","id",1)?>" />
    <meta name="description" content="<?=$db->VeriOkuTek ("ayarlar","sitebaslik","id",1)?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">
    <!-- Vendor CSS -->
    <link rel="stylesheet" href="../../vendor/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="../../vendor/animate/animate.compat.css">
    <link rel="stylesheet" href="../../vendor/font-awesome/css/all.min.css" />
    <link rel="stylesheet" href="../../vendor/boxicons/css/boxicons.min.css" />
    <link rel="stylesheet" href="../../vendor/magnific-popup/magnific-popup.css" />
    <link rel="stylesheet" href="../../vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css" />
    <link rel="stylesheet" href="../../vendor/bootstrap-fileupload/bootstrap-fileupload.min.css" />
    <link rel="stylesheet" href="../../css/theme.css" />
    <link rel="stylesheet" href="../../css/custom.css">
    <script src="../../vendor/modernizr/modernizr.js"></script>
    <link rel="stylesheet" href="../../vendor/datatables/media/css/dataTables.bootstrap5.css" />
    <link rel="stylesheet" href="../../vendor/simple-line-icons/css/simple-line-icons.css" />
</head>
<body>
<section class="body">
    <?php
    include"../include/header.php";
    include"../../class/$class";
    $modul = new Modul();
    ?>
    <div class="inner-wrapper">
        <?php
        include"../include/menu.php";
        ?>

        <section role="main" class="content-body " style="padding: 7px">
            <header class="page-header">
                <h2><?=$modul->pagealtitle[0]?></h2>

                <div class="right-wrapper text-end">
                    <ol class="breadcrumbs">
                        <li><a href="<?=$siteURL?>"><i class="bx bx-home-alt"></i></a></li>
                        <li><span><?=$modul->breadcrumb[1]?></span></li>
                        <?php
                        if(isset($_GET["id"]))
                        {
                            ?>
                            <li><span><?=$modul->breadcrumb[2]?></span></li>
                            <?php
                        }else
                        {
                            ?>
                            <li><span><?=$modul->breadcrumb[3]?></span></li>
                            <?php
                        }
                        ?>


                    </ol>
                    <a class="sidebar-right-toggle" ><i class="fas fa-chevron-left"></i></a>
                </div>
            </header>

            <!-- start: page -->
            <div class="row">
                <div class="col">
                    <section class="card">
                        <div class="card-body">
                            <div class="col-lg-12">
                                <?php
                                if($_POST)
                                {
                                    if(isset($_GET["id"]))
                                    {
                                        $modul->Update ();
                                    }else
                                    {
                                        $modul->Insert ();
                                    }

                                }
                                if(isset($_GET["resim"]))
                                {
                                    $modul->resimSil();
                                }
                                if(isset($_GET["id"]))
                                {
                                    $liste = $db->VeriOkuCoklu ($modul->tablo,array($modul->primary),array($_GET["id"]));
                                }
                                ?>
                            </div>
                            <form class="ajaxFormFalse" method="POST" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-md-8  mb-3">
                                        <label class="form-label"><?=admin_modul_firmalar_kategori?></label>
                                        <select class="form-control vRequired" name="kategori_id">
                                            <option value=""><?=admin_genel_seciniz?></option>
                                            <?php
                                            $firma_kategorileri = $db->VeriOkuCoklu("firma_kategori");
                                            if($firma_kategorileri===false)
                                            {

                                            }else
                                            {
                                                foreach($firma_kategorileri as $fk)
                                                {
                                                    ?>
                                                    <option <?=isset($_GET["id"])?$liste[0]->kategori_id==$fk->id?'selected':'':''?> value="<?=$fk->id?>"><?=$fk->baslik?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>

                                    </div>
                                    <div class="col-md-8  mb-3">
                                        <label class="form-label"><?=admin_modul_firmalar_firma_adi?></label>
                                        <input class="form-control vRequired" name="baslik" value="<?=isset($_GET["id"])?$liste[0]->baslik:''?>" />
                                    </div>

                                    <div class="col-md-8  mb-3">
                                        <label class="form-label"><?=admin_modul_firmalar_aciklama?></label>
                                        <textarea class="form-control ckeditor" name="aciklama"><?=isset($_GET["id"])?$liste[0]->aciklama:''?></textarea>
                                    </div>
                                    <div class="col-md-8  mb-3">
                                        <label class="form-label"><?=admin_modul_firmalar_lokasyon_bilgileri?></label>
                                        <input class="form-control vRequired" name="lokasyon" value="<?=isset($_GET["id"])?$liste[0]->lokasyon:''?>" />
                                    </div>
                                    <div class="col-md-8  mb-3">
                                        <label class="form-label"><?=admin_modul_firmalar_kapasite?></label>
                                        <input class="form-control vRequired" name="kapasite" value="<?=isset($_GET["id"])?$liste[0]->kapasite:''?>" />
                                    </div>
                                    <div class="col-md-8  mb-3">
                                        <label class="form-label"><?=admin_modul_firmalar_sehir?></label>
                                        <select class="form-control vRequired" name="sehir_id">
                                            <option value=""><?=admin_genel_seciniz?></option>
                                            <?php
                                            $sehirler = $db->VeriOkuCoklu("sehirler");
                                            if($sehirler===false)
                                            {

                                            }else
                                            {
                                                foreach($sehirler as $s)
                                                {
                                                    ?>
                                                    <option <?=isset($_GET["id"])?$liste[0]->sehir_id==$s->id?'selected':'':''?> value="<?=$s->id?>"><?=$s->baslik?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>

                                    </div>

                                    <div class="col-md-8  mb-3">
                                        <label class="form-label"><?=admin_modul_firmalar_vitrinde_ciksin_mi?></label>
                                        <select class="form-control vRequired" name="vitrin">
                                            <option value="0"><?=admin_modul_firmalar_vitrinde_ciksin_mi_hayir?></option>
                                            <option value="1"><?=admin_modul_firmalar_vitrinde_ciksin_mi_evet?></option>
                                        </select>

                                    </div>
                                    <section class="card mb-4">
                                        <header class="card-header">
                                            <div class="card-actions">
                                                <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
                                                <a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
                                            </div>

                                            <h2 class="card-title"><?=admin_modul_firmalar_videolar?></h2>
                                            <p class="card-subtitle"><?=admin_modul_firmalar_videolar_alt_aciklama?></p>
                                        </header>
                                        <div class="card-body ">
                                            <div class="row videolar">
                                            <?php
                                            if(isset($_GET["id"]))
                                            {
                                                if($liste[0]->videolar!='')
                                                {
                                                    $videolar = json_decode($liste[0]->videolar);
                                                    foreach($videolar as $v)
                                                    {
                                                        ?>
                                                        <div class="col-md-8  mb-3">
                                                            <label class="form-label"><?=admin_modul_firmalar_video?></label>
                                                            <input class="form-control vRequired" name="video[]" value="<?=isset($_GET["id"])?$v:''?>" />
                                                        </div>
                                                        <?php
                                                    }
                                                }
                                            }else
                                            {
                                                ?>
                                                <div class="col-md-8  mb-3">
                                                    <label class="form-label"><?=admin_modul_firmalar_video?></label>
                                                    <input class="form-control vRequired" name="video[]" value="" />
                                                </div>
                                                <?php
                                            }
                                            ?>
                                            </div>
                                            <button type="button" class="btn btn-default videoeklebtn"><?=admin_modul_firmalar_video_ekle?></button>
                                        </div>



                                    </section>

                                    <section class="card mb-4">
                                        <header class="card-header">
                                            <div class="card-actions">
                                                <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
                                                <a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
                                            </div>

                                            <h2 class="card-title"><?=admin_modul_firmalar_iletisim_bilgileri?></h2>
                                            <p class="card-subtitle"><?=admin_modul_firmalar_iletisim_bilgileri_alt_aciklama?></p>
                                        </header>
                                        <?php
                                        $iletisim_bilgileri ="";
                                        if(isset($_GET["id"]))
                                        {
                                            $iletisim_bilgileri = json_decode($liste[0]->iletisim_bilgileri);
                                        }
                                        ?>
                                        <div class="card-body ">
                                            <div class="col-md-8  mb-3">
                                                <label class="form-label"><?=admin_modul_firmalar_telefon?></label>
                                                <input class="form-control vRequired" name="telefon" value="<?=!empty($iletisim_bilgileri)?$iletisim_bilgileri->telefon:''?>" />
                                            </div>
                                            <div class="col-md-8  mb-3">
                                                <label class="form-label"><?=admin_modul_firmalar_adres?></label>
                                                <input class="form-control vRequired" name="adres" value="<?=!empty($iletisim_bilgileri)?$iletisim_bilgileri->adres:''?>" />
                                            </div>
                                            <div class="col-md-8  mb-3">
                                                <label class="form-label"><?=admin_modul_firmalar_mail_adresi?></label>
                                                <input class="form-control vRequired" name="e_mail" value="<?=!empty($iletisim_bilgileri)?$iletisim_bilgileri->e_mail:''?>" />
                                            </div>
                                            <div class="col-md-8  mb-3">
                                                <label class="form-label"><?=admin_modul_firmalar_harita_kodu?></label>
                                                <input class="form-control vRequired" name="harita" value="<?=!empty($iletisim_bilgileri)?htmlspecialchars($iletisim_bilgileri->harita):''?>" />
                                            </div>
                                        </div>
                                    </section>


                                    <section class="card mb-4">
                                        <header class="card-header">
                                            <div class="card-actions">
                                                <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
                                                <a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
                                            </div>

                                            <h2 class="card-title"><?=admin_modul_firmalar_firma_resmi?></h2>
                                            <p class="card-subtitle"><?=admin_modul_firmalar_firma_resmi_alt_aciklama?></p>
                                        </header>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="col-md-8  mb-3">
                                                        <label class="form-label"><?=admin_modul_firmalar_resim_ekle?></label>
                                                        <input type="file" multiple="multiple" class="form-control" name="resimler[]" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <?php
                                                if(isset($_GET["id"]))
                                                {
                                                    if($liste[0]->resimler!='')
                                                    {
                                                        $resimler = explode(",",$liste[0]->resimler);
                                                        foreach($resimler as $re)
                                                        {
                                                            ?>
                                                            <div class="col-md-3" style="text-align: center">
                                                                <img src="/<?=$dizin?><?=$re?>" style="width: 100%">
                                                                <a style="padding: 2px;padding-left: 12px;padding-right: 13px;font-size: 11px;"  class=" btn btn-danger" href="../../pages/firmalar/firmalarekle.php?islem=duzenle&id=<?=$_GET["id"]?>&resim=<?=$re?>"><i class="bx bx-message-square-minus"></i> Sil</a>
                                                            </div>
                                                            <?php
                                                        }
                                                    }
                                                }

                                                ?>
                                            </div>

                                        </div>
                                    </section>


                                    <section class="card mb-4">
                                        <header class="card-header">
                                            <div class="card-actions">
                                                <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
                                                <a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
                                            </div>

                                            <h2 class="card-title"><?=admin_modul_firmalar_genel_ozellikler?></h2>
                                            <p class="card-subtitle"><?=admin_modul_firmalar_genel_ozellikler_alt_aciklama?></p>
                                        </header>
                                        <div class="card-body ">
                                            <div class="row ozellikler">
                                                <?php
                                                if(isset($_GET["id"]))
                                                {
                                                    $var_olan_ozellikler =  (array) json_decode($liste[0]->genel_ozellikler);
                                                }
                                                ?>
                                                <?php
                                                $kategoriler = $db->VeriOkuCoklu("genel_ozellik_kategoriler");
                                                if($kategoriler===False)
                                                {

                                                }else
                                                {
                                                    foreach($kategoriler as $k)
                                                    {
                                                        ?>
                                                        <p style="font-weight: bold; margin: 0px;"><?=$k->baslik?></p>
                                                        <input type="hidden" name="ozellik_id[]" value="o_<?=$k->id?>">
                                                        <?php
                                                        $ozellikler = $db->VeriOkuCoklu("genel_ozellikler",array("kategori_id"),array($k->id));
                                                        if($ozellikler===false)
                                                        {

                                                        }else
                                                        {
                                                            ?>
                                                            <div class="col-md-12">
                                                            <?php
                                                            foreach($ozellikler as $row)
                                                            {
                                                                ?>
                                                                <div class="checkbox-custom checkbox-default">
                                                                    <input <?=isset($_GET["id"])?in_array($row->id,$var_olan_ozellikler["o_$k->id"])?'checked':'':''?> type="checkbox" name="o_<?=$k->id?>[]" value="<?=$row->id?>">
                                                                    <label for="checkboxExample1"><?=$row->baslik?></label>
                                                                </div>
                                                                <?php
                                                            }
                                                            ?>
                                                            </div>
                                                            <?php
                                                        }
                                                    }
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </section>

                                    <section class="card mb-4">
                                        <header class="card-header">
                                            <div class="card-actions">
                                                <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
                                                <a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
                                            </div>

                                            <h2 class="card-title">Filtre Özellikleri</h2>
                                            <p class="card-subtitle">Filtre Özelliklerini Seçiniz</p>
                                        </header>
                                        <div class="card-body ">
                                            <div class="row ozellikler">
                                                <?php
                                                $kategoriler = $db->VeriOkuCoklu("ozellikler");
                                                if($kategoriler===False)
                                                {

                                                }else
                                                {
                                                    foreach($kategoriler as $k)
                                                    {
                                                        ?>
                                                        <p style="font-weight: bold; margin: 0px;"><?=$k->baslik?></p>
                                                        <input type="hidden" name="fo_ozellik_id[]" value="fo_<?=$k->id?>">
                                                        <?php
                                                        $ozellikler = $db->VeriOkuCoklu("ozellik_karsiliklari",array("ozellik_id"),array($k->id));
                                                        if($ozellikler===false)
                                                        {

                                                        }else
                                                        {
                                                            ?>
                                                            <div class="col-md-12">
                                                                <?php
                                                                foreach($ozellikler as $row)
                                                                {
                                                                    ?>
                                                                    <div class="checkbox-custom checkbox-default">
                                                                        <input <?=isset($_GET["id"])?$db->veriSaydir("mekan_firma_ozellik",array("karsilik_id","firma_mekan_tur","firma_mekan_tur_id"),array($row->id,"firma",$_GET["id"]))>0?'checked':'':''?> type="checkbox" name="fo_<?=$k->id?>[]" value="<?=$row->id?>">
                                                                        <label for="checkboxExample1"><?=$row->baslik?></label>
                                                                    </div>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </div>
                                                            <?php
                                                        }
                                                    }
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </section>
                                    <section class="card mb-4">
                                        <header class="card-header">
                                            <div class="card-actions">
                                                <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
                                                <a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
                                            </div>

                                            <h2 class="card-title"><?=admin_modul_firmalar_fiyatlandirma_baslik?></h2>
                                            <p class="card-subtitle"><?=admin_modul_firmalar_fiyatlandirma_aciklama?></p>
                                        </header>
                                        <div class="card-body ">
                                            <div class="row fiyatlar">
                                                <?php
                                                if(isset($_GET["id"]))
                                                {
                                                    if($liste[0]->fiyatlandirma!='')
                                                    {
                                                        $fiyatlandirma = json_decode($liste[0]->fiyatlandirma);
                                                        foreach($fiyatlandirma as $v)
                                                        {
                                                            ?>
                                                            <div class="col-md-6  mb-3">
                                                                <label class="form-label"><?=admin_modul_firmalar_fiyatlandirma_alt_baslik?></label>
                                                                <input class="form-control vRequired" name="fiyat_baslik[]" value="<?=isset($_GET["id"])?$v->baslik:''?>" />
                                                            </div>
                                                            <div class="col-md-6  mb-3">
                                                                <label class="form-label"><?=admin_modul_firmalar_fiyatlandirma_fiyat?></label>
                                                                <input class="form-control vRequired" name="fiyat_baslik_fiyat[]" value="<?=isset($_GET["id"])?$v->fiyat:''?>" />
                                                            </div>
                                                            <?php
                                                        }
                                                    }
                                                }else
                                                {
                                                    ?>
                                                    <div class="col-md-6  mb-3">
                                                        <label class="form-label"><?=admin_modul_firmalar_fiyatlandirma_alt_baslik?></label>
                                                        <input class="form-control vRequired" name="fiyat_baslik[]" value="" />
                                                    </div>
                                                    <div class="col-md-6  mb-3">
                                                        <label class="form-label"><?=admin_modul_firmalar_fiyatlandirma_fiyat?></label>
                                                        <input class="form-control vRequired" name="fiyat_baslik_fiyat[]" value="" />
                                                    </div>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                            <button type="button" class="btn btn-default fiyatekle"><?=admin_modul_firmalar_yeni_fiyat_ekle?></button>
                                        </div>
                                    </section>
                                </div>





                                <button type="submit" class="btn btn-primary submitbtn"><?=admin_genel_gonder?></button>
                            </form>

                        </div>
                    </section>
                </div>
            </div>

        </section>
    </div>


</section>

<script src="../../vendor/jquery/jquery.js"></script>
<script src="../../vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
<script src="../../vendor/jquery-cookie/jquery.cookie.js"></script>
<script src="../../vendor/popper/umd/popper.min.js"></script>
<script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../../vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="../../vendor/common/common.js"></script>
<script src="../../vendor/nanoscroller/nanoscroller.js"></script>
<script src="../../vendor/magnific-popup/jquery.magnific-popup.js"></script>
<script src="../../vendor/jquery-placeholder/jquery.placeholder.js"></script>
<script src="../../vendor/autosize/autosize.js"></script>
<script src="../../vendor/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>


<script src="../../vendor/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="../../vendor/datatables/media/js/dataTables.bootstrap5.min.js"></script>
<script src="../../vendor/datatables/extras/TableTools/Buttons-1.4.2/js/dataTables.buttons.min.js"></script>
<script src="../../vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.bootstrap4.min.js"></script>
<script src="../../vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.html5.min.js"></script>
<script src="../../vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.print.min.js"></script>
<script src="../../vendor/datatables/extras/TableTools/JSZip-2.5.0/jszip.min.js"></script>
<script src="../../vendor/datatables/extras/TableTools/pdfmake-0.1.32/pdfmake.min.js"></script>
<script src="../../vendor/datatables/extras/TableTools/pdfmake-0.1.32/vfs_fonts.js"></script>

<script src="../../js/examples/examples.datatables.default.js"></script>
<script src="../../js/examples/examples.datatables.row.with.details.js"></script>
<script src="../../js/examples/examples.datatables.tabletools.js"></script>


<script src="../../js/theme.js"></script>
<script src="../../js/custom.js"></script>
<script src="../../js/theme.init.js"></script>
<script type="text/javascript" src="../../js/jquery/jquery-3.1.0.min.js"></script>
<script type="text/javascript" src="../../js/jquery.maskedinput.js"></script>
<script type="text/javascript">
    $( document ).ready(function( $ ) {
        $(".telefoninput").mask("(999) 999 99 99",{placeholder:"(___) ___ __ __"});
    });
</script>
<script type="text/javascript" src="../../js/validation_master.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.ajaxFormFalse').validationForm({'ajaxType':false});
        $('.ajaxFormTrue').validationForm({'ajaxType':true,'ajaxRefreshPage':true});
    })
</script>
<script type="text/javascript">
    $(".videoeklebtn").click(function()
    {
        $(".videolar").append(' <div class="col-md-8  mb-3"><label class="form-label"><?=admin_modul_firmalar_video?></label><input class="form-control vRequired" name="video[]" value="" /></div>')

    })
</script>
<script type="text/javascript">
    $(".ozellikeklebtn").click(function()
    {
        $(".ozellikler").append(' <div class="col-md-8  mb-3"><label class="form-label"><?=admin_modul_firmalar_ozellik?></label><input class="form-control vRequired" name="ozellik[]" value="" /></div>')

    })
</script>

<script type="text/javascript">
    $(".fiyatekle").click(function()
    {
        $(".fiyatlar").append('<div class="col-md-6  mb-3"><label class="form-label"><?=admin_modul_firmalar_fiyatlandirma_alt_baslik?></label><input class="form-control vRequired" name="fiyat_baslik[]" value="" /></div><div class="col-md-6  mb-3"><label class="form-label"><?=admin_modul_firmalar_fiyatlandirma_fiyat?></label><input class="form-control vRequired" name="fiyat_baslik_fiyat[]" value="" /></div>')

    })
</script>

<script type="text/javascript">
    $(document).ready(function(){
        $(".silbtn").click(function () {
            var href = $(this).attr("data-href");
            $(".mdlsilbtn").attr("href",href);
        })
    })
</script>
<script type="text/javascript">
    $(".selectable-all").click(function(){
        $('.selectable-item').not(this).prop('checked', this.checked);
    });
</script>
<script src="../../../ckeditor/ckeditor.js"></script>
<script>
    initSample();
</script>

</body>
</html>