<?php
include "../../class/ayarlar.php";
?>
<!doctype html>
<html class="fixed">
<head>
    <meta charset="UTF-8">
    <title><?=$db->VeriOkuTek("ayarlar","sitebaslik","id",1)?></title>
    <meta name="keywords" content="<?=$db->VeriOkuTek("ayarlar","sitebaslik","id",1)?>"/>
    <meta name="description" content="<?=$db->VeriOkuTek("ayarlar","sitebaslik","id",1)?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">
    <!-- Vendor CSS -->
    <link rel="stylesheet" href="../../vendor/bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" href="../../vendor/animate/animate.compat.css">
    <link rel="stylesheet" href="../../vendor/font-awesome/css/all.min.css"/>
    <link rel="stylesheet" href="../../vendor/boxicons/css/boxicons.min.css"/>
    <link rel="stylesheet" href="../../vendor/magnific-popup/magnific-popup.css"/>
    <link rel="stylesheet" href="../../vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css"/>
    <link rel="stylesheet" href="../../vendor/bootstrap-fileupload/bootstrap-fileupload.min.css"/>
    <link rel="stylesheet" href="../../css/theme.css"/>
    <link rel="stylesheet" href="../../css/custom.css">
    <script src="../../vendor/modernizr/modernizr.js"></script>
    <link rel="stylesheet" href="../../vendor/datatables/media/css/dataTables.bootstrap5.css"/>
    <link rel="stylesheet" href="../../vendor/simple-line-icons/css/simple-line-icons.css"/>
</head>
<body>
<section class="body">
    <?php
    include "../include/header.php";
    include "../../class/$class";
    $modul=new Modul();
    ?>
    <div class="inner-wrapper">
        <?php
        include "../include/menu.php";
        ?>

        <section role="main" class="content-body " style="padding: 7px">
            <header class="page-header">
                <h2><?=$modul->pagealtitle[0]?></h2>

                <div class="right-wrapper text-end">
                    <ol class="breadcrumbs">
                        <li><a href="<?=$siteURL?>"><i class="bx bx-home-alt"></i></a></li>
                        <li><span><?=$modul->breadcrumb[1]?></span></li>
                        <?php
                        if(isset($_GET["id"]))
                        {
                            ?>
                            <li><span><?=$modul->breadcrumb[2]?></span></li>
                            <?php
                        }
                        else
                        {
                            ?>
                            <li><span><?=$modul->breadcrumb[3]?></span></li>
                            <?php
                        }
                        ?>


                    </ol>
                    <a class="sidebar-right-toggle"><i class="fas fa-chevron-left"></i></a>
                </div>
            </header>

            <!-- start: page -->
            <div class="row">
                <div class="col">
                    <section class="card">
                        <div class="card-body">
                            <div class="col-lg-12">
                                <?php
                                if($_POST)
                                {
                                    if(isset($_GET["id"]))
                                    {
                                        $modul->Update();
                                    }
                                    else
                                    {
                                        $modul->Insert();
                                    }
                                }
                                if(isset($_GET["id"]))
                                {
                                    $liste=$db->VeriOkuCoklu($modul->tablo,array($modul->primary),array($_GET["id"]));
                                    $yetkiler = (array) json_decode($liste[0]->yetkiler);
                                }

                                ?>
                            </div>
                            <form class="ajaxFormFalse" method="POST" enctype="multipart/form-data">

                                <div class="row">




                                    <div class="col-md-6  mb-3">
                                        <label class="form-label"><?=admin_modul_editorler_kullanici_adi?></label>
                                        <input class="form-control vRequired" name="kullanici_adi" value="<?=isset($_GET["id"])?$liste[0]->kullanici_adi:''?>"/>
                                    </div>
                                    <div class="col-md-6  mb-3">
                                        <label class="form-label"><?=admin_modul_editorler_sifre?></label>
                                        <input class="form-control vRequired" name="sifre" value="<?=isset($_GET["id"])?$liste[0]->sifre:''?>"/>
                                    </div>
                                    <div class="col-md-12  mb-3">
                                        <h3>Yetkiler</h3>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-check">
                                                    <input class="form-check-input" <?=in_array("raporlar.php", $yetkiler)?'checked':''?> name="yetkiler[]" type="checkbox" value="raporlar.php" id="flexCheckDefault">
                                                    <label class="form-check-label" for="flexCheckDefault"><?=admin_menu_raporlar?></label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-check">
                                                    <input class="form-check-input" <?=in_array("genelayarlar.php", $yetkiler)?'checked':''?> name="yetkiler[]" type="checkbox" value="genelayarlar.php" id="flexCheckDefault">
                                                    <label class="form-check-label" for="flexCheckDefault"><?=admin_menu_genel_ayarlar?></label>
                                                </div>
                                            </div>





                                            <div class="col-md-4">
                                                <div class="form-check">
                                                    <input class="form-check-input" <?=in_array("dilayarlari.php", $yetkiler)?'checked':''?> name="yetkiler[]" type="checkbox" value="dilayarlari.php" id="flexCheckDefault">
                                                    <label class="form-check-label" for="flexCheckDefault"><?=admin_menu_dil_ayarlari?></label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-check">
                                                    <input class="form-check-input" <?=in_array("smtpayarlari.php", $yetkiler)?'checked':''?> name="yetkiler[]" type="checkbox" value="smtpayarlari.php" id="flexCheckDefault">
                                                    <label class="form-check-label" for="flexCheckDefault"><?=admin_menu_smtp_bilgileri?></label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-check">
                                                    <input class="form-check-input" <?=in_array("sehirler.php", $yetkiler)?'checked':''?> name="yetkiler[]" type="checkbox" value="sehirler.php" id="flexCheckDefault">
                                                    <label class="form-check-label" for="flexCheckDefault"><?=admin_menu_sehirler?></label>
                                                </div>
                                            </div>






                                            <div class="col-md-4">
                                                <div class="form-check">
                                                    <input class="form-check-input" <?=in_array("editorler.php", $yetkiler)?'checked':''?> name="yetkiler[]" type="checkbox" value="editorler.php" id="flexCheckDefault">
                                                    <label class="form-check-label" for="flexCheckDefault"><?=admin_menu_editorler?></label>
                                                </div>
                                            </div>


                                            <div class="col-md-4">
                                                <div class="form-check">
                                                    <input class="form-check-input" <?=in_array("menuler.php", $yetkiler)?'checked':''?> name="yetkiler[]" type="checkbox" value="menuler.php" id="flexCheckDefault">
                                                    <label class="form-check-label" for="flexCheckDefault"><?=admin_menu_menuler?></label>
                                                </div>
                                            </div>




                                            <div class="col-md-4">
                                                <div class="form-check">
                                                    <input class="form-check-input" <?=in_array("uyeler.php", $yetkiler)?'checked':''?> name="yetkiler[]" type="checkbox" value="uyeler.php" id="flexCheckDefault">
                                                    <label class="form-check-label" for="flexCheckDefault"><?=admin_menu_uyeler?></label>
                                                </div>
                                            </div>


                                            <div class="col-md-4">
                                                <div class="form-check">
                                                    <input class="form-check-input" <?=in_array("firmalar.php", $yetkiler)?'checked':''?> name="yetkiler[]" type="checkbox" value="firmalar.php" id="flexCheckDefault">
                                                    <label class="form-check-label" for="flexCheckDefault"><?=admin_menu_firmalar?></label>
                                                </div>
                                            </div>


                                            <div class="col-md-4">
                                                <div class="form-check">
                                                    <input class="form-check-input" <?=in_array("firma-kategorileri.php", $yetkiler)?'checked':''?> name="yetkiler[]" type="checkbox" value="firma-kategorileri.php" id="flexCheckDefault">
                                                    <label class="form-check-label" for="flexCheckDefault"><?=admin_menu_firma_kategorileri?></label>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-check">
                                                    <input class="form-check-input" <?=in_array("firma-yorumlari.php", $yetkiler)?'checked':''?> name="yetkiler[]" type="checkbox" value="firma-yorumlari.php" id="flexCheckDefault">
                                                    <label class="form-check-label" for="flexCheckDefault"><?=admin_menu_firma_yorumlari?></label>
                                                </div>
                                            </div>



                                            <div class="col-md-4">
                                                <div class="form-check">
                                                    <input class="form-check-input" <?=in_array("mekanlar.php", $yetkiler)?'checked':''?> name="yetkiler[]" type="checkbox" value="mekanlar.php" id="flexCheckDefault">
                                                    <label class="form-check-label" for="flexCheckDefault"><?=admin_menu_mekanlar?></label>
                                                </div>
                                            </div>


                                            <div class="col-md-4">
                                                <div class="form-check">
                                                    <input class="form-check-input" <?=in_array("mekan-kategorileri.php", $yetkiler)?'checked':''?> name="yetkiler[]" type="checkbox" value="mekan-kategorileri.php" id="flexCheckDefault">
                                                    <label class="form-check-label" for="flexCheckDefault"><?=admin_menu_mekan_kategorileri?></label>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-check">
                                                    <input class="form-check-input" <?=in_array("mekan-yorumlari.php", $yetkiler)?'checked':''?> name="yetkiler[]" type="checkbox" value="mekan-yorumlari.php" id="flexCheckDefault">
                                                    <label class="form-check-label" for="flexCheckDefault"><?=admin_menu_mekan_yorumlari?></label>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-check">
                                                    <input class="form-check-input" <?=in_array("blog.php", $yetkiler)?'checked':''?> name="yetkiler[]" type="checkbox" value="blog.php" id="flexCheckDefault">
                                                    <label class="form-check-label" for="flexCheckDefault"><?=admin_menu_blog?></label>
                                                </div>
                                            </div>





                                            <div class="col-md-4">
                                                <div class="form-check">
                                                    <input class="form-check-input" <?=in_array("dugun-hikayeleri.php", $yetkiler)?'checked':''?> name="yetkiler[]" type="checkbox" value="dugun-hikayeleri.php" id="flexCheckDefault">
                                                    <label class="form-check-label" for="flexCheckDefault"><?=admin_menu_dugun_hikayeleri?></label>
                                                </div>
                                            </div>


                                            <div class="col-md-4">
                                                <div class="form-check">
                                                    <input class="form-check-input" <?=in_array("ajanda-sureleri.php", $yetkiler)?'checked':''?> name="yetkiler[]" type="checkbox" value="ajanda-sureleri.php" id="flexCheckDefault">
                                                    <label class="form-check-label" for="flexCheckDefault"><?=admin_menu_ajanda_sureleri?></label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-check">
                                                    <input class="form-check-input" <?=in_array("ajanda-basliklar.php", $yetkiler)?'checked':''?> name="yetkiler[]" type="checkbox" value="ajanda-basliklar.php" id="flexCheckDefault">
                                                    <label class="form-check-label" for="flexCheckDefault"><?=admin_menu_ajanda_basliklari?></label>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-check">
                                                    <input class="form-check-input" <?=in_array("butce-planlama-kategoriler.php", $yetkiler)?'checked':''?> name="yetkiler[]" type="checkbox" value="butce-planlama-kategoriler.php" id="flexCheckDefault">
                                                    <label class="form-check-label" for="flexCheckDefault"><?=admin_menu_butce_planlama_kategorileri?></label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-check">
                                                    <input class="form-check-input" <?=in_array("butce-planlama.php", $yetkiler)?'checked':''?> name="yetkiler[]" type="checkbox" value="butce-planlama.php" id="flexCheckDefault">
                                                    <label class="form-check-label" for="flexCheckDefault"><?=admin_menu_butce_planlama_basliklari?></label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-check">
                                                    <input class="form-check-input" <?=in_array("seo-ayarlari.php", $yetkiler)?'checked':''?> name="yetkiler[]" type="checkbox" value="seo-ayarlari.php" id="flexCheckDefault">
                                                    <label class="form-check-label" for="flexCheckDefault"><?=admin_menu_seo_ayarlari?></label>
                                                </div>
                                            </div>


                                            <div class="col-md-4">
                                                <div class="form-check">
                                                    <input class="form-check-input" <?=in_array("genel-ozellik-kategorileri.php", $yetkiler)?'checked':''?> name="yetkiler[]" type="checkbox" value="genel-ozellik-kategorileri.php" id="flexCheckDefault">
                                                    <label class="form-check-label" for="flexCheckDefault"><?=admin_menu_genel_kategori_ozellikleri?></label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-check">
                                                    <input class="form-check-input" <?=in_array("genel-ozellikler.php", $yetkiler)?'checked':''?> name="yetkiler[]" type="checkbox" value="genel-ozellikler.php" id="flexCheckDefault">
                                                    <label class="form-check-label" for="flexCheckDefault"><?=admin_menu_genel_ozellikler?></label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-check">
                                                    <input class="form-check-input" <?=in_array("ozellikler.php", $yetkiler)?'checked':''?> name="yetkiler[]" type="checkbox" value="ozellikler.php" id="flexCheckDefault">
                                                    <label class="form-check-label" for="flexCheckDefault"><?=admin_modul_filtre_ozellikleri?></label>
                                                </div>
                                            </div>



                                            {
                                            "genelayarlar.php":"genelayarlar.php",
                                            "sehirler.php":"sehirler.php",
                                            "dilayarlari.php":"dilayarlari.php",
                                            "smtpayarlari.php":"smtpayarlari.php",
                                            "editorler.php":"editorler.php",
                                            "menuler.php":"menuler.php",
                                            "uyeler.php":"uyeler.php",
                                            "firmalar.php":"firmalar.php",
                                            "firma-kategorileri.php":"firma-kategorileri.php",
                                            "firma-yorumlari.php":"firma-yorumlari.php",
                                            "mekanlar.php":"mekanlar.php",
                                            "mekan-kategorileri.php":"mekan-kategorileri.php",
                                            "mekan-yorumlari.php":"mekan-yorumlari.php",
                                            "blog.php":"blog.php",
                                            "dugun-hikayeleri.php":"dugun-hikayeleri.php",
                                            "ajanda-sureleri.php":"ajanda-sureleri.php",
                                            "ajanda-basliklar.php":"ajanda-basliklar.php",
                                            "butce-planlama-kategoriler.php":"butce-planlama-kategoriler.php",
                                            "butce-planlama.php":"butce-planlama.php",
                                            "seo-ayarlari.php":"seo-ayarlari.php",
                                            "genel-ozellik-kategorileri.php":"genel-ozellik-kategorileri.php",
                                            "genel-ozellikler.php":"genel-ozellikler.php",
                                            "ozellikler.php":"ozellikler.php"
                                            }



                                        </div>


                                    </div>

                                </div>
                                <button type="submit" class="btn btn-primary submitbtn"><?=admin_genel_gonder?></button>
                            </form>
                        </div>
                    </section>
                </div>
            </div>

        </section>
    </div>


</section>

<script src="../../vendor/jquery/jquery.js"></script>
<script src="../../vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
<script src="../../vendor/jquery-cookie/jquery.cookie.js"></script>
<script src="../../vendor/popper/umd/popper.min.js"></script>
<script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../../vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="../../vendor/common/common.js"></script>
<script src="../../vendor/nanoscroller/nanoscroller.js"></script>
<script src="../../vendor/magnific-popup/jquery.magnific-popup.js"></script>
<script src="../../vendor/jquery-placeholder/jquery.placeholder.js"></script>
<script src="../../vendor/autosize/autosize.js"></script>
<script src="../../vendor/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>


<script src="../../vendor/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="../../vendor/datatables/media/js/dataTables.bootstrap5.min.js"></script>
<script src="../../vendor/datatables/extras/TableTools/Buttons-1.4.2/js/dataTables.buttons.min.js"></script>
<script src="../../vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.bootstrap4.min.js"></script>
<script src="../../vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.html5.min.js"></script>
<script src="../../vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.print.min.js"></script>
<script src="../../vendor/datatables/extras/TableTools/JSZip-2.5.0/jszip.min.js"></script>
<script src="../../vendor/datatables/extras/TableTools/pdfmake-0.1.32/pdfmake.min.js"></script>
<script src="../../vendor/datatables/extras/TableTools/pdfmake-0.1.32/vfs_fonts.js"></script>

<script src="../../js/examples/examples.datatables.default.js"></script>
<script src="../../js/examples/examples.datatables.row.with.details.js"></script>
<script src="../../js/examples/examples.datatables.tabletools.js"></script>


<script src="../../js/theme.js"></script>
<script src="../../js/custom.js"></script>
<script src="../../js/theme.init.js"></script>
<script type="text/javascript" src="../../js/jquery/jquery-3.1.0.min.js"></script>
<script type="text/javascript" src="../../js/jquery.maskedinput.js"></script>
<script type="text/javascript">
    $(document).ready(function($)
    {
        $(".telefoninput").mask("(999) 999 99 99", {placeholder : "(___) ___ __ __"});
    });
</script>
<script type="text/javascript" src="../../js/validation_master.js"></script>
<script type="text/javascript">
    $(document).ready(function()
    {
        $('.ajaxFormFalse').validationForm({'ajaxType' : false});
        $('.ajaxFormTrue').validationForm({'ajaxType' : true, 'ajaxRefreshPage' : true});
    })
</script>

<script type="text/javascript">
    $(document).ready(function()
    {
        $(".silbtn").click(function()
        {
            var href = $(this).attr("data-href");
            $(".mdlsilbtn").attr("href", href);
        })
    })
</script>
<script type="text/javascript">
    $(".selectable-all").click(function()
    {
        $('.selectable-item').not(this).prop('checked', this.checked);
    });
</script>

</body>
</html>