<?php
include"../../class/ayarlar.php";
?>
<!doctype html>
<html class="fixed">
<head>
    <meta charset="UTF-8">
    <title><?=$db->VeriOkuTek ("ayarlar","sitebaslik","id",1)?></title>
    <meta name="keywords" content="<?=$db->VeriOkuTek ("ayarlar","sitebaslik","id",1)?>" />
    <meta name="description" content="<?=$db->VeriOkuTek ("ayarlar","sitebaslik","id",1)?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">
    <!-- Vendor CSS -->
    <link rel="stylesheet" href="../../vendor/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="../../vendor/animate/animate.compat.css">
    <link rel="stylesheet" href="../../vendor/font-awesome/css/all.min.css" />
    <link rel="stylesheet" href="../../vendor/boxicons/css/boxicons.min.css" />
    <link rel="stylesheet" href="../../vendor/magnific-popup/magnific-popup.css" />
    <link rel="stylesheet" href="../../vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css" />
    <link rel="stylesheet" href="../../vendor/bootstrap-fileupload/bootstrap-fileupload.min.css" />
    <link rel="stylesheet" href="../../css/theme.css" />
    <link rel="stylesheet" href="../../css/custom.css">
    <script src="../../vendor/modernizr/modernizr.js"></script>
    <link rel="stylesheet" href="../../vendor/select2/css/select2.css" />
    <link rel="stylesheet" href="../../vendor/select2-bootstrap-theme/select2-bootstrap.min.css" />
    <link rel="stylesheet" href="../../vendor/bootstrap-multiselect/css/bootstrap-multiselect.css" />
    <link rel="stylesheet" href="../../vendor/bootstrap-tagsinput/bootstrap-tagsinput.css" />
</head>
<body>
<section class="body">
    <?php
    include"../include/header.php";
    include"../../class/$class";
    $modul = new Modul();
    ?>
    <div style="display: none; right: 0px; bottom: 15px; position: fixed; z-index: 9999; background-color: #0ea15b; color:#fff; padding: 15px" class="kayitekleniyor">
        İşlem yapılıyor lütfen bekleyin..
    </div>
    <div class="inner-wrapper">
        <?php
        include"../include/menu.php";
        ?>

        <section role="main" class="content-body " style="padding: 7px">
            <header class="page-header">
                <h2><?=$modul->pagealtitle[0]?></h2>

                <div class="right-wrapper text-end">
                    <ol class="breadcrumbs">
                        <li><a href="<?=$siteURL?>/admin"><i class="bx bx-home-alt"></i></a></li>
                        <li><span><?=$modul->breadcrumb[1]?></span></li>
                    </ol>
                    <a class="sidebar-right-toggle" ><i class="fas fa-chevron-left"></i></a>
                </div>
            </header>

            <!-- start: page -->
            <div class="row">
                <div class="col">
                    <section class="card">
                        <div class="card-body">
                            <div class="col-lg-12">
                                <?php
                                if($_POST)
                                {
                                    $modul->Update ();
                                }
                                $liste= $db->VeriOkuCoklu ($modul->tablo,array($modul->primary),array(1));
                                ?>
                            </div>
                            <form class="ajaxFormFalse" method="POST" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label class="form-label"><?=admin_smtp_modul_host?></label>
                                        <input value="<?=$liste[0]->smtphost?>" name="smtphost" type="text" id="form-field-1" class="form-control vRequired">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="form-label"><?=admin_smtp_modul_user?></label>
                                        <input value="<?=$liste[0]->smtpuser?>" name="smtpuser" type="text" id="form-field-1" class="form-control vRequired">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="form-label"><?=admin_smtp_modul_sifre?></label>
                                        <input id="tags_1" value="<?=$liste[0]->smtpsifre?>" name="smtpsifre" type="text" class="form-control">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="form-label"><?=admin_smtp_modul_port?></label>
                                        <input type="text" value="<?=$liste[0]->smtpport?>" name="smtpport" id="form-field-15" class="form-control">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="form-label"><?=admin_smtp_modul_guvenlik?></label>
                                        <select name="smtpguvenlik" class="form-control">
                                            <option <?=$liste[0]->smtpguvenlik=='TLS'?'selected':''?> value="TLS">TLS</option>
                                            <option <?=$liste[0]->smtpguvenlik=='SSL'?'selected':''?> value="SSL">SSL</option>
                                        </select>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary submitbtn"><?=admin_genel_gonder?></button>
                            </form>
                        </div>
                    </section>
                </div>
            </div>

        </section>
    </div>


</section>

<script src="../../vendor/jquery/jquery.js"></script>
<script src="../../vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
<script src="../../vendor/jquery-cookie/jquery.cookie.js"></script>
<script src="../../vendor/popper/umd/popper.min.js"></script>
<script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../../vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="../../vendor/common/common.js"></script>
<script src="../../vendor/nanoscroller/nanoscroller.js"></script>
<script src="../../vendor/magnific-popup/jquery.magnific-popup.js"></script>
<script src="../../vendor/jquery-placeholder/jquery.placeholder.js"></script>
<script src="../../vendor/autosize/autosize.js"></script>
<script src="../../vendor/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
<script src="../../js/theme.js"></script>
<script src="../../js/custom.js"></script>
<script src="../../js/theme.init.js"></script>
<script type="text/javascript" src="../../js/jquery/jquery-3.1.0.min.js"></script>
<script type="text/javascript" src="../../js/jquery.maskedinput.js"></script>
<script type="text/javascript">
    $( document ).ready(function( $ ) {
        $(".telefoninput").mask("(999) 999 99 99",{placeholder:"(___) ___ __ __"});
    });
</script>
<script type="text/javascript" src="../../js/validation_master.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.ajaxFormFalse').validationForm({'ajaxType':false});
        $('.ajaxFormTrue').validationForm({'ajaxType':true,'ajaxRefreshPage':true});
    })
</script>

</body>
</html>