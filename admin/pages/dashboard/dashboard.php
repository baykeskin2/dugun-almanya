<?php
include"../../class/ayarlar.php";
?>
<!doctype html>
<html class="fixed">
<head>
    <meta charset="UTF-8">
    <title><?=$db->VeriOkuTek ("ayarlar","sitebaslik","id",1)?></title>
    <meta name="keywords" content="<?=$db->VeriOkuTek ("ayarlar","sitebaslik","id",1)?>" />
    <meta name="description" content="<?=$db->VeriOkuTek ("ayarlar","sitebaslik","id",1)?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">
    <!-- Vendor CSS -->
    <link rel="stylesheet" href="../../vendor/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="../../vendor/animate/animate.compat.css">
    <link rel="stylesheet" href="../../vendor/font-awesome/css/all.min.css" />
    <link rel="stylesheet" href="../../vendor/boxicons/css/boxicons.min.css" />
    <link rel="stylesheet" href="../../vendor/magnific-popup/magnific-popup.css" />
    <link rel="stylesheet" href="../../vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css" />
    <link rel="stylesheet" href="../../vendor/bootstrap-fileupload/bootstrap-fileupload.min.css" />
    <link rel="stylesheet" href="../../css/theme.css" />
    <link rel="stylesheet" href="../../css/custom.css">
    <script src="../../vendor/modernizr/modernizr.js"></script>
    <link rel="stylesheet" href="../../vendor/datatables/media/css/dataTables.bootstrap5.css" />
    <link rel="stylesheet" href="../../vendor/simple-line-icons/css/simple-line-icons.css" />
</head>
<body>
<section class="body">
    <?php
    include"../include/header.php";
    include"../../class/$class";
    $modul = new Modul();
    ?>
    <div class="inner-wrapper">
        <?php
        include"../include/menu.php";
        ?>

        <section role="main" class="content-body " style="padding: 7px">
            <header class="page-header">
                <h2><?=$modul->pagealtitle[0]?></h2>

                <div class="right-wrapper text-end">
                    <ol class="breadcrumbs">
                        <li><a href="<?=$siteURL?>"><i class="bx bx-home-alt"></i></a></li>
                        <li><span><?=$modul->breadcrumb[1]?></span></li>
                    </ol>
                    <a class="sidebar-right-toggle" ><i class="fas fa-chevron-left"></i></a>
                </div>
            </header>

            <!-- start: page -->
            <div class="row">
                <div class="col">
                    <section class="card">
                        <div class="card-body">
                            <div class="col-lg-12">
                                <?php
                                if($_POST)
                                {
                                    if(isset($_POST["islem"]))
                                    {
                                        if($_POST["islem"]=="Duzenle")
                                        {
                                            $modul->Update ();
                                        }else
                                        {
                                            $modul->Insert ();
                                        }
                                    }else
                                    {
                                        $modul->Delete_All();
                                    }
                                }
                                if(isset($_GET["islem"]))
                                {
                                    if($_GET["islem"]=="sil")
                                    {
                                        $modul->Delete();
                                    }
                                    if($_GET["islem"]=="onay")
                                    {
                                        $modul->Onay();
                                    }
                                    if($_GET["islem"]=="iptal")
                                    {
                                        $modul->Iptal();
                                    }
                                }
                                ?>
                            </div>

                            <form method="GET" action="" style="margin-bottom: 30px">

                                    <section class="card mb-4">
                                        <header class="card-header">
                                            <div class="card-actions">
                                                <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
                                                <a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
                                            </div>
                                            <h2 class="card-title">Liman / Durum / Cins</h2>
                                        </header>
                                        <div class="card-body">
                                            <div class="row">
                                            <div class="col-md-4">
                                                <label>Liman Seçiniz</label>
                                                <select class="form-control" name="baglama_limani_id">
                                                    <option value="">Lütfen Seçiniz</option>
                                                    <?php
                                                    $limanlar = $db->VeriOkuCoklu("baglama_limanlari");
                                                    if($limanlar===false)
                                                    {

                                                    }else
                                                    {
                                                        foreach($limanlar as $l)
                                                        {
                                                            ?>
                                                            <option <?=isset($_GET["baglama_limani_id"])?$_GET["baglama_limani_id"]==$l->id?'selected':'':''?> value="<?=$l->id?>"><?=$l->baslik?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Durum Seçiniz</label>
                                                <select class="form-control" name="durum">
                                                    <option value="">Lütfen Seçiniz</option>
                                                    <option <?=isset($_GET["durum"])?$_GET["durum"]==0?'selected':'':''?> value="0">SMS Doğrulaması Bekleniyor</option>
                                                    <option <?=isset($_GET["durum"])?$_GET["durum"]==2?'selected':'':''?> value="2">İşlem Bekliyor </option>
                                                    <option <?=isset($_GET["durum"])?$_GET["durum"]==3?'selected':'':''?> value="3">Ödeme İşlemde</option>
                                                    <option <?=isset($_GET["durum"])?$_GET["durum"]==1?'selected':'':''?> value="1">Onaylandı</option>
                                                    <option <?=isset($_GET["durum"])?$_GET["durum"]==4?'selected':'':''?> value="4">Reddedildi</option>
                                                </select>
                                            </div>

                                            <div class="col-md-4">
                                                <label>Cins Seçiniz</label>
                                                <select class="form-control" name="cins">
                                                    <option value="">Lütfen Seçiniz</option>
                                                    <option <?=isset($_GET["cins"])?$_GET["cins"]=='Özel'?'selected':'':''?> value="Özel">Özel</option>
                                                    <option <?=isset($_GET["cins"])?$_GET["cins"]=='Ticari'?'selected':'':''?> value="Ticari">Ticari</option>
                                                    <option <?=isset($_GET["cins"])?$_GET["cins"]=='Balıkçı'?'selected':'':''?> value="Balıkçı">Balıkçı</option>
                                                    <option <?=isset($_GET["cins"])?$_GET["cins"]=='Hizmet'?'selected':'':''?> value="Hizmet">Hizmet</option>
                                                </select>
                                            </div>
                                            </div>
                                        </div>
                                    </section>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <section class="card mb-4">
                                                <header class="card-header">
                                                    <div class="card-actions">
                                                        <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
                                                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
                                                    </div>
                                                    <h2 class="card-title">Boy</h2>
                                                </header>
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label>Minimum Boy</label>
                                                            <input type="number" class="form-control" value="<?=isset($_GET["min_boy"])?$_GET["min_boy"]:''?>" name="min_boy">
                                                        </div>

                                                        <div class="col-md-6">
                                                            <label>Maximum Boy</label>
                                                            <input type="number" class="form-control" value="<?=isset($_GET["max_boy"])?$_GET["max_boy"]:''?>" name="max_boy">
                                                        </div>
                                                    </div>
                                            </section>
                                        </div>

                                        <div class="col-md-6">
                                            <section class="card mb-4">
                                                <header class="card-header">
                                                    <div class="card-actions">
                                                        <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
                                                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
                                                    </div>
                                                    <h2 class="card-title">En</h2>
                                                </header>
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label>Minimum En</label>
                                                            <input type="number" class="form-control" value="<?=isset($_GET["min_en"])?$_GET["min_en"]:''?>" name="min_en">
                                                        </div>

                                                        <div class="col-md-6">
                                                            <label>Maximum En</label>
                                                            <input type="number" class="form-control" value="<?=isset($_GET["max_en"])?$_GET["max_en"]:''?>" name="max_en">
                                                        </div>
                                                    </div>
                                            </section>
                                        </div>


                                        <div class="col-md-6">
                                            <section class="card mb-4">
                                                <header class="card-header">
                                                    <div class="card-actions">
                                                        <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
                                                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
                                                    </div>
                                                    <h2 class="card-title">Su Çekimi</h2>
                                                </header>
                                                <div class="card-body">
                                                    <div class="row">

                                                        <div class="col-md-6">
                                                            <label>Minimum Su Çekimi</label>
                                                            <input type="number" class="form-control" value="<?=isset($_GET["min_su_cekimi"])?$_GET["min_su_cekimi"]:''?>" name="min_su_cekimi">
                                                        </div>

                                                        <div class="col-md-6">
                                                            <label>Maximum Su Çekimi</label>
                                                            <input type="number" class="form-control" value="<?=isset($_GET["max_su_cekimi"])?$_GET["max_su_cekimi"]:''?>" name="max_su_cekimi">
                                                        </div>
                                                    </div>
                                            </section>
                                        </div>

                                        <div class="col-md-6">
                                            <section class="card mb-4">
                                                <header class="card-header">
                                                    <div class="card-actions">
                                                        <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
                                                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
                                                    </div>
                                                    <h2 class="card-title">Tarih Aralığı</h2>
                                                </header>
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label>Bağlama Tarihi(Başlangıç)</label>
                                                            <input type="date" class="form-control" value="<?=isset($_GET["baglama_tarihi_min"])?$_GET["baglama_tarihi_min"]:''?>" name="baglama_tarihi_min">
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label>Bağlama Tarihi(Bitiş)</label>
                                                            <input type="date" class="form-control" value="<?=isset($_GET["baglama_tarihi_min"])?$_GET["baglama_tarihi_maks"]:''?>" name="baglama_tarihi_maks">
                                                        </div>
                                                    </div>
                                            </section>
                                        </div>

                                        <div class="col-md-12">
                                            <button style="width: 100%; margin-top: 20px" type="submit" class="btn btn-primary">Getir</button>
                                        </div>
                                    </div>

                            </form>
                            <?php
                            $url = $_SERVER["REQUEST_URI"];
                            $urlbol = explode("?",$url);
                            if(isset($urlbol[1]))
                            {
                                ?>
                                <div class="col-md-12" style="text-align: right; margin-bottom: 12px;">
                                    <a href="../../../include/exel/disariaktar.php?<?=$urlbol[1]?>" class="btn btn-primary">Kayıtları Dışa Aktar</a>
                                </div>
                                <?php
                            }else
                            {
                                ?>
                                <div class="col-md-12" style="text-align: right; margin-bottom: 12px;">
                                    <a href="../../../include/exel/disariaktar.php" class="btn btn-primary">Kayıtları Dışa Aktar</a>
                                </div>
                                <?php
                            }
                            ?>



                            <form method="POST">
                                <table class="table table-bordered table-striped mb-0" id="datatable-default">

                                    <thead>
                                    <tr>
                                        <th>Tekne Adı</th>
                                        <th>Başvuru Sahibi</th>
                                        <th>Durum</th>
                                        <th>Bağlama Yeri</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $toplam = 0;
                                    $menu =$modul->All();
                                    if($menu==false)
                                    {

                                    }else
                                    {

                                        foreach($menu as $row)
                                        {
                                            $yetkiler = array();
                                            if($_SESSION["kullanici_yetki"]!="admin")
                                            {
                                                $sorgu = $db->VeriOkuCoklu("kategori_yetkili",array("yetkili_id","kategori_id"),array($_SESSION["admin_kullanici_id"],$row->baglama_yeri_id));
                                                $yetkiler[]=$sorgu[0]->gorme;
                                                $yetkiler[]=$sorgu[0]->duzenleme;
                                                $yetkiler[]=$sorgu[0]->silme;
                                            }else
                                            {
                                                $yetkiler[]=1;
                                                $yetkiler[]=1;
                                                $yetkiler[]=1;
                                            }
                                            $toplam+=$row->tutar;
                                            if($yetkiler[0]==1)
                                            {
                                                ?>
                                                <tr>
                                                    <td><?=$row->tekne_adi?></td>
                                                    <td><?=$row->ad_soyad_firma?></td>
                                                    <td>
                                                        <?php
                                                        if($row->durum==0)
                                                        {
                                                            echo "SMS Doğrulaması Bekleniyor";
                                                        }
                                                        if($row->durum==2)
                                                        {
                                                            echo "İşlem Bekliyor ";
                                                        }
                                                        if($row->durum==3)
                                                        {
                                                            echo "Ödeme İşlemde ";
                                                        }
                                                        if($row->durum==1)
                                                        {
                                                            echo "Onaylandı";
                                                        }
                                                        if($row->durum==4)
                                                        {
                                                            echo "Reddedildi";
                                                        }
                                                        ?>
                                                    </td>
                                                    <td><?=$db->VeriOkuTek("baglama_yerleri","baslik","id",$row->baglama_yeri_id)?></td>

                                                </tr>
                                                <?php
                                            }


                                        }
                                    }
                                    ?>
                                    </tbody>
                                </table>

                                <div class="col-md-4">

                                    <section class="card card-warning mb-4">
                                        <header class="card-header">
                                            <div class="card-actions">
                                                <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
                                                <a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
                                            </div>

                                            <h2 class="card-title">Toplam Tutar</h2>
                                        </header>
                                        <div class="card-body">
                                            <?=$toplam?> TL
                                        </div>
                                    </section>
                                </div>


                            </form>
                        </div>
                    </section>
                </div>
            </div>

        </section>
    </div>


</section>
<div class="modal fade " id="silmodal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #d2322d; color:#fff; font-weight: bold; font-size: 17px;">
                <h5 class="modal-title mdlsiltitle" id="exampleModalLabel">Kaydı Sil</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <p><strong class="mdltext">Kaydı silmek istediğinizden emin misiniz?</strong></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-bs-dismiss="modal">Hayır</button>
                <a href="#" class="btn btn-danger mdlsilbtn">Evet</a>
            </div>
        </div>
    </div>
</div>
<script src="../../vendor/jquery/jquery.js"></script>
<script src="../../vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
<script src="../../vendor/jquery-cookie/jquery.cookie.js"></script>
<script src="../../vendor/popper/umd/popper.min.js"></script>
<script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../../vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="../../vendor/common/common.js"></script>
<script src="../../vendor/nanoscroller/nanoscroller.js"></script>
<script src="../../vendor/magnific-popup/jquery.magnific-popup.js"></script>
<script src="../../vendor/jquery-placeholder/jquery.placeholder.js"></script>
<script src="../../vendor/autosize/autosize.js"></script>
<script src="../../vendor/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>


<script src="../../vendor/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="../../vendor/datatables/media/js/dataTables.bootstrap5.min.js"></script>
<script src="../../vendor/datatables/extras/TableTools/Buttons-1.4.2/js/dataTables.buttons.min.js"></script>
<script src="../../vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.bootstrap4.min.js"></script>
<script src="../../vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.html5.min.js"></script>
<script src="../../vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.print.min.js"></script>
<script src="../../vendor/datatables/extras/TableTools/JSZip-2.5.0/jszip.min.js"></script>
<script src="../../vendor/datatables/extras/TableTools/pdfmake-0.1.32/pdfmake.min.js"></script>
<script src="../../vendor/datatables/extras/TableTools/pdfmake-0.1.32/vfs_fonts.js"></script>

<script src="../../js/examples/examples.datatables.default.js"></script>
<script src="../../js/examples/examples.datatables.row.with.details.js"></script>
<script src="../../js/examples/examples.datatables.tabletools.js"></script>


<script src="../../js/theme.js"></script>
<script src="../../js/custom.js"></script>
<script src="../../js/theme.init.js"></script>
<script type="text/javascript" src="../../js/jquery/jquery-3.1.0.min.js"></script>
<script type="text/javascript" src="../../js/jquery.maskedinput.js"></script>
<script type="text/javascript">
    $( document ).ready(function( $ ) {
        $(".telefoninput").mask("(999) 999 99 99",{placeholder:"(___) ___ __ __"});
    });
</script>
<script type="text/javascript" src="../../js/validation_master.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.ajaxFormFalse').validationForm({'ajaxType':false});
        $('.ajaxFormTrue').validationForm({'ajaxType':true,'ajaxRefreshPage':true});
    })
</script>

<script type="text/javascript">
    $(document).ready(function(){
        $(".silbtn").click(function () {
            var href = $(this).attr("data-href");

            var text = $(this).attr("data-text");
            var title = $(this).attr("data-title");
            var btntext = $(this).attr("         data-btn-text");




            $(".mdlsilbtn").html(btntext);
            $(".mdlsilbtn").attr("href",href);
            $(".mdlsiltitle").html(title);
            $(".mdltext").html(text);
        })
    })
</script>
<script type="text/javascript">
    $(".selectable-all").click(function(){
        $('.selectable-item').not(this).prop('checked', this.checked);
    });
</script>

</body>
</html>