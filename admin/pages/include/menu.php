<aside id="sidebar-left" class="sidebar-left">
    <div class="sidebar-header">
        <div class="sidebar-title">
            Kontrol Menüsü
        </div>
	
        <div class="sidebar-toggle d-none d-md-block" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
            <i class="fas fa-bars" aria-label="Toggle sidebar"></i>
        </div>
    </div>
    <div class="nano">
        <div class="nano-content">
            <nav id="menu" class="nav-main" role="navigation">
                <ul class="nav nav-main">
                    <?php
                    if(isset($yetkiler["dashboard.php"]))
                    {
                        ?>
                        <li>
                            <a class="nav-link" href="../dashboard/dashboard.php">
                                <i class="fas fa-border-style" aria-hidden="true"></i> <?=admin_menu_raporlar?>
                            </a>
                        </li>
                        <?php
                    }
                    if(isset($yetkiler["genelayarlar.php"])  OR isset($yetkiler["smtpayarlari.php"]) OR isset($yetkiler["seoayarlari.php"]))
                    {
                        ?>
                        <li class="nav-parent">
                            <a class="nav-link" href="#">
                                <i class="fas fa-border-style" aria-hidden="true"></i>
                                <span><?=admin_menu_ayarlar?></span>
                            </a>
                            <ul class="nav nav-children">
                                <?php
                                if(isset($yetkiler["genelayarlar.php"]))
                                {
                                    ?>
                                    <li>
                                        <a class="nav-link" href="../genelayarlar/genelayarlar.php">
                                                <i class="fas fa-arrow-right"></i> <?=admin_menu_genel_ayarlar?>
                                        </a>
                                    </li>
                                    <?php
                                }
                                if(isset($yetkiler["smtpayarlari.php"]))
                                {
                                    ?>
                                    <li>
                                        <a class="nav-link" href="../smtpayarlari/smtpayarlari.php">
                                            <i class="fas fa-arrow-right"></i> <?=admin_menu_smtp_bilgileri?>
                                        </a>
                                    </li>
                                    <?php
                                }
                                if(isset($yetkiler["seo-ayarlari.php"]))
                                {
                                    ?>
                                    <li>
                                        <a class="nav-link" href="../seo-ayarlari/seo-ayarlari.php">
                                            <i class="fas fa-arrow-right"></i> <?=admin_menu_seo_ayarlari?>
                                        </a>
                                    </li>
                                    <?php
                                }
                                ?>
                            </ul>
                        </li>
                        <?php
                    }

                    if(isset($yetkiler["sehirler.php"]) OR isset($yetkiler["dilayarlari.php"]))
                    {
                        ?>
                        <li class="nav-parent">
                            <a class="nav-link" href="#">
                                <i class="fas fa-border-style" aria-hidden="true"></i>
                                <span><?=admin_menu_tanimlar?></span>
                            </a>
                            <ul class="nav nav-children">
                                <?php
                                if(isset($yetkiler["dilayarlari.php"]))
                                {
                                    ?>
                                    <li>
                                        <a class="nav-link" href="../dilayarlari/dilayarlari.php">
                                            <i class="fas fa-arrow-right"></i> <?=admin_menu_dil_ayarlari?>
                                        </a>
                                    </li>
                                    <?php
                                }
                                if(isset($yetkiler["sehirler.php"]))
                                {
                                    ?>
                                    <li>
                                        <a class="nav-link" href="../sehirler/sehirler.php">
                                            <i class="fas fa-arrow-right"></i> <?=admin_menu_sehirler?>
                                        </a>
                                    </li>
                                    <?php
                                }
                                ?>
                            </ul>
                        </li>
                        <?php
                    }
                    if(isset($yetkiler["editorler.php"]))
                    {
                        ?>
                        <li>
                            <a class="nav-link" href="../editorler/editorler.php">
                                <i class="fas fa-border-style" aria-hidden="true"></i> <?=admin_menu_editorler?>
                            </a>
                        </li>
                        <?php
                    }

                    if(isset($yetkiler["menuler.php"]))
                    {
                        ?>
                        <li>
                            <a class="nav-link" href="../menuler/menuler.php">
                                <i class="fas fa-border-style" aria-hidden="true"></i> <?=admin_menu_menuler?>
                            </a>
                        </li>
                        <?php
                    }


                    if(isset($yetkiler["uyeler.php"]))
                    {
                        ?>
                        <li>
                            <a class="nav-link" href="../uyeler/uyeler.php">
                                <i class="fas fa-border-style" aria-hidden="true"></i> <?=admin_menu_uyeler?>
                            </a>
                        </li>
                        <?php
                    }

                    if(isset($yetkiler["firmalar.php"]) OR isset($yetkiler["firma-kategorileri.php"]) OR isset($yetkiler["firma-yorumlari.php"]))
                    {
                        ?>
                        <li class="nav-parent">
                            <a class="nav-link" href="#">
                                <i class="fas fa-border-style" aria-hidden="true"></i>
                                <span><?=admin_menu_firmalar?></span>
                            </a>
                            <ul class="nav nav-children">
                                <?php
                                if(isset($yetkiler["firmalar.php"]))
                                {
                                    ?>
                                    <li>
                                        <a class="nav-link" href="../firmalar/firmalar.php">
                                            <i class="fas fa-arrow-right"></i> <?=admin_menu_firmalar?>
                                        </a>
                                    </li>
                                    <?php
                                }
                                if(isset($yetkiler["firma-kategorileri.php"]))
                                {
                                    ?>
                                    <li>
                                        <a class="nav-link" href="../firma-kategorileri/firma-kategorileri.php">
                                            <i class="fas fa-arrow-right"></i> <?=admin_menu_firma_kategorileri?>
                                        </a>
                                    </li>
                                    <?php
                                }
                                if(isset($yetkiler["firma-yorumlari.php"]))
                                {
                                    ?>
                                    <li>
                                        <a class="nav-link" href="../firma-yorumlari/firma-yorumlari.php">
                                            <i class="fas fa-arrow-right"></i> <?=admin_menu_firma_yorumlari?>
                                        </a>
                                    </li>
                                    <?php
                                }
                                ?>
                            </ul>
                        </li>
                        <?php
                    }


                    if(isset($yetkiler["mekanlar.php"]) OR isset($yetkiler["mekan-kategorileri.php"]) OR isset($yetkiler["mekan-yorumlari.php"]))
                    {
                        ?>
                        <li class="nav-parent">
                            <a class="nav-link" href="#">
                                <i class="fas fa-border-style" aria-hidden="true"></i>
                                <span><?=admin_menu_mekanlar?></span>
                            </a>
                            <ul class="nav nav-children">
                                <?php
                                if(isset($yetkiler["mekanlar.php"]))
                                {
                                    ?>
                                    <li>
                                        <a class="nav-link" href="../mekanlar/mekanlar.php">
                                            <i class="fas fa-arrow-right"></i> <?=admin_menu_mekanlar?>
                                        </a>
                                    </li>
                                    <?php
                                }
                                if(isset($yetkiler["mekan-kategorileri.php"]))
                                {
                                    ?>
                                    <li>
                                        <a class="nav-link" href="../mekan-kategorileri/mekan-kategorileri.php">
                                            <i class="fas fa-arrow-right"></i> <?=admin_menu_mekan_kategorileri?>
                                        </a>
                                    </li>
                                    <?php
                                }
                                if(isset($yetkiler["mekan-yorumlari.php"]))
                                {
                                    ?>
                                    <li>
                                        <a class="nav-link" href="../mekan-yorumlari/mekan-yorumlari.php">
                                            <i class="fas fa-arrow-right"></i> <?=admin_menu_mekan_yorumlari?>
                                        </a>
                                    </li>
                                    <?php
                                }
                                ?>
                            </ul>
                        </li>
                        <?php
                    }

                    if(isset($yetkiler["blog.php"]))
                    {
                        ?>
                        <li>
                            <a class="nav-link" href="../blog/blog.php">
                                <i class="fas fa-border-style" aria-hidden="true"></i> <?=admin_menu_blog?>
                            </a>
                        </li>
                        <?php
                    }
                    if(isset($yetkiler["dugun-hikayeleri.php"]))
                    {
                        ?>
                        <li>
                            <a class="nav-link" href="../dugun-hikayeleri/dugun-hikayeleri.php">
                                <i class="fas fa-border-style" aria-hidden="true"></i> <?=admin_menu_dugun_hikayeleri?>
                            </a>
                        </li>
                        <?php
                    }


                    if(isset($yetkiler["ajanda-sureleri.php"]) OR isset($yetkiler["ajanda-basliklar.php"]))
                    {
                        ?>
                        <li class="nav-parent">
                            <a class="nav-link" href="#">
                                <i class="fas fa-border-style" aria-hidden="true"></i>
                                <span><?=admin_menu_ajanda?></span>
                            </a>
                            <ul class="nav nav-children">
                                <?php
                                if(isset($yetkiler["ajanda-sureleri.php"]))
                                {
                                    ?>
                                    <li>
                                        <a class="nav-link" href="../ajanda-sureleri/ajanda-sureleri.php">
                                            <i class="fas fa-arrow-right"></i> <?=admin_menu_ajanda_sureleri?>
                                        </a>
                                    </li>
                                    <?php
                                }
                                if(isset($yetkiler["ajanda-basliklar.php"]))
                                {
                                    ?>
                                    <li>
                                        <a class="nav-link" href="../ajanda-basliklar/ajanda-basliklar.php">
                                            <i class="fas fa-arrow-right"></i> <?=admin_menu_ajanda_basliklari?>
                                        </a>
                                    </li>
                                    <?php
                                }
                                ?>
                            </ul>
                        </li>
                        <?php
                    }


                    if(isset($yetkiler["butce-planlama.php"]) OR isset($yetkiler["butce-planlama-kategoriler.php"]))
                    {
                        ?>
                        <li class="nav-parent">
                            <a class="nav-link" href="#">
                                <i class="fas fa-border-style" aria-hidden="true"></i>
                                <span><?=admin_menu_butce_planlama?></span>
                            </a>
                            <ul class="nav nav-children">
                                <?php
                                if(isset($yetkiler["butce-planlama-kategoriler.php"]))
                                {
                                    ?>
                                    <li>
                                        <a class="nav-link" href="../butce-planlama-kategoriler/butce-planlama-kategoriler.php">
                                            <i class="fas fa-arrow-right"></i> <?=admin_menu_butce_planlama_kategorileri?>
                                        </a>
                                    </li>
                                    <?php
                                }
                                if(isset($yetkiler["butce-planlama.php"]))
                                {
                                    ?>
                                    <li>
                                        <a class="nav-link" href="../butce-planlama/butce-planlama.php">
                                            <i class="fas fa-arrow-right"></i> <?=admin_menu_butce_planlama_basliklari?>
                                        </a>
                                    </li>
                                    <?php
                                }
                                ?>
                            </ul>
                        </li>
                        <?php
                    }


                    if(isset($yetkiler["genel-ozellik-kategorileri.php"]) OR isset($yetkiler["genel-ozellikler.php"]) OR isset($yetkiler["ozellikler.php"]))
                    {
                        ?>
                        <li class="nav-parent">
                            <a class="nav-link" href="#">
                                <i class="fas fa-border-style" aria-hidden="true"></i>
                                <span><?=admin_modul_ozellikler?></span>
                            </a>
                            <ul class="nav nav-children">
                                <?php
                                if(isset($yetkiler["genel-ozellik-kategorileri.php"]))
                                {
                                    ?>
                                    <li>
                                        <a class="nav-link" href="../genel-ozellik-kategorileri/genel-ozellik-kategorileri.php">
                                            <i class="fas fa-arrow-right"></i> <?=admin_menu_genel_kategori_ozellikleri?>
                                        </a>
                                    </li>
                                    <?php
                                }
                                if(isset($yetkiler["genel-ozellikler.php"]))
                                {
                                    ?>
                                    <li>
                                        <a class="nav-link" href="../genel-ozellikler/genel-ozellikler.php">
                                            <i class="fas fa-arrow-right"></i> <?=admin_menu_genel_ozellikler?>
                                        </a>
                                    </li>
                                    <?php
                                }
                                if(isset($yetkiler["ozellikler.php"]))
                                {
                                    ?>
                                    <li>
                                        <a class="nav-link" href="../ozellikler/ozellikler.php">
                                            <i class="fas fa-arrow-right"></i> <?=admin_modul_filtre_ozellikleri?>
                                        </a>
                                    </li>
                                    <?php
                                }
                                ?>
                            </ul>
                        </li>
                        <?php
                    }
                    ?>
                </ul>
            </nav>



        </div>

        <script>
            // Maintain Scroll Position
            if (typeof localStorage !== 'undefined') {
                if (localStorage.getItem('sidebar-left-position') !== null) {
                    var initialPosition = localStorage.getItem('sidebar-left-position'),
                        sidebarLeft = document.querySelector('#sidebar-left .nano-content');

                    sidebarLeft.scrollTop = initialPosition;
                }
            }
        </script>

    </div>

</aside>