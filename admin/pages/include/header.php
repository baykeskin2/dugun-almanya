<header class="header">
    <div class="logo-container">
        

        <div class="d-md-none toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
            <i class="fas fa-bars" aria-label="Toggle sidebar"></i>
        </div>

    </div>
    <style>
        .select2-container--bootstrap .select2-selection--single
        {
            height: 41px !important;
            padding-top: 10px !important;
        }
    </style>
    <link rel="stylesheet" href="../../vendor/select2/css/select2.css" />
    <link rel="stylesheet" href="../../vendor/select2-bootstrap-theme/select2-bootstrap.min.css" />

    <!-- start: search & user box -->
    <div class="header-right">


        <span class="separator"></span>

        <span class="separator"></span>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong><a href="https://toplutasima.muttas.net/" target="_blank"<span style="color: #1a0cd1;"><?=admin_header_siteyi_goruntule?></span></a></strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <div id="userbox" class="userbox">
            <a href="#" data-bs-toggle="dropdown">
                <figure class="profile-picture">
                    <img src="../../img/%21logged-user.jpg" alt="Joseph Doe" class="rounded-circle" data-lock-picture="img/%21logged-user.jpg" />
                </figure>
                <div class="profile-info" data-lock-name="Hüseyin BAŞOĞLAN" data-lock-email="huseyinbasoglan@mugla.bel.tr">
                    <span class="name">Admin</span>
                    <span class="role">Administrator</span>
                </div>

                <i class="fa custom-caret"></i>
            </a>

            <div class="dropdown-menu">
                <ul class="list-unstyled mb-2">
                    <li class="divider"></li>
                    <li>
                        <a role="menuitem" href="../login/login.php?islem=logout" ></i> <?=admin_header_cikis_yap?></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- end: search & user box -->
</header>