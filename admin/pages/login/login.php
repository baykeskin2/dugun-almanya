<?php
include"../../class/ayarlar.php";
if(isset($_GET["islem"]) AND $_GET["islem"]=="logout")
{
    session_destroy();
}
include"../../class/login/login.php";
$login = new Login();

?>
<!doctype html>
<html class="fixed">
<head>
    <meta charset="UTF-8">
    <title><?=$db->VeriOkuTek ("ayarlar","sitebaslik","id",1)?></title>
    <meta name="keywords" content="<?=$db->VeriOkuTek ("ayarlar","sitebaslik","id",1)?>" />
    <meta name="description" content="<?=$db->VeriOkuTek ("ayarlar","sitebaslik","id",1)?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="../../vendor/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="../../vendor/animate/animate.compat.css">
    <link rel="stylesheet" href="../../vendor/font-awesome/css/all.min.css" />
    <link rel="stylesheet" href="../../vendor/boxicons/css/boxicons.min.css" />
    <link rel="stylesheet" href="../../vendor/magnific-popup/magnific-popup.css" />
    <link rel="stylesheet" href="../../vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css" />
    <link rel="stylesheet" href="../../css/theme.css" />
    <link rel="stylesheet" href="../../css/custom.css">
    <script src="../../vendor/modernizr/modernizr.js"></script>
    <script src="../../master/style-switcher/style.switcher.localstorage.js"></script>
</head>
<body>
<!-- start: page -->
<section class="body-sign">
    <div class="center-sign">


        <div class="panel card-sign">
            <div class="card-title-sign mt-3 text-end">
                <h2 class="title text-uppercase font-weight-bold m-0"><i class="bx bx-user-circle me-1 text-6 position-relative top-5"></i> Giriş Yap</h2>
            </div>
            <div class="card-body">
                <?php
                if($_POST)
                {
                    $login->giris ();
                }
                ?>
                <form action="" method="post">
                    <div class="form-group mb-3">
                        <label><?=admin_login_kullanici_adi?></label>
                        <div class="input-group">
                            <input name="kullanici_adi" type="text" class="form-control form-control-lg" />
                            <span class="input-group-text"><i class="bx bx-user text-4"></i></span>
                        </div>
                    </div>

                    <div class="form-group mb-3">
                        <label><?=admin_login_sifre?></label>
                        <div class="input-group">
                            <input name="sifre" type="password" class="form-control form-control-lg" />
                            <span class="input-group-text"><i class="bx bx-lock text-4"></i></span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-8">

                        </div>
                        <div class="col-sm-4 text-end">
                            <button type="submit" class="btn btn-primary mt-2"><?=admin_login_giris_yap_btn?></button>
                        </div>
                    </div>


                </form>
            </div>
        </div>

        <p class="text-center text-muted mt-3 mb-3">&copy; <?=admin_login_copright?></p>
    </div>
</section>
<!-- end: page -->

<!-- Vendor -->
<script src="../../vendor/jquery/jquery.js"></script>
<script src="../../vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
<script src="../../vendor/jquery-cookie/jquery.cookie.js"></script>
<script src="../../master/style-switcher/style.switcher.js"></script>
<script src="../../vendor/popper/umd/popper.min.js"></script>
<script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../../vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="../../vendor/common/common.js"></script>
<script src="../../vendor/nanoscroller/nanoscroller.js"></script>
<script src="../../vendor/magnific-popup/jquery.magnific-popup.js"></script>
<script src="../../vendor/jquery-placeholder/jquery.placeholder.js"></script>
<script src="../../js/theme.js"></script>
<script src="../../js/custom.js"></script>
<script src="../../js/theme.init.js"></script>
</body>
</html>