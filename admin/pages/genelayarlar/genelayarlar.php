<?php
include"../../class/ayarlar.php";
?>
<!doctype html>
<html class="fixed">
<head>
    <meta charset="UTF-8">
    <title><?=$db->VeriOkuTek ("ayarlar","sitebaslik","id",1)?></title>
    <meta name="keywords" content="<?=$db->VeriOkuTek ("ayarlar","sitebaslik","id",1)?>" />
    <meta name="description" content="<?=$db->VeriOkuTek ("ayarlar","sitebaslik","id",1)?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">
    <!-- Vendor CSS -->
    <link rel="stylesheet" href="../../vendor/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="../../vendor/animate/animate.compat.css">
    <link rel="stylesheet" href="../../vendor/font-awesome/css/all.min.css" />
    <link rel="stylesheet" href="../../vendor/boxicons/css/boxicons.min.css" />
    <link rel="stylesheet" href="../../vendor/magnific-popup/magnific-popup.css" />
    <link rel="stylesheet" href="../../vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css" />
    <link rel="stylesheet" href="../../vendor/bootstrap-fileupload/bootstrap-fileupload.min.css" />
    <link rel="stylesheet" href="../../css/theme.css" />
    <link rel="stylesheet" href="../../css/custom.css">
    <script src="../../vendor/modernizr/modernizr.js"></script>
</head>
<body>
<section class="body">
    <?php
    include"../include/header.php";
    include"../../class/$class";
    $modul = new Modul();
    ?>
    <div class="inner-wrapper">
        <?php
        include"../include/menu.php";
        ?>

        <section role="main" class="content-body " style="padding: 7px">
            <header class="page-header">
                <h2><?=$modul->pagealtitle[0]?></h2>

                <div class="right-wrapper text-end">
                    <ol class="breadcrumbs">
                        <li><a href="<?=$siteURL?>"><i class="bx bx-home-alt"></i></a></li>
                        <li><span><?=$modul->breadcrumb[1]?></span></li>
                    </ol>
                    <a class="sidebar-right-toggle" ><i class="fas fa-chevron-left"></i></a>
                </div>
            </header>

            <!-- start: page -->
            <div class="row">
                <div class="col">
                    <section class="card">
                        <div class="card-body">
                            <div class="col-lg-12">
                                <?php
                                if($_POST)
                                {
                                    $modul->Update ();
                                }
                                $liste= $db->VeriOkuCoklu ($modul->tablo,array($modul->primary),array(1));
                                ?>
                            </div>
                            <form class="ajaxFormFalse" method="POST" enctype="multipart/form-data">
                                <div class="mb-3">
                                    <label class="form-label"><?=admin_modul_ayarlar_site_basligi?></label>
                                    <input type="text" value="<?=$liste[0]->sitebaslik?>" name="sitebaslik" class="form-control">
                                </div>

                                <div class="mb-3">
                                    <label class="form-label"><?=admin_modul_ayarlar_site_url?></label>
                                    <input type="text" value="<?=$liste[0]->siteadresi?>" name="siteadresi" class="form-control vRequired">
                                </div>

                                <div class="mb-3">
                                    <label class="form-label"><?=admin_modul_ayarlar_dizin?></label>
                                    <input type="text" value="<?=$liste[0]->dizin?>" name="dizin"  class="form-control vRequired">
                                </div>


                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-6 mb-3">
                                            <label class="form-label"><?=admin_modul_ayarlar_mail_adresi?></label>
                                            <input type="text" value="<?=$liste[0]->mail?>" name="mail"  class="form-control">
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label class="form-label"><?=admin_modul_ayarlar_telefon?></label>
                                            <input type="text" value="<?=$liste[0]->telefon?>" name="telefon"  class="form-control">
                                        </div>
                                    </div>
                                </div>


                                <div class="mb-3">
                                    <label class="form-label"><?=admin_modul_ayarlar_adres?></label>
                                    <input value="<?=$liste[0]->adres?>" name="adres" type="text" class="form-control vRequired">
                                </div>



                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="mb-3 col-md-4">
                                            <label class="form-label"><?=admin_modul_ayarlar_linkedin?></label>
                                            <input type="text" value="<?=$liste[0]->linkedin?>" name="linkedin"  class="form-control">
                                        </div>
                                        <div class="mb-3 col-md-4">
                                            <label class="form-label"><?=admin_modul_ayarlar_youtube?></label>
                                            <input type="text" value="<?=$liste[0]->youtube?>" name="youtube"  class="form-control">
                                        </div>
                                        <div class="mb-3 col-md-3">
                                            <label class="form-label"><?=admin_modul_ayarlar_instagram?></label>
                                            <input type="text" value="<?=$liste[0]->instagram?>" name="instagram"  class="form-control">
                                        </div>
                                        <div class="mb-3 col-md-6">
                                            <label class="form-label"><?=admin_modul_ayarlar_facebook?></label>
                                            <input type="text" value="<?=$liste[0]->facebook?>" name="facebook"  class="form-control">
                                        </div>
                                        <div class="mb-3 col-md-6">
                                            <label class="form-label"><?=admin_modul_ayarlar_twitter?></label>
                                            <input type="text" value="<?=$liste[0]->twitter?>" name="twitter"  class="form-control">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="mb-3 col-md-6">
                                            <label class="form-label"><?=admin_modul_ayarlar_header_kodlari?></label>
                                            <textarea class="form-control" name="headerkod"><?=$liste[0]->headerkod?></textarea>
                                        </div>
                                        <div class="mb-3 col-md-6">
                                            <label class="form-label"><?=admin_modul_ayarlar_footer_kodlari?></label>
                                            <textarea class="form-control" name="footerkod"><?=$liste[0]->footerkod?></textarea>

                                        </div>
                                    </div>
                                </div>



                                <button type="submit" class="btn btn-primary submitbtn"><?=admin_genel_gonder?></button>
                            </form>
                        </div>
                    </section>
                </div>
            </div>

        </section>
    </div>


</section>


<script src="../../vendor/jquery/jquery.js"></script>
<script src="../../vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
<script src="../../vendor/jquery-cookie/jquery.cookie.js"></script>
<script src="../../vendor/popper/umd/popper.min.js"></script>
<script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../../vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="../../vendor/common/common.js"></script>
<script src="../../vendor/nanoscroller/nanoscroller.js"></script>
<script src="../../vendor/magnific-popup/jquery.magnific-popup.js"></script>
<script src="../../vendor/jquery-placeholder/jquery.placeholder.js"></script>
<script src="../../vendor/autosize/autosize.js"></script>
<script src="../../vendor/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
<script src="../../js/theme.js"></script>
<script src="../../js/custom.js"></script>
<script src="../../js/theme.init.js"></script>
<script type="text/javascript" src="../../js/jquery/jquery-3.1.0.min.js"></script>
<script type="text/javascript" src="../../js/jquery.maskedinput.js"></script>
<script type="text/javascript">
    $( document ).ready(function( $ ) {
        $(".telefoninput").mask("(999) 999 99 99",{placeholder:"(___) ___ __ __"});
    });
</script>
<script type="text/javascript" src="../../js/validation_master.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.ajaxFormFalse').validationForm({'ajaxType':false});
        $('.ajaxFormTrue').validationForm({'ajaxType':true,'ajaxRefreshPage':true});
    })
</script>

</body>
</html>