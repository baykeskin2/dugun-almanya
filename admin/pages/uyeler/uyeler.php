<?php
include"../../class/ayarlar.php";
?>
<!doctype html>
<html class="fixed">
<head>
    <meta charset="UTF-8">
    <title><?=$db->VeriOkuTek ("ayarlar","sitebaslik","id",1)?></title>
    <meta name="keywords" content="<?=$db->VeriOkuTek ("ayarlar","sitebaslik","id",1)?>" />
    <meta name="description" content="<?=$db->VeriOkuTek ("ayarlar","sitebaslik","id",1)?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">
    <!-- Vendor CSS -->
    <link rel="stylesheet" href="../../vendor/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="../../vendor/animate/animate.compat.css">
    <link rel="stylesheet" href="../../vendor/font-awesome/css/all.min.css" />
    <link rel="stylesheet" href="../../vendor/boxicons/css/boxicons.min.css" />
    <link rel="stylesheet" href="../../vendor/magnific-popup/magnific-popup.css" />
    <link rel="stylesheet" href="../../vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css" />
    <link rel="stylesheet" href="../../vendor/bootstrap-fileupload/bootstrap-fileupload.min.css" />
    <link rel="stylesheet" href="../../css/theme.css" />
    <link rel="stylesheet" href="../../css/custom.css">
    <script src="../../vendor/modernizr/modernizr.js"></script>
    <link rel="stylesheet" href="../../vendor/datatables/media/css/dataTables.bootstrap5.css" />
    <link rel="stylesheet" href="../../vendor/simple-line-icons/css/simple-line-icons.css" />
</head>
<body>
<section class="body">
    <?php
    include"../include/header.php";
    include"../../class/$class";
    $modul = new Modul();
    ?>
    <div class="inner-wrapper">
        <?php
        include"../include/menu.php";
        ?>

        <section role="main" class="content-body " style="padding: 7px">
            <header class="page-header">
                <h2><?=$modul->pagealtitle[0]?></h2>

                <div class="right-wrapper text-end">
                    <ol class="breadcrumbs">
                        <li><a href="<?=$siteURL?>"><i class="bx bx-home-alt"></i></a></li>
                        <li><span><?=$modul->breadcrumb[1]?></span></li>
                    </ol>
                    <a class="sidebar-right-toggle" ><i class="fas fa-chevron-left"></i></a>
                </div>
            </header>

            <!-- start: page -->
            <div class="row">
                <div class="col">
                    <section class="card">
                        <div class="card-body">
                            <div class="col-lg-12">
                                <?php
                                if($_POST)
                                {
                                    if(isset($_POST["islem"]))
                                    {
                                        if($_POST["islem"]=="Duzenle")
                                        {
                                            $modul->Update ();
                                        }
                                        if($_POST["islem"]=="bakiye_yukle")
                                        {
                                            $modul->bakiye_yukle ();
                                        }
                                        else
                                        {
                                            $modul->Insert ();
                                        }
                                    }else
                                    {
                                        if(isset($_POST["ban_sebebi"]))
                                        {
                                            $modul->Banla();
                                        }else
                                        {
                                            $modul->Delete_All();
                                        }

                                    }
                                }
                                if(isset($_GET["islem"]))
                                {
                                    if($_GET["islem"]=="sil")
                                    {
                                        $modul->Delete();
                                    }

                                    if($_GET["islem"]=="bankaldir")
                                    {
                                        $modul->BanKaldir();
                                    }

                                }
                                ?>
                            </div>

                            <form method="POST">
                                <table class="table table-bordered table-striped mb-0" id="datatable-tabletools">

                                    <thead>
                                    <tr>
                                        <th >
                                        <span class="checkbox-custom checkbox-primary">
                                            <input class="selectable-all" type="checkbox">
                                            <label></label>
                                        </span>
                                        </th>
                                        <th><?=admin_modul_uyeler_adi_soyadi?></th>
                                        <th><?=admin_modul_uyeler_mail_adresi?></th>
                                        <th><?=admin_modul_uyeler_telefon_numarasi?></th>
                                        <th><?=admin_modul_uyeler_dugun_tarihi?></th>
                                        <th><?=admin_modul_uyeler_dugun_sehri?></th>
                                        <th class="text-right"><?=admin_genel_islemler?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $menu =$modul->All();
                                    if($menu==false)
                                    {

                                    }else
                                    {
                                        foreach($menu as $row)
                                        {
                                            ?>
                                            <tr>
                                                <th>
                                                <span style="padding-left: 0px" class="checkbox-custom checkbox-primary">
                                                    <input name="SilID[]" class="selectable-item" type="checkbox" id="row-<?=$row->{$modul->primary}?>" value="<?=$row->{$modul->primary}?>">
                                                    <label for="row-619"></label>
                                                </span>
                                                </th>
                                                <td><?=$row->ad_soyad?></td>
                                                <td><?=$row->mail_adresi?></td>
                                                <td><?=$row->telefon?></td>
                                                <td><?=$sistem->tarihYaz($row->dugun_tarihi)?></td>
                                                <td><?=$db->VeriOkuTek("sehirler","baslik","id",$row->dugun_sehri_id)?></td>

                                                <td class="text-right">
                                                    <a class="silbtn silbtn"  data-bs-toggle="modal" data-bs-target="#silmodal" data-href="?islem=sil&id=<?=$row->{$modul->primary}?>" href="javascript:void(0);"><i class="icons icon-close"></i> </a>
                                                    <a class="duzenlebtn"  href="<?=$admin?>/pages/<?=str_replace (".php","",$class)?>ekle.php?islem=duzenle&id=<?=$row->{$modul->primary}?>"><i class="icons icon-pencil"></i> </a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                    </tbody>
                                </table>
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-danger"><?=admin_genel_secilenleri_sil?></button>
                                </div>
                            </form>
                        </div>
                    </section>
                </div>
            </div>

        </section>
    </div>


</section>
<div class="modal fade " id="silmodal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #d2322d; color:#fff; font-weight: bold; font-size: 17px;">
                <h5 class="modal-title" id="exampleModalLabel"><?=admin_genel_kaydi_sil_mdl_baslik?></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <p><strong><?=admin_genel_kaydi_sil_mdl_aciklama?></strong></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-bs-dismiss="modal"><?=admin_genel_kaydi_sil_mdl_iptal?></button>
                <a href="#" class="btn btn-danger mdlsilbtn"><?=admin_genel_kaydi_sil_mdl_evet?></a>
            </div>
        </div>
    </div>
</div>
<div class="modal fade " id="bansebebimodal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #d2322d; color:#fff; font-weight: bold; font-size: 17px;">
                <h5 class="modal-title" id="exampleModalLabel">Ban Sebebi</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">

            </div>

        </div>
    </div>
</div>
<div class="modal fade " id="manbakmdl" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form method="POST" action="">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #0ea15b; color:#fff; font-weight: bold; font-size: 17px;">
                    <h5 class="modal-title" id="exampleModalLabel">Kullanıcıya Paypal Bakiyesi Ekle</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <label>Yüklenen Paypal Adresi</label>
                    <input type="text" class="form-control" name="paypal_adresi" style="margin-bottom: 10px">

                    <label>TXID</label>
                    <input type="text" class="form-control" name="tx_id" style="margin-bottom: 10px">

                    <label>Yüklenecek Tutar</label>
                    <input type="text" class="form-control currency" name="yuklenen_tutar" style="margin-bottom: 10px">
                    <input type="hidden" name="uye_id" value="" id="mdlyukle_uye_id">
                    <input type="hidden" name="islem" value="bakiye_yukle">


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-bs-dismiss="modal">İptal</button>
                    <button  type="submit" style="background-color: #0ea15b; border:1px solid  #0ea15b;" class="btn btn-danger mdlsilbtn">Yükle</button>
                </div>
            </div>
        </form>
    </div>
</div>


<div class="modal fade " id="banlamodal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form method="POST" action="../../pages/uyeler/uyeler.php">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #d2322d; color:#fff; font-weight: bold; font-size: 17px;">
                    <h5 class="modal-title" id="exampleModalLabel">Üye Banla</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="uye_id" id="banlamdlinput" value="">
                    <textarea class="form-control" name="ban_sebebi" placeholder="Ban sebebini yazınız"></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-bs-dismiss="modal">İptal</button>
                    <button  type="submit" class="btn btn-danger mdlsilbtn">Banla</button>
                </div>
            </div>
        </form>

    </div>
</div>


<script src="../../vendor/jquery/jquery.js"></script>
<script src="../../vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
<script src="../../vendor/jquery-cookie/jquery.cookie.js"></script>
<script src="../../vendor/popper/umd/popper.min.js"></script>
<script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../../vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="../../vendor/common/common.js"></script>
<script src="../../vendor/nanoscroller/nanoscroller.js"></script>
<script src="../../vendor/magnific-popup/jquery.magnific-popup.js"></script>
<script src="../../vendor/jquery-placeholder/jquery.placeholder.js"></script>
<script src="../../vendor/autosize/autosize.js"></script>
<script src="../../vendor/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>


<script src="../../vendor/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="../../vendor/datatables/media/js/dataTables.bootstrap5.min.js"></script>
<script src="../../vendor/datatables/extras/TableTools/Buttons-1.4.2/js/dataTables.buttons.min.js"></script>
<script src="../../vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.bootstrap4.min.js"></script>
<script src="../../vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.html5.min.js"></script>
<script src="../../vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.print.min.js"></script>
<script src="../../vendor/datatables/extras/TableTools/JSZip-2.5.0/jszip.min.js"></script>
<script src="../../vendor/datatables/extras/TableTools/pdfmake-0.1.32/pdfmake.min.js"></script>
<script src="../../vendor/datatables/extras/TableTools/pdfmake-0.1.32/vfs_fonts.js"></script>

<script src="../../js/examples/examples.datatables.default.js"></script>
<script src="../../js/examples/examples.datatables.row.with.details.js"></script>
<script src="../../js/examples/examples.datatables.tabletools.js"></script>


<script src="../../js/theme.js"></script>
<script src="../../js/custom.js"></script>
<script src="../../js/theme.init.js"></script>
<script type="text/javascript" src="../../js/jquery/jquery-3.1.0.min.js"></script>
<script type="text/javascript" src="../../js/jquery.maskedinput.js"></script>
<script type="text/javascript">
    $( document ).ready(function( $ ) {
        $(".telefoninput").mask("(999) 999 99 99",{placeholder:"(___) ___ __ __"});
    });
</script>
<script type="text/javascript" src="../../js/validation_master.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.ajaxFormFalse').validationForm({'ajaxType':false});
        $('.ajaxFormTrue').validationForm({'ajaxType':true,'ajaxRefreshPage':true});
    })
</script>
<script type="text/javascript">
    $(".manbakykl").click(function()
    {
        var id = $(this).attr("data-id");
        $("#mdlyukle_uye_id").val(id);

    })
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $(".silbtn").click(function () {
            var href = $(this).attr("data-href");
            $(".mdlsilbtn").attr("href",href);
        })
    })
</script>


<script type="text/javascript">
    $(document).ready(function(){
        $(".banlabtn").click(function () {
           var id=$(this).attr("data-id");
           $("#banlamdlinput").val(id);
        })
    })
</script>

<script type="text/javascript">
    $(document).ready(function(){
        $(".banlabtn").click(function () {
           var id=$(this).attr("data-id");
           $("#banlamdlinput").val(id);
        })
    })
</script>

<script type="text/javascript">
    $(document).ready(function(){
        $(".bansebebibtn").click(function () {
           var text=$(this).attr("data-text");
           $("#bansebebimodal .modal-body").html(text);
        })
    })
</script>
<script type="text/javascript">
    $(".selectable-all").click(function(){
        $('.selectable-item').not(this).prop('checked', this.checked);
    });
</script>
<script src="../../../assets/js/jquery.mask.min.js"></script>
<script>
    $(function()
    {
        $('.currency').mask("#,##0.00", {reverse: true});
    })
</script>
</body>
</html>