<?php
include"../../class/ayarlar.php";
?>
<!doctype html>
<html class="fixed">
<head>
    <meta charset="UTF-8">
    <title><?=$db->VeriOkuTek ("ayarlar","sitebaslik","id",1)?></title>
    <meta name="keywords" content="<?=$db->VeriOkuTek ("ayarlar","sitebaslik","id",1)?>" />
    <meta name="description" content="<?=$db->VeriOkuTek ("ayarlar","sitebaslik","id",1)?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">
    <!-- Vendor CSS -->
    <link rel="stylesheet" href="../../vendor/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="../../vendor/animate/animate.compat.css">
    <link rel="stylesheet" href="../../vendor/font-awesome/css/all.min.css" />
    <link rel="stylesheet" href="../../vendor/boxicons/css/boxicons.min.css" />
    <link rel="stylesheet" href="../../vendor/magnific-popup/magnific-popup.css" />
    <link rel="stylesheet" href="../../vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css" />
    <link rel="stylesheet" href="../../vendor/bootstrap-fileupload/bootstrap-fileupload.min.css" />
    <link rel="stylesheet" href="../../css/theme.css" />
    <link rel="stylesheet" href="../../css/custom.css">
    <script src="../../vendor/modernizr/modernizr.js"></script>
    <link rel="stylesheet" href="../../vendor/datatables/media/css/dataTables.bootstrap5.css" />
    <link rel="stylesheet" href="../../vendor/simple-line-icons/css/simple-line-icons.css" />
    <link rel="stylesheet" href="../../../assets/css/intlTelInput.css" rel="stylesheet" />
</head>
<body>
<section class="body">
    <?php
    include"../include/header.php";
    include"../../class/$class";
    $modul = new Modul();
    ?>
    <div class="inner-wrapper">
        <?php
        include"../include/menu.php";
        ?>

        <section role="main" class="content-body " style="padding: 7px">
            <header class="page-header">
                <h2><?=$modul->pagealtitle[0]?></h2>

                <div class="right-wrapper text-end">
                    <ol class="breadcrumbs">
                        <li><a href="<?=$siteURL?>"><i class="bx bx-home-alt"></i></a></li>
                        <li><span><?=$modul->breadcrumb[1]?></span></li>
                        <?php
                        if(isset($_GET["id"]))
                        {
                            ?>
                            <li><span><?=$modul->breadcrumb[2]?></span></li>
                            <?php
                        }else
                        {
                            ?>
                            <li><span><?=$modul->breadcrumb[3]?></span></li>
                            <?php
                        }
                        ?>


                    </ol>
                    <a class="sidebar-right-toggle" ><i class="fas fa-chevron-left"></i></a>
                </div>
            </header>

            <!-- start: page -->
            <div class="row">
                <div class="col">
                    <section class="card">
                        <div class="card-body">
                            <div class="col-lg-12">
                                <?php
                                if($_POST)
                                {
                                    if(isset($_GET["id"]))
                                    {
                                        $modul->Update ();
                                    }else
                                    {
                                        $modul->Insert ();
                                    }

                                }
                                if(isset($_GET["id"]))
                                {
                                    $liste = $db->VeriOkuCoklu ($modul->tablo,array($modul->primary),array($_GET["id"]));
                                }


                                ?>
                            </div>
                            <form class="ajaxFormFalse" method="POST" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-md-6  mb-3">
                                        <label class="form-label"><?=admin_modul_uyeler_adi_soyadi?></label>
                                        <input type="text" value="<?=isset($_GET["id"])?$liste[0]->ad_soyad:''?>" class="form-control" name="ad_soyad">
                                    </div>

                                    <div class="col-md-6  mb-3">
                                        <label class="form-label"><?=admin_modul_uyeler_mail_adresi?></label>
                                        <input type="text" value="<?=isset($_GET["id"])?$liste[0]->mail_adresi:''?>" class="form-control" name="mail_adresi">
                                    </div>
                                    <div class="col-md-6  mb-3">
                                        <label class="form-label"><?=admin_modul_uyeler_telefon_numarasi?></label>
                                        <input type="text" value="<?=isset($_GET["id"])?$liste[0]->telefon:''?>" class="form-control" name="telefon">
                                    </div>

                                    <div class="col-md-9  mb-3">
                                        <label class="form-label"><?=admin_modul_uyeler_sifre?></label>
                                        <input type="text" value="<?=isset($_GET["id"])?$liste[0]->sifre:''?>" class="form-control" name="sifre">
                                    </div>

                                    <div class="col-md-4  mb-3">
                                        <label class="form-label"><?=admin_modul_uyeler_dugun_tarihi?></label>
                                        <input type="date" value="<?=isset($_GET["id"])?$liste[0]->dugun_tarihi:''?>" class="form-control" name="dugun_tarihi">
                                    </div>

                                    <div class="col-md-9  mb-3">
                                        <label class="form-label"><?=admin_modul_uyeler_dugun_sehri?></label>
                                        <select class="form-control vRequired" name="dugun_sehri_id">
                                            <option value=""><?=admin_modul_uyeler_dugun_sehri_seciniz?></option>
                                            <?php
                                            $sehirler = $db->VeriOkuCoklu("sehirler");
                                            if($sehirler===false)
                                            {

                                            }else
                                            {
                                                foreach($sehirler as $s)
                                                {
                                                    ?>
                                                    <option <?=isset($_GET["id"])?$liste[0]->dugun_sehri_id==$s->id?'selected':'':''?> value="<?=$s->id?>"><?=$s->baslik?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>




                                </div>


                                <section class="card mb-4">
                                    <header class="card-header">
                                        <div class="card-actions">
                                            <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
                                            <a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
                                        </div>

                                        <h2 class="card-title"><?=admin_modul_uyeler_favori_firmalar_baslik?></h2>
                                        <p class="card-subtitle"><?=admin_modul_uyeler_favori_firmalar_aciklama?></p>
                                    </header>
                                    <div class="card-body">




                                    </div>
                                </section>

                                <section class="card mb-4">
                                    <header class="card-header">
                                        <div class="card-actions">
                                            <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
                                            <a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
                                        </div>

                                        <h2 class="card-title"><?=admin_modul_uyeler_son_gezilen_firmalar_baslik?></h2>
                                        <p class="card-subtitle"><?=admin_modul_uyeler_son_gezilen_firmalar_aciklama?></p>
                                    </header>
                                    <div class="card-body">




                                    </div>
                                </section>
                                <section class="card mb-4">
                                    <header class="card-header">
                                        <div class="card-actions">
                                            <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
                                            <a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
                                        </div>

                                        <h2 class="card-title"><?=admin_modul_uyeler_alisveris_listesi_baslik?></h2>
                                        <p class="card-subtitle"><?=admin_modul_uyeler_alisveris_listesi_aciklama?></p>
                                    </header>
                                    <div class="card-body">




                                    </div>
                                </section>



                                <button type="submit" class="btn btn-primary submitbtn"><?=admin_genel_gonder?></button>
                            </form>

                        </div>
                    </section>


                </div>
            </div>

        </section>
    </div>


</section>

<script src="../../vendor/jquery/jquery.js"></script>
<script src="../../vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
<script src="../../vendor/jquery-cookie/jquery.cookie.js"></script>
<script src="../../vendor/popper/umd/popper.min.js"></script>
<script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../../vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="../../vendor/common/common.js"></script>
<script src="../../vendor/nanoscroller/nanoscroller.js"></script>
<script src="../../vendor/magnific-popup/jquery.magnific-popup.js"></script>
<script src="../../vendor/jquery-placeholder/jquery.placeholder.js"></script>
<script src="../../vendor/autosize/autosize.js"></script>
<script src="../../vendor/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>


<script src="../../vendor/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="../../vendor/datatables/media/js/dataTables.bootstrap5.min.js"></script>
<script src="../../vendor/datatables/extras/TableTools/Buttons-1.4.2/js/dataTables.buttons.min.js"></script>
<script src="../../vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.bootstrap4.min.js"></script>
<script src="../../vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.html5.min.js"></script>
<script src="../../vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.print.min.js"></script>
<script src="../../vendor/datatables/extras/TableTools/JSZip-2.5.0/jszip.min.js"></script>
<script src="../../vendor/datatables/extras/TableTools/pdfmake-0.1.32/pdfmake.min.js"></script>
<script src="../../vendor/datatables/extras/TableTools/pdfmake-0.1.32/vfs_fonts.js"></script>

<script src="../../js/examples/examples.datatables.default.js"></script>
<script src="../../js/examples/examples.datatables.row.with.details.js"></script>
<script src="../../js/examples/examples.datatables.tabletools.js"></script>


<script src="../../js/theme.js"></script>
<script src="../../js/custom.js"></script>
<script src="../../js/theme.init.js"></script>
<script type="text/javascript" src="../../js/jquery/jquery-3.1.0.min.js"></script>
<script type="text/javascript" src="../../js/jquery.maskedinput.js"></script>
<script type="text/javascript">
    $( document ).ready(function( $ ) {
        $(".telefoninput").mask("(999) 999 99 99",{placeholder:"(___) ___ __ __"});
    });
</script>
<script type="text/javascript" src="../../js/validation_master.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.ajaxFormFalse').validationForm({'ajaxType':false});
        $('.ajaxFormTrue').validationForm({'ajaxType':true,'ajaxRefreshPage':true});
    })
</script>
<script src="../../../assets/js/jquery.mask.min.js"></script>
<script>
    $(function()
    {
        $('.currency').mask("#,##0.00", {reverse: true});
    })
</script>

<script type="text/javascript">
    $(document).ready(function(){
        $(".silbtn").click(function () {
            var href = $(this).attr("data-href");
            $(".mdlsilbtn").attr("href",href);
        })
    })
</script>
<script type="text/javascript">
    $(".selectable-all").click(function(){
        $('.selectable-item').not(this).prop('checked', this.checked);
    });
</script>



<script src="../../../assets/js/utils.js"></script>
<script src="../../../assets/js/intlTelInput.js"></script>

<script>

    $("#p_number").intlTelInput({
        initialCountry: "tr",
        preferredCountries: ["tr","us","gb"],
        separateDialCode: true,
        autoPlaceholder: 'aggressive',
        hiddenInput: 'hiddenPhoneNumber',
    });

    $("body").on("click",".country",function()
    {
        var phone = $(this).find(".dial-code").html();
        $("#hiddenPhoneNumber").val(phone);

    })


</script>


<script src="../../../ckeditor/ckeditor.js"></script>
<script>
    initSample();
</script>

</body>
</html>