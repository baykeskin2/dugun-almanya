<?php
Class Modul
{
    function arama_mekan_kategoriler()
    {
        global $db;
        return $db->VeriOkuCoklu("mekan_kategori");
    }
    function arama_firma_kategoriler()
    {
        global $db;
        return $db->VeriOkuCoklu("firma_kategori");
    }

    function sehirler()
    {
        global $db;
        return $db->VeriOkuCoklu("sehirler",array("one_cikan"),array(1),"","id","desc",5);
    }


    function sehir_firma($id)
    {
        global $db;
        return $db->veriSaydir("firmalar",array("sehir_id"),array($id));

    }

    function vitrindeki_firmalar()
    {
        global $db;
        return $db->VeriOkuCokluSorgu("SELECT resimler,id,url,baslik FROM firmalar WHERE vitrin=1 ORDER BY RAND() LIMIT 8 ");
    }
}