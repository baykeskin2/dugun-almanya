<?php
Class Modul
{
    function Detay()
    {
        global $db;
        return $db->VeriOkuCoklu("firmalar",array("id"),array($_GET["id"]))[0];
    }

    function uyelik_farki()
    {
        global $db;
        $origin =  date_create($this->Detay()->kayit_tarihi);
        $target =  date_create(date("Y-m-d"));
        $interval = date_diff($origin, $target);
        $yil = $interval->format('%y');
        $ay = $interval->format('%m');
        $gun = $interval->format('%d');

        $string ="";
        if($yil>0)
        {
            $string.=$yil." Yıl ";
        }
        if($yil>0)
        {
            if($ay<1)
            {
                $string.="1 Aylık";
            }else
            {
                $string.=$ay." Aylık ";
            }

        }

        if($ay<1 AND $yil<1)
        {
            if($gun>0)
            {
                $string.=$gun." Günlük";
            }else
            {
                $string.="1 Günlük";
            }
        }

        return $string;

    }

    function all_yorumlar()
    {
        global $db;
        return $db->VeriOkuCoklu("firma_yorumlar",array("firma_id","onay"),array($this->Detay()->id,1),"","id","DESC","10");

    }
}