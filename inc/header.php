<?php
include"../../include/Sistem.php";
$sistem = new Sistem();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Almanya Düğün Projesi</title>
    <link rel="stylesheet" href="/<?=$dizin?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/<?=$dizin?>assets/fonts/font-avasome/css/all.css" >
    <link rel="stylesheet" href="/<?=$dizin?>assets/css/style.css">
    <link rel="stylesheet" href="/<?=$dizin?>assets/css/validation_master.css">
    <link rel="stylesheet" href="/<?=$dizin?>assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/<?=$dizin?>assets/css/owl.theme.default.min.css">
    <meta name="viewport"  content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="/<?=$dizin?>assets/css/simple-rating.css">



</head>
<body>

<section class="header">
    <div class="container-fluid">
        <div class="container">
            <div class="row header-top">
                <div class="col-md-3 logo">
                    <img src="<?=$general_function->logo()?>">
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbar-content" aria-controls="navbar-content" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fa-solid fa-bars fa-fw"></i>
                    </button>
                </div>
                <div class="col-md-9 uye-ol-btn text-right">
                    <a href="#"><i class="fa-regular fa-circle-user fa-fw"></i> Üye Ol </a>
                    |
                    <a href="#">Üye Girişi </a>
                </div>
            </div>

            <div class="col-md-12 mobilarama">
                <form class="d-flex ms-auto">
                    <div class="input-group">
                        <input class="form-control border-0 mr-2" type="search" placeholder="Ne aramıştınız ?" aria-label="Search">
                        <button class="btn btn-primary border-0" type="submit"><i class="fa-solid fa-magnifying-glass fa-fw"></i></button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</section>

<section class="menu">
    <div class="container-fluid">
        <div class="container menu-p-5">
            <nav class="navbar navbar-expand-lg">
                <div class="container-fluid menu-p-0">

                    <div class="collapse navbar-collapse" id="navbar-content">
                        <ul class="navbar-nav mr-auto mb-2 mb-lg-0">

                            <?php
                            $i =0;
                            $menuler = $general_function->menuler(0,1);
                            if($menuler===false)
                            {
                            }else
                            {
                                foreach($menuler as $um)
                                {
                                    $alt_menuler = $general_function->menuler($um->id,1);
                                    if($alt_menuler===false)
                                    {
                                        ?>
                                        <li class="nav-item">
                                            <a class="nav-link" href="<?=$um->url?>" tabindex="-1" aria-disabled="true"><i class="<?=$um->icon?>"></i> <?=$um->baslik?></a>
                                        </li>
                                        <?php
                                    }else
                                    {
                                        ?>
                                        <li class="nav-item dropdown dropdown-mega position-static">
                                            <a class="nav-link dropdown-toggle" href="#" data-bs-toggle="dropdown" data-bs-auto-close="outside"><i class="<?=$um->icon?>"></i> <?=$um->baslik?></a>
                                            <div class="dropdown-menu shadow">
                                                <div class="mega-content px-4">
                                                    <div class="container-fluid">
                                                        <div class="row altmenuler">
                                                            <div class="col-12 col-sm-6 col-md-6 py-4">
                                                               <div class="row">
                                                                   <?php
                                                                   foreach($alt_menuler as $am)
                                                                   {
                                                                       ?>
                                                                       <div class="col-12 col-sm-6 col-md-6">
                                                                           <a href="<?=$am->url?>"><?=$am->baslik?></a>
                                                                       </div>
                                                                       <?php
                                                                   }
                                                                   ?>
                                                               </div>
                                                            </div>

                                                            <div class="col-12 col-sm-6 col-md-6 py-4">

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <?php

                                    }
                                }
                            }

                            ?>
                        </ul>
                        <form class="d-flex ms-auto masaustuarama">
                            <div class="input-group">
                                <input class="form-control border-0 mr-2" type="search" placeholder="Ne aramıştınız ?" aria-label="Search">
                                <button class="btn btn-primary border-0" type="submit"><i class="fa-solid fa-magnifying-glass fa-fw"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
            </nav>
        </div>
    </div>
</section>


