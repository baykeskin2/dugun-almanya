<section class="footer">
    <div class="container-fluid footerpad">
        <div class="container">
            <div class="row">
                <div class="col-md-4 logove-sosyal">
                    <img src="/<?=$dizin?>images/logo.png">
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially</p>
                    <p>
                        <a href="#"><i class="fa-brands fa-linkedin fa-fw"></i> </a>
                        <a href="#"><i class="fa-brands fa-youtube fa-fw"></i> </a>
                        <a href="#"><i class="fa-brands fa-instagram fa-fw"></i> </a>
                    </p>
                </div>
                <div class="col-6 col-md-4 destek">
                    <h3>DESTEK</h3>
                    <p>
                        Telefon ile destek gün ve saatlerimiz
                        Hafta içi her gün | 10:00-18:00

                    </p>
                    <p>
                        <span><?=$db->VeriOkuTek("ayarlar","telefon","id",1)?></span>
                    </p>
                    <p>
                        Teknik destek ve sorunlarınız için bize e-posta adresimizden ulaşabilirsiniz.

                    </p>
                    <p> <span> <?=$db->VeriOkuTek("ayarlar","mail","id",1)?></span></p>
                </div>
                <div class="col-6 col-md-4 linkler">
                    <h3>KATEGORİLER</h3>
                    <ul>
                        <?php
                        $menuler = $general_function->menuler(0,2);
                        if($menuler===false)
                        {

                        }else
                        {
                            foreach($menuler as $m)
                            {
                                ?>
                                <li>
                                    <a href="<?=$m->url?>"><?=$m->baslik?></a>
                                </li>
                                <?php
                            }
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid bottom">
        <p>© 2022 Lorem  Tüm Hakları Saklıdır.</p>
    </div>
</section>
<script type="text/javascript">
    var dizin='dugun-almanya/';
    var vMinTarmesaj            ="";
    var vMaxTarmesaj            ="";
    var vCheckMinTarmesaj       ="";
    var vCheckRequiredmaxmesaj  ="";
    var vRequiredmesaj          ="Bu alanı boş bıraktınız";
    var vNumericmesaj           ="Bu alana sadece rakam girebilirsiniz";
    var vNumericNotmesaj        ="Bu alanda sadece hatf girebilirsiniz";
    var vEmailFiltermesaj       ="Geçerli bir mail adresi giriniz";
    var vMaxcharmesaj           ="Bu alanda minimum seçim yapmanız gerekiyor";
    var vMincharmesaj           ="Bu alanda minimum seçim yapmanız gerekiyor";
    var vPasswordConfirmmesaj   ="Girdiğiniz şifreler birbiri ile aynı olmalıdır";
    var vCheckRequiredmesaj     ="Bu alanda seçim yapmalısınız";
    var vCheckRequiredminmesaj  ="Bu alanda seçim yapmalısınız";
    var vTcRequiredmesaj        ="Geçerli bir TC Numarası giriniz";
    var vYoutubemesaj           ="Geçerli Bir Adres Giriniz";
    var vFileSizemesaj          ="%c KB'den fazla resim veya dosya yükleyemezsiniz.";
    var js_kalan_stok_uyari ="Bu ürünün kalan stok adeti : ";
    var js_tc_uyari = "Bu kategoride ürün alabilmek için tc kimlik numaranızı doğrulamanız gerekmektedir TC Kimlik numaranızı doğrulamak için";
    var js_tikla = "Bu kategoride ürün alabilmek için tc kimlik numaranızı doğrulamanız gerekmektedir TC Kimlik numaranızı doğrulamak için";
</script>
<script type="text/javascript" src="/<?=$dizin?>assets/js/popper.min.js"></script>
<script type="text/javascript" src="/<?=$dizin?>assets/js/bootstrap.js"></script>
<script type="text/javascript" src="/<?=$dizin?>assets/js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="/<?=$dizin?>assets/js/jquery.maskedinput.js"></script>
<script type="text/javascript" src="/<?=$dizin?>assets/js/validation_master.js"></script>
<script type="text/javascript" src="/<?=$dizin?>assets/js/jquery.mask.min.js"></script>
<script type="text/javascript" src="/<?=$dizin?>assets/js/owl.carousel.js"></script>
<link rel="stylesheet" href="/<?=$dizin?>assets/css/animate.css">
<script type="text/javascript" src="/<?=$dizin?>assets/js/app.js"></script>

<script src="/<?=$dizin?>assets/js/simple-rating.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('.rating').rating();
    });
</script>
</body>
</html>