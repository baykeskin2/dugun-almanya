$( document ).ready(function( $ ) {
    $(".telefoninput").mask("(999) 999 99 99",{placeholder:"(___) ___ __ __"});
    $(".ibanno").mask("TR 9999 9999 9999 9999 9999 9999",{placeholder:"TR ____ ____ ____ ____ ____ ____"});
});

$(document).ready(function(){
    $('.ajaxFormFalse').validationForm({'ajaxType':false});
    $('.ajaxFormTrue').validationForm({'ajaxType':true,'ajaxRefreshPage':true});
})
$(function()
{
    $('.currency').mask("#,##0", {reverse : true});
})


function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}


$('.loop').owlCarousel({
    center: true,
    items:2,
    loop:true,
    nav:true,
    margin:10,
    dots:false,
    responsive:{
        600:{
            items:6
        }
    }
});
$('.loop-indirim').owlCarousel({
    center: true,
    items:2,
    loop:true,
    nav:true,
    dots:false,
    margin:10,
    responsive:{
        600:{
            items:6
        }
    }
});


$('.loop-yorumlar').owlCarousel({
    center: true,
    items:2,
    loop:true,
    dots:false,
    nav:true,
    margin:20,
    responsive:{
        600:{
            items:2
        }
    }
});
$('.loop-yorumlar-mobil').owlCarousel({
    center: false,
    items:1,
    loop:true,
    dots:false,
    nav:true,
    margin:20,
    responsive:{
        600:{
            items:2
        }
    }
});

$('.detayowl').owlCarousel({
    animateOut: 'slideOutDown',
    animateIn: 'flipInX',
    items:1,
    margin:10,
    dots:false,
    stagePadding:0,
    smartSpeed:450,
    nav:true

})
$(".aramakategoriler li a").click(function()
{
    $(".kategoribtn").html($(this).html());
    $("#aramakategori-id").val($(this).attr("data-id"));
    $("#aramakategori-tur").val($(this).attr("data-tur"));
})
$(".sehirsecimi li a").click(function()
{
    $(".sehirbtn").html($(this).html());
    $("#sehir-id").val($(this).attr("data-id"));
})

// Favorilere Ekleme Çıkarma
$("body").on("click",'.favorilere-ekle',function()
{
    var id = $(this).attr("data-id");
    var tur = $(this).attr("data-tur");
    $(this).html('<i class="fa-solid fa-heart fa-fw"></i> Listemden Çıkar');
    $(this).removeClass('favorilere-ekle');
    $(this).addClass('favorilerden-cikar');

    $.ajax({
        type: 'POST',
        url: "/"+dizin+"Ajax.php?islem=favorilere-ekle&id="+id+"&tur="+tur+"",
        success: function(sonuc){

        }
    });
})
$("body").on("click",'.favorilerden-cikar',function()
{
    var id = $(this).attr("data-id");
    var tur = $(this).attr("data-tur");
    $(this).html('<i class="fa-solid fa-heart fa-fw"></i> Listeme Ekle');
    $(this).removeClass('favorilerden-cikar');
    $(this).addClass('favorilere-ekle');

    $.ajax({
        type: 'POST',
        url: "/"+dizin+"Ajax.php?islem=favorilerden-cikar&id="+id+"&tur="+tur+"",
        success: function(sonuc){

        }
    });
})
// Favorilere Ekleme Çıkarma
