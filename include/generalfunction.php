<?php
Class General_Function
{


    function menuler($ust_menu_id,$konum)
    {
        global $db;
        return $db->VeriOkuCoklu("menuler",array("ust_menu_id","konum"),array($ust_menu_id,$konum));
    }

    function urller()
    {
        global $db;
        return array(
          'detay-url'=>'detail',
          'firma-detay-url'=>'unternehmen',
          'mekan-detay-url'=>'platz',
          'kategori-url'=>'kategorie',
          'mekan-kategori-url'=>'platz',
          'firma-kategori-url'=>'unternehmen',
          'ust-kategori-firma-url'=>'unternehmen',
          'ust-kategori-mekan-url'=>'platz',
          );
    }

    function puanlar($id,$round)
    {
        global $db;
        $ortalama = $db->VeriOkuCokluSorgu("SELECT AVG(puan) as ortalama,COUNT(id) as toplam FROM firma_yorumlar WHERE firma_id=$id");
        return array(round($ortalama[0]->ortalama,$round), 5-round($ortalama[0]->ortalama,$round),$ortalama[0]->toplam);
    }

    function logo()
    {
        global $dizin;
        return "/".$dizin."images/logo.png";
    }
}