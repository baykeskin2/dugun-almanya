<?php
class veritabaniislem
{
    public $veritabani="almanya-dugun";
    public $host      ="localhost";
    public $hesap     ="root";
    public $sifre     ="";
    public $baglan;
    public $bilgial   =array();
    function __construct()
    {
        try
        {

            $this->baglan=new PDO("mysql:host=$this->host;dbname=$this->veritabani",$this->hesap,$this->sifre);
            $this->baglan->exec("SET NAMES UTF8 COLLATE utf8_turkish_ci");
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }
    function VeriOkuTek($tablo,$alan,$sartalan,$sartkarsilik)
    {
        $dondur   =null;
        $yenisorgu="SELECT $alan FROM $tablo WHERE $sartalan=?";
        $stuncek  =$this->baglan->prepare($yenisorgu);
        $stuncek->execute(array($sartkarsilik));
        $dondur=$stuncek->fetch(PDO::FETCH_OBJ);
        return $dondur->{$alan};
    }
    function VeriOkuTekCoklu($tablo,$alan,$sartalan=array(),$sartkarsilik=array(),$parametre,$isaret,$order=null,$ordertur=null,$limin=null)
    {
        $dondur       =null;
        $sartalansorgu="";
        foreach($sartalan as $s)
        {
            $sartalansorgu.=" $s".$isaret."? $parametre";
        }
        $sartalansorgu=substr($sartalansorgu,0,strlen($sartalansorgu)-strlen($parametre));
        if($order!=null)
        {
            if($limin!=null)
            {
                $ordersorgu=" ORDER BY $order $ordertur LIMIT $limin";
            }
            else
            {
                $ordersorgu=" ORDER BY $order $ordertur";
            }
        }
        else
        {
            $ordersorgu="";
        }
        $yenisorgu="SELECT $alan FROM $tablo WHERE $sartalansorgu $ordersorgu";
        $stuncek=$this->baglan->prepare($yenisorgu);
        $stuncek->execute($sartkarsilik);
        $dondur=$stuncek->fetch(PDO::FETCH_OBJ);
        return $dondur->{$alan};
    }
    function veriGuncelleParametresiz($tablo,$sartalan=array(),$where,$guncelleid,$istisna=array(),$istisnakarsilik=array())
    {
        $sayi     =0;
        $birlestir="";
        foreach($sartalan as $sartal)
        {
            $birlestir.=$sartal."=?,";
        }
        if(count($istisna)>0)
        {
            foreach($istisna as $istisnayaz)
            {
                $birlestir.=$istisnayaz."=?,";
            }
        }
        $kes         =trim(substr($birlestir,0,-1));
        $ekleme      =$this->baglan->prepare("UPDATE $tablo SET $kes WHERE $where=$guncelleid");
        $sartkarsilik=array();
        foreach($sartalan as $row)
        {
            $sartkarsilik[]=$_POST[$row];
        }
        if(count($istisnakarsilik)>0)
        {
            foreach($istisnakarsilik as $karsiliktaz)
            {
                $sartkarsilik[]=$karsiliktaz;
            }
        }
        $ekleme->execute($sartkarsilik);
        if($ekleme)
        {
            $sayi=1;
        }
        else
        {
            $sayi=0;
        }
        return $sayi;
    }
    function veriGuncelle($tablo,$sartalan=array(),$sartkarsilik=array(),$where)
    {
        $sayi     =0;
        $birlestir="";
        foreach($sartalan as $sartal)
        {
            $birlestir.=$sartal."=?,";
        }
        $kes   =trim(substr($birlestir,0,-1));
        $ekleme=$this->baglan->prepare("UPDATE $tablo SET $kes WHERE $where=?");
        $ekleme->execute($sartkarsilik);
        if($ekleme)
        {
            $sayi=1;
        }
        else
        {
            $sayi=0;
        }
        return $sayi;
    }
    function veriSil($tablo,$sartalan=array(),$sartkarsilik=array(),$parametre)
    {
        $sayi     =0;
        $birlestir="";
        foreach($sartalan as $sartal)
        {
            if($parametre=="")
            {
                $birlestir.=$sartal."=? AND ";
            }
            else
            {
                $birlestir.=$sartal."$parametre? AND ";
            }
        }
        $kes  =trim(substr($birlestir,0,-4));
        $silme=$this->baglan->prepare("DELETE FROM $tablo WHERE $kes");
        $silme->execute($sartkarsilik);
        if($silme)
        {
            $sayi=1;
        }
        else
        {
            $sayi=0;
        }
    }
    function veriEkleSayiAl($tablo,$sartalan=array(),$sartkarsilik=array())
    {
        $sayi     =0;
        $birlestir="";
        foreach($sartalan as $sartal)
        {
            $birlestir.=$sartal.",";
        }
        $kes   =trim(substr($birlestir,0,-1));
        $ekleme=$this->baglan->prepare("INSERT INTO $tablo VALUES ($kes)");
        $ekleme->execute($sartkarsilik);
        $sayi=$this->baglan->lastInsertId();
        return $sayi;
    }
    function veriEkle($tablo,$sartalan=array(),$sartkarsilik=array())
    {
        $sayi     =0;
        $birlestir="";
        if($tablo=="uyebildirim")
        {
            $sartalan[]="?";
            $sartkarsilik[]=0;
        }

        foreach($sartalan as $sartal)
        {
            $birlestir.=$sartal.",";
        }
        $kes=trim(substr($birlestir,0,-1));
        $ekleme=$this->baglan->prepare("INSERT INTO $tablo VALUES ($kes)");

        $ekleme->execute($sartkarsilik);
        if($ekleme)
        {
            $sayi=1;
        }
        else
        {
            $sayi=0;
        }
        return $sayi;
    }
    function VeriOkuCokluSorgu($sorgu,$donus="")
    {
        $this->bilgial=null;
        $dondur = "";
        $alancek=$this->baglan->prepare("$sorgu");
        $alancek->execute();

        if($donus=="")
        {
            if($alancek->rowCount()<1)
            {
                $dondur =false;
            }else
            {
                $dondur = $alancek->fetchAll(PDO::FETCH_OBJ);
            }
            return $dondur;
        }else
        {
            if($alancek->rowCount()<1)
            {
                $dondur =false;
            }else
            {
                $dondur =array($alancek->fetchAll(PDO::FETCH_OBJ),$alancek->rowCount());
            }
            return $dondur;
        }

    }
    function VeriOkuCoklu($tablo,$sart=array(),$sartkarsilik=array(),$operator="",$order="",$ordertur="",$limit="",$alanlar ="")
    {
        $this->bilgial=null;
        $dondur = "";
        if($alanlar!='')
        {
            $cek = $alanlar;
        }else
        {
            $cek="*";
        }
        $sorgu ="SELECT $cek FROM $tablo";
        if( count($sart)>0)
        {
            $sorgu.=" WHERE ";
            if($operator!="")
            {
                $op =$operator;
            }else
            {
                $op ="=";
            }
            $param="$op? AND ";
            $birlestir=implode($param,$sart);
            $birlestir.=$op."?";
            $sorgu.=$birlestir;
        }

        if($order!="")
        {
            $sorgu.=" ORDER BY $order $ordertur";
        }

        if($limit!="")
        {
            $sorgu.=" LIMIT $limit";
        }

        if(count($sartkarsilik)>0)
        {
            $alancek=$this->baglan->prepare("$sorgu");
            $alancek->execute($sartkarsilik);
            if($alancek->rowCount()<1)
            {
                $dondur =false;
            }else
            {
                $dondur = $alancek->fetchAll(PDO::FETCH_OBJ);
            }
        }else
        {
            $alancek=$this->baglan->prepare("$sorgu");
            $alancek->execute($sartkarsilik);
            if($alancek->rowCount()<1)
            {
                $dondur =false;
            }else
            {
                $dondur = $alancek->fetchAll(PDO::FETCH_OBJ);
            }
        }
        return $dondur;
    }
    function veriSaydir($tablo,$sart=array(),$sartkarsilik=array(),$operator="",$order="",$ordertur="",$limit="")
    {

        $this->bilgial=null;
        $sorgu ="SELECT * FROM $tablo";
        if( count($sart)>0)
        {
            $sorgu.=" WHERE ";
            if($operator!="")
            {
                $op =$operator;
            }else
            {
                $op ="=";
            }
            $param="$op? AND ";
            $birlestir=implode($param,$sart);
            $birlestir.=$op."?";
            $sorgu.=$birlestir;
        }

        if($order!="")
        {
            $sorgu.=" ORDER BY $order $ordertur";
        }

        if($limit!="")
        {
            $sorgu.=" LIMIT $limit";
        }



        if(count($sartkarsilik)>0)
        {
            $alancek=$this->baglan->prepare("$sorgu");
            $alancek->execute($sartkarsilik);
            $sayi=$alancek->rowCount();
        }else
        {
            $alancek=$this->baglan->prepare("$sorgu");
            $alancek->execute($sartkarsilik);
            $sayi=$alancek->rowCount();
        }

        return $sayi;
    }
    function veriSaydirSorgu($sorgu,$sart=array())
    {
        $sayi=0;
        $sorgu=$this->baglan->prepare("$sorgu");
        $sorgu->execute($sart);
        $sayi=$sorgu->rowCount();
        return $sayi;
    }
    function Basarili($mesaj)
    {
        ?>
        <div class="clearfix"></div>
        <div class="alert alert-success alert-dismissible fade show"><?=$mesaj?> <a class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></a></div>
        <div class="clearfix"></div>
        <?php
    }
    function Basarisiz($mesaj)
    {
        ?>
        <div class="clearfix"></div>
        <div class="alert alert-danger alert-dismissible fade show"><?=$mesaj?> <a class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></a></div>
        <div class="clearfix"></div>
        <?php
    }
    function urlYap($text)
    {
        $find = array('Ç', 'Ş', 'Ğ', 'Ü', 'İ', 'Ö', 'ç', 'ş', 'ğ', 'ü', 'ö', 'ı', '+', '#');
        $replace = array('c', 's', 'g', 'u', 'i', 'o', 'c', 's', 'g', 'u', 'o', 'i', 'plus', 'sharp');
        $text = strtolower(str_replace($find, $replace, $text));
        $text = preg_replace("@[^A-Za-z0-9\-_\.\+]@i", ' ', $text);
        $text = trim(preg_replace('/\s+/', ' ', $text));
        $text = str_replace(' ', '-', $text);
        return $text;
    }
    function cevir($tarih)
    {
        $parcala   =explode("-",$tarih);
        $yeni_tarih=$parcala[2]."-".$parcala[1]."-".$parcala[0];
        return $yeni_tarih;
    }
    function saatlicevir($tarih)
    {
        $bosluk  =explode(" ",$tarih);
        $parcala =explode("-",$bosluk[0]);
        $parcala2=explode(":",$bosluk[1]);
        $yeni_tarih=$parcala[2]."/".$parcala[1]."/".$parcala[0]." ".$parcala2[0].":".$parcala2[1];
        return $yeni_tarih;
    }
    function urlAl()
    {
        $links     ="$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $parcalas  =explode("/",$links);
        $soracaksss=str_replace("-","",$parcalas[1]);
        $soracaks  =$soracaksss;
        return $soracaks;
    }
    function urlTam()
    {
        $links="$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        return $links;
    }
    function sifre_uret()
    {
        $sifre_ver    ="";
        $karakterler  ="1234567890ABCDEFGHIJKLMNOPRSTUVYZ";
        $karakter_sayi=strlen($karakterler);
        for($ras=0 ;$ras<6 ;$ras++)
        {
            $rakam_ver=rand(0,$karakter_sayi-1);
            $sifre_ver.=$karakterler[$rakam_ver];
        }
        return $sifre_ver;
    }
    function __destruct()
    {
        $this->baglan=null;
    }
}

?>