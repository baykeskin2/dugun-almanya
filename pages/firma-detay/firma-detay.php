<?php
include "../../inc/header.php";
$detay = $modul->Detay();
$puanlar = $general_function->puanlar($_GET["id"],0);
$puanlar_round = $general_function->puanlar($_GET["id"],1);
$iletisim_bilgileri = json_decode($detay->iletisim_bilgileri);

?>

<section class="content detaycontent">

    <div class="container-fluid">
        <div class="container">
            <div class="col-md-12 ">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?=$siteURL?>">Anasayfa</a></li>
                        <li class="breadcrumb-item"><a href="<?=$siteURL?>/unternehmen">Düğün Firmaları</a></li>
                        <li class="breadcrumb-item active" aria-current="page"><?=$detay->baslik?></li>
                    </ol>
                </nav>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-9">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="detayowl owl-carousel owl-theme">
                                    <?php
                                    $resimler = explode(",",$detay->resimler);
                                    foreach($resimler as $r)
                                    {
                                        ?>
                                        <div class="item">
                                            <img src="/<?=$dizin?><?=$r?>">
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                                <div class="owl-uzeri">
                                    <span><i class="fa-regular fa-image fa-fw"></i> <?=count($resimler)?> Görsel </span>
                                    <span><i class="fa-regular fa-image fa-fw"></i> 3 Video </span>
                                </div>
                            </div>
                        </div>

                        <div class="detay-bilgiler">
                            <h2><?=$detay->baslik?></h2>
                            <p>
                                <span>
                                    <?php
                                    for($i=0; $i<$puanlar[0]; $i++ )
                                    {
                                        ?>
                                        <i class="fa-solid fa-star fa-fw"></i>
                                        <?php
                                    }
                                    for($i=0; $i<$puanlar[1]; $i++ )
                                    {
                                        ?>
                                        <i class="fa-regular fa-star fa-fw"></i>
                                        <?php
                                    }
                                    ?>

                                </span>
                                <span><?=$puanlar[2]?> Yorum </span>
                                <span><?=$db->VeriOkuTek("sehirler","baslik","id",$detay->sehir_id)?></span>
                                <span><i class="fa-solid fa-phone fa-fw"></i> <?=$iletisim_bilgileri->telefon?></span>
                                <span><a href="#"><i class="fa-solid fa-envelope fa-fw"></i> E-Posta Gönder</a> </span>
                                <?php
                                if(isset($_SESSION["id"]))
                                {
                                    $uye_favoriler = json_decode($db->VeriOkuTek("uyeler","favori_firmalar","id",$_SESSION["id"]));
                                    if($uye_favoriler=='')
                                    {
                                        ?>
                                        <span data-tur="firma" data-id="<?=$_GET["id"]?>" class="favori favorilere-ekle" style="cursor: pointer"><i class="fa-solid fa-heart fa-fw"></i> Listeme Ekle </span>
                                        <?php
                                    }else
                                    {
                                        if(in_array($_GET["id"],$uye_favoriler))
                                        {
                                            ?>
                                            <span data-tur="firma" data-id="<?=$_GET["id"]?>" class="favori favorilerden-cikar" style="cursor: pointer"><i class="fa-solid fa-heart fa-fw"></i> Listemden Çıkar </span>
                                            <?php
                                        }else
                                        {
                                            ?>
                                            <span data-tur="firma" data-id="<?=$_GET["id"]?>" class="favori favorilere-ekle" style="cursor: pointer"><i class="fa-solid fa-heart fa-fw"></i> Listeme Ekle </span>
                                            <?php
                                        }
                                    }


                                }else
                                {
                                    ?>
                                    <span class="favori favorilere-ekle" style="cursor: pointer"><i class="fa-solid fa-heart fa-fw"></i> Listeme Ekle </span>
                                    <?php
                                }
                                ?>

                            </p>

                            <?php
                            /*
                            ?>

                            <p>Haftasonu, Kokteyl, Paket</p>
                            <p>
                                <span>3.000TL - 7.000TL</span>
                                <span><i class="fa-solid fa-user fa-fw"></i> Üyelere Özel</span>
                                <span>Teklif Al</span>
                            </p>
                            <p>
                                <span>Ücretsiz Hemen <strong> Üye Ol</strong></span>
                                <span><i class="fa-solid fa-gift fa-fw"></i> Kampanyalar </span>
                            </p>
                            */
                            ?>


                            <p>Maksimum Kapasite</p>
                            <p><?=$detay->kapasite?> Kişilik</p>

                            <div class="adresbilgileri">
                                <p><i class="fa-solid fa-map-pin fa-fw"></i> <?=$db->VeriOkuTek("sehirler","baslik","id",$detay->sehir_id)?>,Almanya <a href="#harita" style="cursor: pointer"> Haritada Gör</a> </p>
                                <p><i class="fa-solid fa-gift fa-fw"></i> <?=$modul->uyelik_farki()?> Düğün.co Üyesi</p>
                            </div>
                        </div>

                        <?php
                        /*
                        ?>
                        <div class="detay-favori-yorum">
                            <h4>En Favori Yorum</h4>
                            <div class="resim">
                                <img src="../../images/favoriyorumresim.png" style="width: 100%">
                            </div>
                            <div class="kisibilgileri">
                                <p>Busem & Ömer</p>
                                <p>Düğün Tarihi: 01.01.2023</p>
                                <p> <i class="fa-solid fa-star fa-fw"></i>
                                    <i class="fa-solid fa-star fa-fw"></i>
                                    <i class="fa-solid fa-star fa-fw"></i>
                                    <i class="fa-solid fa-star fa-fw"></i>
                                    <i class="fa-regular fa-star fa-fw"></i>
                                </p>
                            </div>
                            <div class="clearfix"></div>

                            <div class="col-md-12">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic only five centuries, but also the leap into electronic
                                </p>
                            </div>

                        </div>
                        */
                        ?>


                        <div class="detay-fiyatlandirma">
                            <h4>Senfoni Garden Fiyatlandırma</h4>

                            <div class="row">
                                <?php
                                $fiyat_decode =json_decode($detay->fiyatlandirma);
                                foreach($fiyat_decode as $fd)
                                {
                                    ?>
                                    <div class="col-md-6">
                                        <p><?=$fd->baslik?></p>
                                        <p><?=$fd->fiyat?></p>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>

                        <?php
                        /*
                        ?>
                        <div class="detay-fiyatlandirma">
                            <h4>Senfoni Garden Kapasite</h4>

                            <div class="row">
                                <div class="col-md-6">
                                    <p>Minimum Yemekli Fiyat (Hafta sonu)</p>
                                    <p>200-300TL</p>
                                </div>
                                <div class="col-md-6">
                                    <p>Minimum Yemekli Fiyat (Hafta sonu)</p>
                                    <p>200-300TL</p>
                                </div>
                                <div class="col-md-6">
                                    <p>Minimum Kokteyl Fiyat (Hafta sonu)</p>
                                    <p>200-300TL</p>
                                </div>
                                <div class="col-md-6">
                                    <p>Minimum Kokteyl Fiyat (Hafta sonu)</p>
                                    <p>200-300TL</p>
                                </div>
                            </div>
                        </div>
                        */
                        ?>


                        <div class="detay-fiyatlandirma">
                            <h4>Senfoni Garden Genel Özellikler</h4>

                            <div class="row">
                                <?php
                                $ozellikler = json_decode($detay->genel_ozellikler);
                                foreach($ozellikler as $key=> $row)
                                {
                                    $kategori = explode("_",$key);
                                    $karsiliklar = array();
                                    foreach($row as $k)
                                    {
                                        $karsiliklar[]=$db->VeriOkuTek("genel_ozellikler","baslik","id",$k);
                                    }

                                    ?>
                                    <div class="col-md-6">
                                        <p><?=$db->VeriOkuTek("genel_ozellik_kategoriler","baslik","id",$kategori[1])?></p>
                                        <p><?=implode(",",$karsiliklar)?></p>
                                    </div>
                                    <?php

                                }
                                ?>
                            </div>
                        </div>





                        <div class="detay-sikca-sorulan-sorular">
                            <h4>Senfoni Garden Sıkça Sorulan Sorular</h4>

                            <h5>Senfoni Garden’da düğün fiyatları ne kadar?</h5>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                            <h5>Senfoni Garden’da düğün fiyatları ne kadar?</h5>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                            <h5>Senfoni Garden’da düğün fiyatları ne kadar?</h5>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                            <h5>Senfoni Garden’da düğün fiyatları ne kadar?</h5>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                        </div>


                        <div class="detay-sikca-sorulan-sorular">
                            <h4>Senfoni Garden Hakkında</h4>
                            <?php
                            echo $detay->aciklama;
                            ?>
                        </div>


                        <div class="detay-iletisim">
                            <h4>İletişim</h4>

                            <div class="block">
                                <p>Telefon</p>
                                <p><?=$iletisim_bilgileri->telefon?></p>
                            </div>
                            <div class="block">
                                <p>Adres</p>
                                <p><?=$iletisim_bilgileri->adres?></p>
                            </div>
                            <div class="block">
                                <p>E-Posta</p>
                                <p><?=$iletisim_bilgileri->e_mail?></p>
                            </div>

                            <div class="block" id="harita">
                                <p>Harita Konumu</p>
                                <p><?=$iletisim_bilgileri->harita?></p>
                            </div>
                        </div>



                        <div class="detay-yorumlar">
                            <h4>Yorumlar</h4>
                            <p>Bu mekanda mı evlendin? Yorum yaparak burada evlenecek diğer çiftlere yardımcı ol.</p>
                            <div class="genelpuan">
                               <?=$puanlar_round[0]?>
                            </div>
                            <div class="yildizlar">
                                <p>
                                    <?php
                                    for($i=0; $i<$puanlar[0]; $i++ )
                                    {
                                        ?>
                                        <i class="fa-solid fa-star fa-fw"></i>
                                        <?php
                                    }
                                    for($i=0; $i<$puanlar[1]; $i++ )
                                    {
                                        ?>
                                        <i class="fa-regular fa-star fa-fw"></i>
                                        <?php
                                    }
                                    ?>
                                </p>
                            </div>

                            <div class="clearfix"></div>

                            <?php
                            $yorumlar = $modul->all_yorumlar();
                            if($yorumlar===false)
                            {

                            }else
                            {
                                foreach($yorumlar as $row)
                                {
                                    ?>
                                    <div class="yorum">

                                        <div class="resim">
                                            <?php
                                            $gelin_bas_harf = mb_substr($row->gelin_adi,0,1,"UTF-8");
                                            $damat_bas_harf = mb_substr($row->damat_adi,0,1,"UTF-8");
                                            ?>
                                            <?php
                                            echo $gelin_bas_harf."&".$damat_bas_harf;
                                            ?>
                                        </div>
                                        <div class="kisibilgileri">
                                            <p><?=$row->gelin_adi?> & <?=$row->damat_adi?></p>
                                            <p>Düğün Tarihi: <?=$sistem->tarihYaz($row->dugun_tarihi)?></p>
                                            <p>
                                                <?php
                                                for($i=0; $i<$row->puan; $i++ )
                                                {
                                                    ?>
                                                    <i class="fa-solid fa-star fa-fw"></i>
                                                    <?php
                                                }
                                                $eksikolan = 5-$row->puan;
                                                for($i=0; $i<$eksikolan; $i++ )
                                                {
                                                    ?>
                                                    <i class="fa-regular fa-star fa-fw"></i>
                                                    <?php
                                                }
                                                ?>
                                            </p>
                                        </div>
                                        <div class="clearfix"></div>

                                        <div class="col-md-12">
                                            <p><?=$row->yorum?></p>
                                        </div>

                                    </div>
                                    <?php
                                }
                            }

                            ?>





                            <div class="yorum-merak">
                                <p>Yorumunu çok merak ediyoruz 😍</p>
                                <p>Bu firma ile ilgili tecrübelerini yazarak evlenecek çiftlerin <br>karar sürecine yardımcı olabilirsin.</p>
                                <button class="btn btn-primary"  data-bs-toggle="modal" data-bs-target="#exampleModal">Yorum Yap</button>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-3 detaysag">
                        <h3>İletişim Formu</h3>
                        <form method="POST" class="ajaxFormTrue btnhide" action="/<?=$dizin?>Ajax.php?islem=firma-teklif">
                            <input type="hidden" name="firma_id" value="<?=$_GET["id"]?>">
                            <input type="text" name="ad_soyad" class="form-control vRequired" placeholder="Ad Soyad">
                            <input type="text" name="mail_adresi" class="form-control vRequired" placeholder="E-Posta">
                            <input type="text" name="telefon_numarasi" class="form-control vRequired" placeholder="Telefon">
                            <input type="date" name="dugun_tarihi" class="form-control vRequired" placeholder="Düğün Tarihi">
                            <select class="form-control vRequired" name="davetli_sayisi">
                                <option>Davetli Sayısı</option>
                                <option value="0-50">0-50</option>
                                <option value="50-10">50-100</option>
                                <option value="100-150">100-150</option>
                                <option value="150-200">150-200</option>
                                <option value="200-250">200-250</option>
                                <option value="250-300">250-300</option>
                                <option value="300-500">300-500</option>
                                <option value="500-750">500-750</option>
                                <option value="750-1000">750-1.000</option>
                            </select>
                            <textarea name="message" class="form-control vRequired" placeholder="Mesajınız"></textarea>

                            <div class="form-check vCheckRequired">
                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                <label class="form-check-label" for="flexCheckDefault">
                                    Kullanıcı sözleşmesi ve pazarlama izni metinlerini okudum ve kabul ediyorum.
                                </label>
                            </div>

                            <button type="submit" class="btn btn-primary teklifal">Ücretsiz Teklif Al</button>

                            <div class="vAjaxErrors"></div>
                        </form>

                        <div class="uyeol text-center">
                            <img src="/<?=$dizin?>images/uyeolresim">
                            <p>Senfoni Garden seni bekliyor! <br>Şimdi Lorem Ipsun çiftlerine<br> özel %18 indirim avantajıyla!</p>

                            <button type="button" class="btn btn-primary uyeol">Üye Ol</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade yorumyapmodal" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form method="POST" class="ajaxFormTrue" action="/<?=$dizin?>Ajax.php?islem=firma-yorum">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="exampleModalLabel"><?=$detay->baslik?> yorum yapın</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">

                    <input type="hidden" name="tur" value="firma" >
                    <input type="hidden" name="id" value="<?=$detay->id?>" >

                    <label>Gelinin Adı</label>
                    <input type="text" name="gelin_adi" class="form-control vRequired" id="floatingPassword">


                    <label>Damadın Adı</label>
                    <input type="text" name="damat_adi" class="form-control vRequired" id="floatingPassword">


                    <label>Düğün Tarihi</label>
                    <input type="date" name="dugun_tarihi" class="form-control vRequired" id="floatingPassword">


                    <label><?=$detay->baslik?>  için puanınız</label>
                    <input name="puan" class="rating vRequired">

                    <label><?=$detay->baslik?>  için yorumunuz</label>
                    <textarea class="form-control vRequired" name="yorum"></textarea>

                    <div class="col-md-12 vAjaxErrors"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">İptal</button>
                    <button type="submit" class="btn btn-primary">Kaydet</button>
                </div>
            </div>
        </form>


    </div>
</div>
<?php
include "../../inc/footer.php";
?>
