<?php
include "../../inc/header.php";
?>
    <section class="content">
        <div class="container-fluid arama">
            <div class="container">
                <p>Düğün Planlaması</p>
                <p>Büyük gün için yapılacak tüm işleri aylar hatta <br>dilersen yıllar öncesinden planla</p>
                <div class="row">
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-9">
                                <div class="bg">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Nasıl Yardımcı Olalım</label>
                                            <input type="hidden" name="kategori" id="aramakategori-id" value="0">
                                            <input type="hidden" name="kategori_tur" id="aramakategori-tur" value="mekan">
                                            <input type="hidden" name="sehir_id" id="sehir-id" value="1">
                                            <div class="dropdown">
                                                <button class="btn btn-secondary dropdown-toggle kategoribtn" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                    Düğün Mekanları
                                                </button>
                                                <ul class="dropdown-menu scrollbar style-3">
                                                    <div class="force-overflow aramakategoriler">
                                                        <li class="ana"><a class="dropdown-item" href="javascript:void(0);" data-tur="mekan" data-id="0">Düğün Mekanları</a></li>
                                                        <?php
                                                        $kategoriler = $modul->arama_mekan_kategoriler();
                                                        foreach($kategoriler as $k)
                                                        {
                                                            ?>
                                                            <li><a class="dropdown-item" href="javascript:void(0);" data-tur="mekan" data-id="<?=$k->id?>"><?=$k->baslik?></a></li>
                                                            <?php
                                                        }
                                                        ?>
                                                        <li class="ana"><a class="dropdown-item" href="javascript:void(0);" data-tur="firma" data-id="0">Düğün Firmaları</a></li>
                                                        <?php
                                                        $kategoriler = $modul->arama_firma_kategoriler();
                                                        foreach($kategoriler as $k)
                                                        {
                                                            ?>
                                                            <li><a class="dropdown-item" href="javascript:void(0);" data-tur="firma" data-id="<?=$k->id?>"><?=$k->baslik?></a></li>
                                                            <?php
                                                        }
                                                        ?>
                                                    </div>

                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <label>Şehir</label>
                                            <div class="dropdown">
                                                <button class="btn btn-secondary dropdown-toggle sehirbtn" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                   <?=$db->VeriOkuTek("sehirler","baslik","id",1)?>
                                                </button>
                                                <ul class="dropdown-menu  style-3">
                                                    <div class="force-overflow sehirsecimi">
                                                    <?php
                                                    $sehirler = $db->VeriOkuCoklu("sehirler");
                                                    if($sehirler===false)
                                                    {

                                                    }else
                                                    {
                                                        foreach($sehirler as $s)
                                                        {
                                                            ?>
                                                            <li><a class="dropdown-item" href="javascript:void(0);" data-id="<?=$s->id?>"><?=$s->baslik?></a></li>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                    </div>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <button type="button" class="btn btn-primary aramabtn">Filtrele</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid genel-bilgi">
            <div class="container">
                <h3 class="text-center">Düğün.co Nasıl Yardımcı Olur</h3>
                <div class="row">
                    <div class="col-6 col-md-4 block text-center">
                        <i class="fa-solid fa-sliders fa-fw"></i>
                        <p>Düğün Planlaması Yap</p>
                        <p>Yüzlerce içeriğe göz atıp <br> ilham al, düğününü kolayca <br> planla</p>
                    </div>
                    <div class="col-6 col-md-4 block text-center">
                        <i class="fa-solid fa-magnifying-glass fa-fw"></i>
                        <p>Firmaları İncele</p>
                        <p>Firma sayfalarındaki bilgi, fotoğraf ve <br> çift yorumlarına bakarak <br> firmaları yakından
                            tanıyın</p>
                    </div>
                    <div class="col-md-4 block text-center">
                        <i class="fa-regular fa-handshake fa-fw"></i>
                        <p>Sana Uygun Firmalarla Eşleş</p>
                        <p>Beğendiğin firmalar ile <br> iletişime geçerek, fiyat <br> teklifleri al</p>
                    </div>
                </div>
            </div>
        </div>


        <div class="container-fluid gelinlikler">
            <h3 class="text-center">Gelinlikler</h3>
            <p class="text-center">Seni düğünün yıldızı yapacak eb güzel gelinlik modellerini bul<br>, randevunu hemen
                oluştur</p>
            <div class="col-md-12">
                <div class="loop owl-carousel owl-theme">
                    <div class="item hover15">
                        <figure>
                            <a href=""> <img src="../../images/gelinlikresim.png" style="width: 100%"></a>
                        </figure>
                        <p><span>Marka:</span> Yase Gelinlik</p>
                        <p><span>Model:</span> YS2022</p>
                        <p><span>Özellikler:</span> Klasik,Bohem</p>
                        <p>3.500TL-7.000TL</p>
                        <a class="btn btn-primary" href="#">İncele</a>
                    </div>
                    <div class="item hover15">
                        <figure>
                            <a href=""> <img src="../../images/gelinlikresim.png" style="width: 100%"></a>
                        </figure>
                        <p><span>Marka:</span> Yase Gelinlik</p>
                        <p><span>Model:</span> YS2022</p>
                        <p><span>Özellikler:</span> Klasik,Bohem</p>
                        <p>3.500TL-7.000TL</p>
                        <a class="btn btn-primary" href="#">İncele</a>
                    </div>
                    <div class="item hover15">
                        <figure>
                            <a href=""> <img src="../../images/gelinlikresim.png" style="width: 100%"></a>
                        </figure>
                        <p><span>Marka:</span> Yase Gelinlik</p>
                        <p><span>Model:</span> YS2022</p>
                        <p><span>Özellikler:</span> Klasik,Bohem</p>
                        <p>3.500TL-7.000TL</p>
                        <a class="btn btn-primary" href="#">İncele</a>
                    </div>

                    <div class="item hover15">
                        <figure>
                            <a href=""> <img src="../../images/gelinlikresim.png" style="width: 100%"></a>
                        </figure>
                        <p><span>Marka:</span> Yase Gelinlik</p>
                        <p><span>Model:</span> YS2022</p>
                        <p><span>Özellikler:</span> Klasik,Bohem</p>
                        <p>3.500TL-7.000TL</p>
                        <a class="btn btn-primary" href="#">İncele</a>
                    </div>
                    <div class="item hover15">
                        <figure>
                            <a href=""> <img src="../../images/gelinlikresim.png" style="width: 100%"></a>
                        </figure>
                        <p><span>Marka:</span> Yase Gelinlik</p>
                        <p><span>Model:</span> YS2022</p>
                        <p><span>Özellikler:</span> Klasik,Bohem</p>
                        <p>3.500TL-7.000TL</p>
                        <a class="btn btn-primary" href="#">İncele</a>
                    </div>
                    <div class="item hover15">
                        <figure>
                            <a href=""> <img src="../../images/gelinlikresim.png" style="width: 100%"></a>
                        </figure>
                        <p><span>Marka:</span> Yase Gelinlik</p>
                        <p><span>Model:</span> YS2022</p>
                        <p><span>Özellikler:</span> Klasik,Bohem</p>
                        <p>3.500TL-7.000TL</p>
                        <a class="btn btn-primary" href="#">İncele</a>
                    </div>
                    <div class="item hover15">
                        <figure>
                            <a href=""> <img src="../../images/gelinlikresim.png" style="width: 100%"></a>
                        </figure>
                        <p><span>Marka:</span> Yase Gelinlik</p>
                        <p><span>Model:</span> YS2022</p>
                        <p><span>Özellikler:</span> Klasik,Bohem</p>
                        <p>3.500TL-7.000TL</p>
                        <a class="btn btn-primary" href="#">İncele</a>
                    </div>
                    <div class="item hover15">
                        <figure>
                            <a href=""> <img src="../../images/gelinlikresim.png" style="width: 100%"></a>
                        </figure>
                        <p><span>Marka:</span> Yase Gelinlik</p>
                        <p><span>Model:</span> YS2022</p>
                        <p><span>Özellikler:</span> Klasik,Bohem</p>
                        <p>3.500TL-7.000TL</p>
                        <a class="btn btn-primary" href="#">İncele</a>
                    </div>
                    <div class="item hover15">
                        <figure>
                            <a href=""> <img src="../../images/gelinlikresim.png" style="width: 100%"></a>
                        </figure>
                        <p><span>Marka:</span> Yase Gelinlik</p>
                        <p><span>Model:</span> YS2022</p>
                        <p><span>Özellikler:</span> Klasik,Bohem</p>
                        <p>3.500TL-7.000TL</p>
                        <a class="btn btn-primary" href="#">İncele</a>
                    </div>
                    <div class="item hover15">
                        <figure>
                            <a href=""> <img src="../../images/gelinlikresim.png" style="width: 100%"></a>
                        </figure>
                        <p><span>Marka:</span> Yase Gelinlik</p>
                        <p><span>Model:</span> YS2022</p>
                        <p><span>Özellikler:</span> Klasik,Bohem</p>
                        <p>3.500TL-7.000TL</p>
                        <a class="btn btn-primary" href="#">İncele</a>
                    </div>
                    <div class="item hover15">
                        <figure>
                            <a href=""> <img src="../../images/gelinlikresim.png" style="width: 100%"></a>
                        </figure>
                        <p><span>Marka:</span> Yase Gelinlik</p>
                        <p><span>Model:</span> YS2022</p>
                        <p><span>Özellikler:</span> Klasik,Bohem</p>
                        <p>3.500TL-7.000TL</p>
                        <a class="btn btn-primary" href="#">İncele</a>
                    </div>
                </div>

                <p class="tutmunugor"><a href="#">Tümünü İncele</a></p>
            </div>
        </div>

        <div class="container-fluid incele ">
            <div class="row">
                <div class="col-md-6 position-relative right">
                    <a href="#"><img style="width: 100%" src="../../images/incele1.png"> </a>
                    <div class="isim">
                        <p>Gelinlikler</p>
                        <p><a href="#">1500 Firmayı İncele <i class="fa-solid fa-chevron-right fa-fw"></i> </a></p>
                    </div>
                </div>
                <div class="col-md-6 position-relative left">
                    <a href="#"><img style="width: 100%" src="../../images/incele1.png"> </a>
                    <div class="isim">
                        <p>Düğün Firmaları</p>
                        <p><a href="#">1500 Firmayı İncele <i class="fa-solid fa-chevron-right fa-fw"></i></a></p>
                    </div>
                </div>
                <div class="col-md-6 position-relative right">
                    <a href="#"><img style="width: 100%" src="../../images/incele1.png"> </a>
                    <div class="isim">
                        <p>İndirimler</p>
                        <p><a href="#">1500 Firmayı İncele <i class="fa-solid fa-chevron-right fa-fw"></i></a></p>
                    </div>
                </div>
                <div class="col-md-6 position-relative left">
                    <a href="#"><img style="width: 100%" src="../../images/incele1.png"> </a>
                    <div class="isim">
                        <p>Düğün Mekanları</p>
                        <p><a href="#">1500 Firmayı İncele <i class="fa-solid fa-chevron-right fa-fw"></i></a></p>
                    </div>
                </div>
            </div>
        </div>


        <div class="container-fluid indirimleri-yakala ">
            <h3 class="text-center">İndirimleri Yakala</h3>
            <p class="text-center">Düğününü planlarken bütçeni de hesaba kat! Firma<br> indirimleri ve kampanyalar
                burada!</p>
            <div class="col-md-12">
                <div class="loop-indirim owl-carousel owl-theme">
                    <div class="item hover15">
                        <figure>
                            <a href=""> <img src="../../images/gelinlikresim.png" style="width: 100%"></a>
                        </figure>
                        <p><span>Marka:</span> Yase Gelinlik</p>
                        <p><span>Model:</span> YS2022</p>
                        <p><span>Özellikler:</span> Klasik,Bohem</p>
                        <p>3.500TL-7.000TL</p>
                        <a class="btn btn-primary" href="#">İncele</a>
                    </div>
                    <div class="item hover15">
                        <figure>
                            <a href=""> <img src="../../images/gelinlikresim.png" style="width: 100%"></a>
                        </figure>
                        <p><span>Marka:</span> Yase Gelinlik</p>
                        <p><span>Model:</span> YS2022</p>
                        <p><span>Özellikler:</span> Klasik,Bohem</p>
                        <p>3.500TL-7.000TL</p>
                        <a class="btn btn-primary" href="#">İncele</a>
                    </div>
                    <div class="item hover15">
                        <figure>
                            <a href=""> <img src="../../images/gelinlikresim.png" style="width: 100%"></a>
                        </figure>
                        <p><span>Marka:</span> Yase Gelinlik</p>
                        <p><span>Model:</span> YS2022</p>
                        <p><span>Özellikler:</span> Klasik,Bohem</p>
                        <p>3.500TL-7.000TL</p>
                        <a class="btn btn-primary" href="#">İncele</a>
                    </div>

                    <div class="item hover15">
                        <figure>
                            <a href=""> <img src="../../images/gelinlikresim.png" style="width: 100%"></a>
                        </figure>
                        <p><span>Marka:</span> Yase Gelinlik</p>
                        <p><span>Model:</span> YS2022</p>
                        <p><span>Özellikler:</span> Klasik,Bohem</p>
                        <p>3.500TL-7.000TL</p>
                        <a class="btn btn-primary" href="#">İncele</a>
                    </div>
                    <div class="item hover15">
                        <figure>
                            <a href=""> <img src="../../images/gelinlikresim.png" style="width: 100%"></a>
                        </figure>
                        <p><span>Marka:</span> Yase Gelinlik</p>
                        <p><span>Model:</span> YS2022</p>
                        <p><span>Özellikler:</span> Klasik,Bohem</p>
                        <p>3.500TL-7.000TL</p>
                        <a class="btn btn-primary" href="#">İncele</a>
                    </div>
                    <div class="item hover15">
                        <figure>
                            <a href=""> <img src="../../images/gelinlikresim.png" style="width: 100%"></a>
                        </figure>
                        <p><span>Marka:</span> Yase Gelinlik</p>
                        <p><span>Model:</span> YS2022</p>
                        <p><span>Özellikler:</span> Klasik,Bohem</p>
                        <p>3.500TL-7.000TL</p>
                        <a class="btn btn-primary" href="#">İncele</a>
                    </div>
                    <div class="item hover15">
                        <figure>
                            <a href=""> <img src="../../images/gelinlikresim.png" style="width: 100%"></a>
                        </figure>
                        <p><span>Marka:</span> Yase Gelinlik</p>
                        <p><span>Model:</span> YS2022</p>
                        <p><span>Özellikler:</span> Klasik,Bohem</p>
                        <p>3.500TL-7.000TL</p>
                        <a class="btn btn-primary" href="#">İncele</a>
                    </div>
                    <div class="item hover15">
                        <figure>
                            <a href=""> <img src="../../images/gelinlikresim.png" style="width: 100%"></a>
                        </figure>
                        <p><span>Marka:</span> Yase Gelinlik</p>
                        <p><span>Model:</span> YS2022</p>
                        <p><span>Özellikler:</span> Klasik,Bohem</p>
                        <p>3.500TL-7.000TL</p>
                        <a class="btn btn-primary" href="#">İncele</a>
                    </div>
                    <div class="item hover15">
                        <figure>
                            <a href=""> <img src="../../images/gelinlikresim.png" style="width: 100%"></a>
                        </figure>
                        <p><span>Marka:</span> Yase Gelinlik</p>
                        <p><span>Model:</span> YS2022</p>
                        <p><span>Özellikler:</span> Klasik,Bohem</p>
                        <p>3.500TL-7.000TL</p>
                        <a class="btn btn-primary" href="#">İncele</a>
                    </div>
                    <div class="item hover15">
                        <figure>
                            <a href=""> <img src="../../images/gelinlikresim.png" style="width: 100%"></a>
                        </figure>
                        <p><span>Marka:</span> Yase Gelinlik</p>
                        <p><span>Model:</span> YS2022</p>
                        <p><span>Özellikler:</span> Klasik,Bohem</p>
                        <p>3.500TL-7.000TL</p>
                        <a class="btn btn-primary" href="#">İncele</a>
                    </div>
                    <div class="item hover15">
                        <figure>
                            <a href=""> <img src="../../images/gelinlikresim.png" style="width: 100%"></a>
                        </figure>
                        <p><span>Marka:</span> Yase Gelinlik</p>
                        <p><span>Model:</span> YS2022</p>
                        <p><span>Özellikler:</span> Klasik,Bohem</p>
                        <p>3.500TL-7.000TL</p>
                        <a class="btn btn-primary" href="#">İncele</a>
                    </div>
                </div>

                <p class="tutmunugor"><a href="#">Tümünü İncele</a></p>
            </div>
        </div>

        <div class="container-fluid mobil-uygulama">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <h3>Mobil Uygulama</h3>
                        <p>İstediğin heyşey elinin altında</p>
                        <img src="../../images/mobilapp.png">
                        <img src="../../images/mobilapp2.png">
                    </div>
                    <div class="col-md-8 text-right image">
                        <img src="../../images/mobil.png">
                    </div>
                </div>

            </div>
        </div>
    </section>

<?php
include "../../inc/footer.php";
?>