<?php
include "../../inc/header.php";
?>
<section class="content detaycontent">

    <div class="container-fluid">
        <div class="container">
            <div class="col-md-12 ">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?=$siteURL?>">Anasayfa</a></li>
                        <li class="breadcrumb-item"><a href="#">Düğün Mekanları</a></li>
                        <li class="breadcrumb-item active" aria-current="page">İstanbul Düğün Mekanları</li>
                    </ol>
                </nav>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-9">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="detayowl owl-carousel owl-theme">
                                    <div class="item">
                                        <img src="../../images/detayresim.png">
                                    </div>
                                    <div class="item">
                                        <img src="../../images/detayresim.png">
                                    </div>
                                    <div class="item">
                                        <img src="../../images/detayresim.png">
                                    </div>

                                    <div class="item">
                                        <img src="../../images/detayresim.png">
                                    </div>

                                </div>
                                <div class="owl-uzeri">
                                    <span><i class="fa-regular fa-image fa-fw"></i> 12 Görsel </span>
                                    <span><i class="fa-regular fa-image fa-fw"></i> 3 Video </span>
                                </div>
                            </div>
                        </div>

                        <div class="detay-bilgiler">
                            <h2>Senfoni Garden</h2>
                            <p>
                                <span>
                                    <i class="fa-solid fa-star fa-fw"></i>
                                    <i class="fa-solid fa-star fa-fw"></i>
                                    <i class="fa-solid fa-star fa-fw"></i>
                                    <i class="fa-solid fa-star fa-fw"></i>
                                    <i class="fa-regular fa-star fa-fw"></i>
                                </span>
                                <span>51 Yorum </span>
                                <span>İstanbul</span>
                                <span><i class="fa-solid fa-phone fa-fw"></i> +90 500 000 00 00</span>
                                <span><a href="#"><i class="fa-solid fa-envelope fa-fw"></i> E-Posta Gönder</a> </span>
                                <span><i class="fa-solid fa-heart fa-fw"></i> Listeme Ekle </span>
                            </p>

                            <p>Haftasonu, Kokteyl, Paket</p>
                            <p>
                                <span>3.000TL - 7.000TL</span>
                                <span><i class="fa-solid fa-user fa-fw"></i> Üyelere Özel</span>
                                <span>Teklif Al</span>
                            </p>
                            <p>
                                <span>Ücretsiz Hemen <strong> Üye Ol</strong></span>
                                <span><i class="fa-solid fa-gift fa-fw"></i> Kampanyalar </span>
                            </p>


                            <p>Maksimum Kapasite</p>
                            <p>200-300 Kişilik</p>

                            <div class="adresbilgileri">
                                <p><i class="fa-solid fa-map-pin fa-fw"></i> Şişli, İstanbul Haritada Gör </p>
                                <p><i class="fa-solid fa-bolt-lightning fa-fw"></i> Hızlı firma 24 saat içinde cevap veriyorlar </p>
                                <p><i class="fa-solid fa-gift fa-fw"></i> 2 Yıllık Lorem Ipsun Üyesi</p>
                            </div>
                        </div>

                        <div class="detay-favori-yorum">
                            <h4>En Favori Yorum</h4>
                            <div class="resim">
                                <img src="../../images/favoriyorumresim.png" style="width: 100%">
                            </div>
                            <div class="kisibilgileri">
                                <p>Busem & Ömer</p>
                                <p>Düğün Tarihi: 01.01.2023</p>
                                <p> <i class="fa-solid fa-star fa-fw"></i>
                                    <i class="fa-solid fa-star fa-fw"></i>
                                    <i class="fa-solid fa-star fa-fw"></i>
                                    <i class="fa-solid fa-star fa-fw"></i>
                                    <i class="fa-regular fa-star fa-fw"></i>
                                </p>
                            </div>
                            <div class="clearfix"></div>

                            <div class="col-md-12">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic only five centuries, but also the leap into electronic
                                </p>
                            </div>

                        </div>


                        <div class="detay-fiyatlandirma">
                            <h4>Senfoni Garden Fiyatlandırma</h4>

                            <div class="row">
                                <div class="col-md-6">
                                    <p>Minimum Yemekli Fiyat (Hafta sonu)</p>
                                    <p>200-300TL</p>
                                </div>
                                <div class="col-md-6">
                                    <p>Minimum Yemekli Fiyat (Hafta sonu)</p>
                                    <p>200-300TL</p>
                                </div>
                                <div class="col-md-6">
                                    <p>Minimum Kokteyl Fiyat (Hafta sonu)</p>
                                    <p>200-300TL</p>
                                </div>
                                <div class="col-md-6">
                                    <p>Minimum Kokteyl Fiyat (Hafta sonu)</p>
                                    <p>200-300TL</p>
                                </div>
                            </div>
                        </div>


                        <div class="detay-fiyatlandirma">
                            <h4>Senfoni Garden Kapasite</h4>

                            <div class="row">
                                <div class="col-md-6">
                                    <p>Minimum Yemekli Fiyat (Hafta sonu)</p>
                                    <p>200-300TL</p>
                                </div>
                                <div class="col-md-6">
                                    <p>Minimum Yemekli Fiyat (Hafta sonu)</p>
                                    <p>200-300TL</p>
                                </div>
                                <div class="col-md-6">
                                    <p>Minimum Kokteyl Fiyat (Hafta sonu)</p>
                                    <p>200-300TL</p>
                                </div>
                                <div class="col-md-6">
                                    <p>Minimum Kokteyl Fiyat (Hafta sonu)</p>
                                    <p>200-300TL</p>
                                </div>
                            </div>
                        </div>


                        <div class="detay-fiyatlandirma">
                            <h4>Senfoni Garden Genel Özellikler</h4>

                            <div class="row">
                                <div class="col-md-6">
                                    <p>Davet Alanları</p>
                                    <p>Sadece açık alan</p>
                                </div>
                                <div class="col-md-6">
                                    <p>Mekan Özellikleri
                                    </p>
                                    <p>Bahçe, Çim alan, Orman içinde</p>
                                </div>
                                <div class="col-md-6">
                                    <p>Manzara</p>
                                    <p>Panaromik manzara, Park manzaralı, Orman manzaralı, Kır manzaralı, Bahçe manzaralı, Doğa manzaralı
                                    </p>
                                </div>

                            </div>
                        </div>


                        <div class="detay-ozellikler">
                            <h4>Senfoni Garden Özellikler</h4>

                            <div class="row">
                                <div class="col-md-6">
                                    <p><i class="fa-solid fa-check fa-fw"></i>  Yemek Servisi</p>
                                </div>
                                <div class="col-md-6">
                                    <p><i class="fa-solid fa-check fa-fw"></i> Yemek Servisi</p>
                                </div>
                                <div class="col-md-6">
                                    <p><i class="fa-solid fa-check fa-fw"></i>  Yemek Servisi</p>
                                </div>
                                <div class="col-md-6">
                                    <p><i class="fa-solid fa-check fa-fw"></i> Yemek Servisi</p>
                                </div>
                                <div class="col-md-6">
                                    <p><i class="fa-solid fa-check fa-fw"></i>  Yemek Servisi</p>
                                </div>
                                <div class="col-md-6">
                                    <p><i class="fa-solid fa-check fa-fw"></i> Yemek Servisi</p>
                                </div>
                                <div class="col-md-6">
                                    <p><i class="fa-solid fa-check fa-fw"></i>  Yemek Servisi</p>
                                </div>
                                <div class="col-md-6">
                                    <p><i class="fa-solid fa-check fa-fw"></i> Yemek Servisi</p>
                                </div>
                                <div class="col-md-6">
                                    <p><i class="fa-solid fa-check fa-fw"></i>  Yemek Servisi</p>
                                </div>
                                <div class="col-md-6">
                                    <p><i class="fa-solid fa-check fa-fw"></i> Yemek Servisi</p>
                                </div>
                            </div>
                        </div>


                        <div class="detay-sikca-sorulan-sorular">
                            <h4>Senfoni Garden Sıkça Sorulan Sorular</h4>

                            <h5>Senfoni Garden’da düğün fiyatları ne kadar?</h5>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                            <h5>Senfoni Garden’da düğün fiyatları ne kadar?</h5>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                            <h5>Senfoni Garden’da düğün fiyatları ne kadar?</h5>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                            <h5>Senfoni Garden’da düğün fiyatları ne kadar?</h5>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                        </div>


                        <div class="detay-sikca-sorulan-sorular">
                            <h4>Senfoni Garden Hakkında</h4>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap

                                into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                            </p>
                            <h5>
                                Davet Alanları
                            </h5>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap

                                into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                            </p>
                            <h5>
                                Davet Alanları
                            </h5>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap

                                into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                            </p>
                        </div>


                        <div class="detay-iletisim">
                            <h4>İletişim</h4>

                            <div class="block">
                                <p>Telefon</p>
                                <p>+90 500 000 00 00</p>
                            </div>
                            <div class="block">
                                <p>Adres</p>
                                <p>is simply dummy text of the printing and typesetting industry. Lorem Ipsum</p>
                            </div>
                            <div class="block">
                                <p>Nasıl Gelinir?</p>
                                <p>is simply dummy text of the printing and typesetting industry. Lorem Ipsum</p>
                            </div>
                            <div class="block">
                                <p>E-Posta</p>
                                <p>info@loremıpsun.com</p>
                            </div>

                            <div class="block">
                                <p>Harita Konumu</p>
                                <p><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d34062.45921178033!2d39.67686435780646!3d41.00940314800655!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40643e79bd76252b%3A0x195746e94e495a84!2sBe%C5%9Filli%20Sahilli%20EKO%20Park%C4%B1!5e0!3m2!1str!2str!4v1673520815326!5m2!1str!2str" width="100%" height="250" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe></p>
                            </div>
                        </div>

                        <div class="detay-yorumlar">
                            <h4>En Favori Yorum</h4>
                            <p>Bu mekanda mı evlendin? Yorum yaparak burada evlenecek diğer çiftlere yardımcı ol.</p>
                            <div class="genelpuan">
                                4.2
                            </div>
                            <div class="yildizlar">
                                <p> <i class="fa-solid fa-star fa-fw"></i>
                                    <i class="fa-solid fa-star fa-fw"></i>
                                    <i class="fa-solid fa-star fa-fw"></i>
                                    <i class="fa-solid fa-star fa-fw"></i>
                                    <i class="fa-regular fa-star fa-fw"></i>
                                    Kalite
                                </p>
                                <p> <i class="fa-solid fa-star fa-fw"></i>
                                    <i class="fa-solid fa-star fa-fw"></i>
                                    <i class="fa-solid fa-star fa-fw"></i>
                                    <i class="fa-solid fa-star fa-fw"></i>
                                    <i class="fa-regular fa-star fa-fw"></i>
                                    Personel
                                </p>
                                <p> <i class="fa-solid fa-star fa-fw"></i>
                                    <i class="fa-solid fa-star fa-fw"></i>
                                    <i class="fa-solid fa-star fa-fw"></i>
                                    <i class="fa-solid fa-star fa-fw"></i>
                                    <i class="fa-regular fa-star fa-fw"></i>
                                    İletişim
                                </p>
                            </div>

                            <div class="clearfix"></div>
                            <div class="yorum">

                                <div class="resim">
                                    <img src="../../images/favoriyorumresim.png" style="width: 100%">
                                </div>
                                <div class="kisibilgileri">
                                    <p>Busem & Ömer</p>
                                    <p>Düğün Tarihi: 01.01.2023</p>
                                    <p> <i class="fa-solid fa-star fa-fw"></i>
                                        <i class="fa-solid fa-star fa-fw"></i>
                                        <i class="fa-solid fa-star fa-fw"></i>
                                        <i class="fa-solid fa-star fa-fw"></i>
                                        <i class="fa-regular fa-star fa-fw"></i>
                                    </p>
                                </div>
                                <div class="clearfix"></div>

                                <div class="col-md-12">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic only five centuries, but also the leap into electronic
                                    </p>
                                </div>

                            </div>
                            <div class="yorum">

                                <div class="resim">
                                    <img src="../../images/favoriyorumresim.png" style="width: 100%">
                                </div>
                                <div class="kisibilgileri">
                                    <p>Busem & Ömer</p>
                                    <p>Düğün Tarihi: 01.01.2023</p>
                                    <p> <i class="fa-solid fa-star fa-fw"></i>
                                        <i class="fa-solid fa-star fa-fw"></i>
                                        <i class="fa-solid fa-star fa-fw"></i>
                                        <i class="fa-solid fa-star fa-fw"></i>
                                        <i class="fa-regular fa-star fa-fw"></i>
                                    </p>
                                </div>
                                <div class="clearfix"></div>

                                <div class="col-md-12">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic only five centuries, but also the leap into electronic
                                    </p>
                                </div>

                            </div>
                            <div class="yorum">

                                <div class="resim">
                                    <img src="../../images/favoriyorumresim.png" style="width: 100%">
                                </div>
                                <div class="kisibilgileri">
                                    <p>Busem & Ömer</p>
                                    <p>Düğün Tarihi: 01.01.2023</p>
                                    <p> <i class="fa-solid fa-star fa-fw"></i>
                                        <i class="fa-solid fa-star fa-fw"></i>
                                        <i class="fa-solid fa-star fa-fw"></i>
                                        <i class="fa-solid fa-star fa-fw"></i>
                                        <i class="fa-regular fa-star fa-fw"></i>
                                    </p>
                                </div>
                                <div class="clearfix"></div>

                                <div class="col-md-12">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic only five centuries, but also the leap into electronic
                                    </p>
                                </div>

                            </div>


                            <div class="yorum-merak">
                                <p>Yorumunu çok merak ediyoruz 😍</p>
                                <p>Bu firma ile ilgili tecrübelerini yazarak evlenecek çiftlerin <br>karar sürecine yardımcı olabilirsin.</p>
                                <button class="btn btn-primary">Yorum Yap</button>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-3 detaysag">
                        <h3>İletişim Formu</h3>
                        <form method="POST">
                            <input type="text" class="form-control" placeholder="Ad Soyad">
                            <input type="text" class="form-control" placeholder="E-Posta">
                            <input type="text" class="form-control" placeholder="Telefon">
                            <input type="text" class="form-control" placeholder="Düğün Tarihi">
                            <select class="form-control">
                                <option>Davetli Sayısı</option>
                            </select>
                            <textarea class="form-control" placeholder="Mesajınız"></textarea>

                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                <label class="form-check-label" for="flexCheckDefault">
                                    Kullanıcı sözleşmesi ve pazarlama izni metinlerini okudum ve kabul ediyorum.
                                </label>
                            </div>

                            <button type="button" class="btn btn-primary teklifal">Ücretsiz Teklif Al</button>
                        </form>

                        <div class="uyeol text-center">
                            <img src="../../images/uyeolresim">
                            <p>Senfoni Garden seni bekliyor! <br>Şimdi Lorem Ipsun çiftlerine<br> özel %18 indirim avantajıyla!</p>

                            <button type="button" class="btn btn-primary uyeol">Üye Ol</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
include "../../inc/footer.php";
?>
