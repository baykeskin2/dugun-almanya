<?php
include "../../inc/header.php";
?>
<section class="content">
    <div class="container-fluid arama">
        <div class="container">
            <p>Düğün Planlaması</p>
            <p>Büyük gün için yapılacak tüm işleri aylar hatta <br>dilersen yıllar öncesinden planla</p>
            <div class="row">
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="bg">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Nasıl Yardımcı Olalım</label>
                                        <select class="form-select bootstrap-select">
                                            <option>Düğün Mekanları</option>
                                            <option value="1">One</option>
                                            <option value="2">Two</option>
                                            <option value="3">Three</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Şehir</label>
                                        <select class="form-select">
                                            <option>İstanbul</option>
                                            <option value="1">One</option>
                                            <option value="2">Two</option>
                                            <option value="3">Three</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <button type="button" class="btn btn-primary">Filtrele</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="mySidenav" class="sidenav">
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
        <div class="accordion" id="filtreler">
            <div class="accordion-item">
                <h2 class="accordion-header" id="heading-bolgeler">
                    <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#bolgeler" aria-expanded="true" aria-controls="bolgeler">
                        Bölge
                    </button>
                </h2>
                <div id="bolgeler" class="accordion-collapse collapse show" aria-labelledby="bolgeler" data-bs-parent="#filtreler">
                    <div class="accordion-body">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                            <label class="form-check-label" for="flexCheckDefault">
                                İstanbul (18)
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                            <label class="form-check-label" for="flexCheckDefault">
                                İstanbul (18)
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                            <label class="form-check-label" for="flexCheckDefault">
                                Ankara (18)
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                            <label class="form-check-label" for="flexCheckDefault">
                                İzmir (18)
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                            <label class="form-check-label" for="flexCheckDefault">
                                Bursa (18)
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="accordion-item">
                <h2 class="accordion-header" id="heading-ilce">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#ilce" aria-expanded="false" aria-controls="ilce">
                        İlçe
                    </button>
                </h2>
                <div id="ilce" class="accordion-collapse collapse" aria-labelledby="ilce" data-bs-parent="#filtreler">
                    <div class="accordion-body">
                        <strong>This is the second item's accordion body.</strong> It is hidden by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables. It's also worth noting that just about any HTML can go within the <code>.accordion-body</code>, though the transition does limit overflow.
                    </div>
                </div>
            </div>
            <div class="accordion-item">
                <h2 class="accordion-header" id="heading-mekan-turu">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#mekan-turu" aria-expanded="false" aria-controls="mekan-turu">
                        Mekan Türü
                    </button>
                </h2>
                <div id="mekan-turu" class="accordion-collapse collapse" aria-labelledby="mekan-turu" data-bs-parent="#filtreler">
                    <div class="accordion-body">
                        <strong>This is the third item's accordion body.</strong> It is hidden by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables. It's also worth noting that just about any HTML can go within the <code>.accordion-body</code>, though the transition does limit overflow.
                    </div>
                </div>
            </div>

            <div class="accordion-item">
                <h2 class="accordion-header" id="heading-kapasite">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#kapasite" aria-expanded="false" aria-controls="kapasite">
                        Kapasite
                    </button>
                </h2>
                <div id="kapasite" class="accordion-collapse collapse" aria-labelledby="kapasite" data-bs-parent="#filtreler">
                    <div class="accordion-body">
                        <strong>This is the third item's accordion body.</strong> It is hidden by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables. It's also worth noting that just about any HTML can go within the <code>.accordion-body</code>, though the transition does limit overflow.
                    </div>
                </div>
            </div>

            <div class="accordion-item">
                <h2 class="accordion-header" id="heading-ozellikler">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#ozellikler" aria-expanded="false" aria-controls="ozellikler">
                        Mekan Özellikleri
                    </button>
                </h2>
                <div id="ozellikler" class="accordion-collapse collapse" aria-labelledby="ozellikler" data-bs-parent="#filtreler">
                    <div class="accordion-body">
                        <strong>This is the third item's accordion body.</strong> It is hidden by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables. It's also worth noting that just about any HTML can go within the <code>.accordion-body</code>, though the transition does limit overflow.
                    </div>
                </div>
            </div>
        </div>
    </div>
    <p class="opennav" onclick="openNav()">&#9776; Mekanları Fitrele</p>
    <div class="container-fluid vitrindeki-mekanlar">
        <div class="container">

            <div class="row">
                <div class="col-md-3 kategorifiltreler">
                    <div class="accordion" id="filtreler">
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="heading-bolgeler">
                                <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#bolgeler" aria-expanded="true" aria-controls="bolgeler">
                                  Bölge
                                </button>
                            </h2>
                            <div id="bolgeler" class="accordion-collapse collapse show" aria-labelledby="bolgeler" data-bs-parent="#filtreler">
                                <div class="accordion-body">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                        <label class="form-check-label" for="flexCheckDefault">
                                            İstanbul (18)
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                        <label class="form-check-label" for="flexCheckDefault">
                                            İstanbul (18)
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                        <label class="form-check-label" for="flexCheckDefault">
                                            Ankara (18)
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                        <label class="form-check-label" for="flexCheckDefault">
                                            İzmir (18)
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                        <label class="form-check-label" for="flexCheckDefault">
                                            Bursa (18)
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="heading-ilce">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#ilce" aria-expanded="false" aria-controls="ilce">
                                    İlçe
                                </button>
                            </h2>
                            <div id="ilce" class="accordion-collapse collapse" aria-labelledby="ilce" data-bs-parent="#filtreler">
                                <div class="accordion-body">
                                    <strong>This is the second item's accordion body.</strong> It is hidden by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables. It's also worth noting that just about any HTML can go within the <code>.accordion-body</code>, though the transition does limit overflow.
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="heading-mekan-turu">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#mekan-turu" aria-expanded="false" aria-controls="mekan-turu">
                                    Mekan Türü
                                </button>
                            </h2>
                            <div id="mekan-turu" class="accordion-collapse collapse" aria-labelledby="mekan-turu" data-bs-parent="#filtreler">
                                <div class="accordion-body">
                                    <strong>This is the third item's accordion body.</strong> It is hidden by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables. It's also worth noting that just about any HTML can go within the <code>.accordion-body</code>, though the transition does limit overflow.
                                </div>
                            </div>
                        </div>

                        <div class="accordion-item">
                            <h2 class="accordion-header" id="heading-kapasite">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#kapasite" aria-expanded="false" aria-controls="kapasite">
                                    Kapasite
                                </button>
                            </h2>
                            <div id="kapasite" class="accordion-collapse collapse" aria-labelledby="kapasite" data-bs-parent="#filtreler">
                                <div class="accordion-body">
                                    <strong>This is the third item's accordion body.</strong> It is hidden by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables. It's also worth noting that just about any HTML can go within the <code>.accordion-body</code>, though the transition does limit overflow.
                                </div>
                            </div>
                        </div>

                        <div class="accordion-item">
                            <h2 class="accordion-header" id="heading-ozellikler">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#ozellikler" aria-expanded="false" aria-controls="ozellikler">
                                    Mekan Özellikleri
                                </button>
                            </h2>
                            <div id="ozellikler" class="accordion-collapse collapse" aria-labelledby="ozellikler" data-bs-parent="#filtreler">
                                <div class="accordion-body">
                                    <strong>This is the third item's accordion body.</strong> It is hidden by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables. It's also worth noting that just about any HTML can go within the <code>.accordion-body</code>, though the transition does limit overflow.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="col-md-12 aramasonucsayisi">
                        <div class="row">
                            <div class="col-md-6">
                                <p>1.212 düğün mekanları bulundu.</p>
                            </div>
                            <div class="col-md-6 text-right">
                               <span><a href="#">Haritayı Aç</a> </span>
                               <span><a href="#">Yakınımda Ara</a> </span>
                            </div>
                        </div>
                    </div>
                    <h3>Vitrindeki Düğün Mekanları</h3>
                    <div class="row">
                        <div class="col-6 col-md-3">
                            <div class="bg">
                                <img src="../../images/vitrin-mekan.png" style="width: 100%">
                                <p><a href="#">Şanzelize yolu</a> </p>
                            </div>

                        </div>
                        <div class="col-6 col-md-3">
                            <div class="bg">
                                <img src="../../images/vitrin-mekan.png" style="width: 100%">
                                <p><a href="#">Şanzelize yolu</a> </p>
                            </div>

                        </div>
                        <div class="col-6 col-md-3">
                            <div class="bg">
                                <img src="../../images/vitrin-mekan.png" style="width: 100%">
                                <p><a href="#">Şanzelize yolu</a> </p>
                            </div>

                        </div>
                        <div class="col-6 col-md-3">
                            <div class="bg">
                                <img src="../../images/vitrin-mekan.png" style="width: 100%">
                                <p><a href="#">Şanzelize yolu</a> </p>
                            </div>

                        </div>
                        <div class="col-6 col-md-3">
                            <div class="bg">
                                <img src="../../images/vitrin-mekan.png" style="width: 100%">
                                <p><a href="#">Şanzelize yolu</a> </p>
                            </div>

                        </div>
                        <div class="col-6 col-md-3">
                            <div class="bg">
                                <img src="../../images/vitrin-mekan.png" style="width: 100%">
                                <p><a href="#">Şanzelize yolu</a> </p>
                            </div>

                        </div>
                        <div class="col-6 col-md-3">
                            <div class="bg">
                                <img src="../../images/vitrin-mekan.png" style="width: 100%">
                                <p><a href="#">Şanzelize yolu</a> </p>
                            </div>

                        </div>
                        <div class="col-6 col-md-3">
                            <div class="bg">
                                <img src="../../images/vitrin-mekan.png" style="width: 100%">
                                <p><a href="#">Şanzelize yolu</a> </p>
                            </div>

                        </div>
                    </div>

                    <div class="col-md-12 mekan">
                        <div class="bg">
                            <div class="row">
                                <div class="col-md-3 resim">
                                    <a href="#"><img src="../../images/thumb_svadba-camlica-tepesi_3LP72GPD.jpeg" class="w-100"> </a>
                                </div>
                                <div class="col-md-9">
                                    <h3><a href="#">Svadba Çamlıca</a> </h3>
                                    <p><a href="#">Üsküdar</a> </p>
                                    <p>İstanbul, Üsküdar’da düğün mekanları için araştırma yapıyorsanız size Svadba Çamlıca'yı önerebiliriz. Tesisimiz eşsiz Boğaz ve doğa manzarasıyla dikkat çekiyor. Hem açık, hem de kapalı alanlarıyla...</p>
                                    <div class="row mt-15">
                                        <div class="col-md-6">
                                            <div class="k-p">
                                                <p>Maks. Kapasite</p>
                                                <p>500-750 kişi</p>
                                            </div>
                                            <div class="k-p">
                                                <p>Min. Yemekli</p>
                                                <p>100 - 200 TL</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6 butonlar">
                                            <a href="#" class="btn btn-primary teklifbtn">Ücretsiz Teklif Al</a>
                                            <a href="#" class="btn btn-primary incelebtn">Mekanı İncele</a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 mekan">
                        <div class="bg">
                            <div class="row">
                                <div class="col-md-3 resim">
                                    <a href="#"><img src="../../images/thumb_svadba-camlica-tepesi_3LP72GPD.jpeg" class="w-100"> </a>
                                </div>
                                <div class="col-md-9">
                                    <h3><a href="#">Svadba Çamlıca</a> </h3>
                                    <p><a href="#">Üsküdar</a> </p>
                                    <p>İstanbul, Üsküdar’da düğün mekanları için araştırma yapıyorsanız size Svadba Çamlıca'yı önerebiliriz. Tesisimiz eşsiz Boğaz ve doğa manzarasıyla dikkat çekiyor. Hem açık, hem de kapalı alanlarıyla...</p>
                                    <div class="row mt-15">
                                        <div class="col-md-6">
                                            <div class="k-p">
                                                <p>Maks. Kapasite</p>
                                                <p>500-750 kişi</p>
                                            </div>
                                            <div class="k-p">
                                                <p>Min. Yemekli</p>
                                                <p>100 - 200 TL</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6 butonlar">
                                            <a href="#" class="btn btn-primary teklifbtn">Ücretsiz Teklif Al</a>
                                            <a href="#" class="btn btn-primary incelebtn">Mekanı İncele</a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 mekan">
                        <div class="bg">
                            <div class="row">
                                <div class="col-md-3 resim">
                                    <a href="#"><img src="../../images/thumb_svadba-camlica-tepesi_3LP72GPD.jpeg" class="w-100"> </a>
                                </div>
                                <div class="col-md-9">
                                    <h3><a href="#">Svadba Çamlıca</a> </h3>
                                    <p><a href="#">Üsküdar</a> </p>
                                    <p>İstanbul, Üsküdar’da düğün mekanları için araştırma yapıyorsanız size Svadba Çamlıca'yı önerebiliriz. Tesisimiz eşsiz Boğaz ve doğa manzarasıyla dikkat çekiyor. Hem açık, hem de kapalı alanlarıyla...</p>
                                    <div class="row mt-15">
                                        <div class="col-md-6">
                                            <div class="k-p">
                                                <p>Maks. Kapasite</p>
                                                <p>500-750 kişi</p>
                                            </div>
                                            <div class="k-p">
                                                <p>Min. Yemekli</p>
                                                <p>100 - 200 TL</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6 butonlar">
                                            <a href="#" class="btn btn-primary teklifbtn">Ücretsiz Teklif Al</a>
                                            <a href="#" class="btn btn-primary incelebtn">Mekanı İncele</a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 mekan">
                        <div class="bg">
                            <div class="row">
                                <div class="col-md-3 resim">
                                    <a href="#"><img src="../../images/thumb_svadba-camlica-tepesi_3LP72GPD.jpeg" class="w-100"> </a>
                                </div>
                                <div class="col-md-9">
                                    <h3><a href="#">Svadba Çamlıca</a> </h3>
                                    <p><a href="#">Üsküdar</a> </p>
                                    <p>İstanbul, Üsküdar’da düğün mekanları için araştırma yapıyorsanız size Svadba Çamlıca'yı önerebiliriz. Tesisimiz eşsiz Boğaz ve doğa manzarasıyla dikkat çekiyor. Hem açık, hem de kapalı alanlarıyla...</p>
                                    <div class="row mt-15">
                                        <div class="col-md-6">
                                            <div class="k-p">
                                                <p>Maks. Kapasite</p>
                                                <p>500-750 kişi</p>
                                            </div>
                                            <div class="k-p">
                                                <p>Min. Yemekli</p>
                                                <p>100 - 200 TL</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6 butonlar">
                                            <a href="#" class="btn btn-primary teklifbtn">Ücretsiz Teklif Al</a>
                                            <a href="#" class="btn btn-primary incelebtn">Mekanı İncele</a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 mekan">
                        <div class="bg">
                            <div class="row">
                                <div class="col-md-3 resim">
                                    <a href="#"><img src="../../images/thumb_svadba-camlica-tepesi_3LP72GPD.jpeg" class="w-100"> </a>
                                </div>
                                <div class="col-md-9">
                                    <h3><a href="#">Svadba Çamlıca</a> </h3>
                                    <p><a href="#">Üsküdar</a> </p>
                                    <p>İstanbul, Üsküdar’da düğün mekanları için araştırma yapıyorsanız size Svadba Çamlıca'yı önerebiliriz. Tesisimiz eşsiz Boğaz ve doğa manzarasıyla dikkat çekiyor. Hem açık, hem de kapalı alanlarıyla...</p>
                                    <div class="row mt-15">
                                        <div class="col-md-6">
                                            <div class="k-p">
                                                <p>Maks. Kapasite</p>
                                                <p>500-750 kişi</p>
                                            </div>
                                            <div class="k-p">
                                                <p>Min. Yemekli</p>
                                                <p>100 - 200 TL</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6 butonlar">
                                            <a href="#" class="btn btn-primary teklifbtn">Ücretsiz Teklif Al</a>
                                            <a href="#" class="btn btn-primary incelebtn">Mekanı İncele</a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 mekan">
                        <div class="bg">
                            <div class="row">
                                <div class="col-md-3 resim">
                                    <a href="#"><img src="../../images/thumb_svadba-camlica-tepesi_3LP72GPD.jpeg" class="w-100"> </a>
                                </div>
                                <div class="col-md-9">
                                    <h3><a href="#">Svadba Çamlıca</a> </h3>
                                    <p><a href="#">Üsküdar</a> </p>
                                    <p>İstanbul, Üsküdar’da düğün mekanları için araştırma yapıyorsanız size Svadba Çamlıca'yı önerebiliriz. Tesisimiz eşsiz Boğaz ve doğa manzarasıyla dikkat çekiyor. Hem açık, hem de kapalı alanlarıyla...</p>
                                    <div class="row mt-15">
                                        <div class="col-md-6">
                                            <div class="k-p">
                                                <p>Maks. Kapasite</p>
                                                <p>500-750 kişi</p>
                                            </div>
                                            <div class="k-p">
                                                <p>Min. Yemekli</p>
                                                <p>100 - 200 TL</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6 butonlar">
                                            <a href="#" class="btn btn-primary teklifbtn">Ücretsiz Teklif Al</a>
                                            <a href="#" class="btn btn-primary incelebtn">Mekanı İncele</a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 mekan">
                        <div class="bg">
                            <div class="row">
                                <div class="col-md-3 resim">
                                    <a href="#"><img src="../../images/thumb_svadba-camlica-tepesi_3LP72GPD.jpeg" class="w-100"> </a>
                                </div>
                                <div class="col-md-9">
                                    <h3><a href="#">Svadba Çamlıca</a> </h3>
                                    <p><a href="#">Üsküdar</a> </p>
                                    <p>İstanbul, Üsküdar’da düğün mekanları için araştırma yapıyorsanız size Svadba Çamlıca'yı önerebiliriz. Tesisimiz eşsiz Boğaz ve doğa manzarasıyla dikkat çekiyor. Hem açık, hem de kapalı alanlarıyla...</p>
                                    <div class="row mt-15">
                                        <div class="col-md-6">
                                            <div class="k-p">
                                                <p>Maks. Kapasite</p>
                                                <p>500-750 kişi</p>
                                            </div>
                                            <div class="k-p">
                                                <p>Min. Yemekli</p>
                                                <p>100 - 200 TL</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6 butonlar">
                                            <a href="#" class="btn btn-primary teklifbtn">Ücretsiz Teklif Al</a>
                                            <a href="#" class="btn btn-primary incelebtn">Mekanı İncele</a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 kategoriaciklama">
                    <div class="bg">
                        <h4>What is Lorem Ipsum?</h4>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                        <br>
                        <h4>What is Lorem Ipsum?</h4>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                        <br>
                        <h4>What is Lorem Ipsum?</h4>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                        <br>
                        <h4>What is Lorem Ipsum?</h4>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>

                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<?php
include "../../inc/footer.php";
?>
