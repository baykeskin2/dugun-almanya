<?php
include "../../inc/header.php";
?>
<section class="content">
    <div class="container-fluid arama">
        <div class="container">
            <p>Düğün Planlaması</p>
            <p>Büyük gün için yapılacak tüm işleri aylar hatta <br>dilersen yıllar öncesinden planla</p>
            <div class="row">
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="bg">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Nasıl Yardımcı Olalım</label>
                                        <input type="hidden" name="kategori" id="aramakategori-id" value="0">
                                        <input type="hidden" name="kategori_tur" id="aramakategori-tur" value="mekan">
                                        <input type="hidden" name="sehir_id" id="sehir-id" value="1">
                                        <div class="dropdown">
                                            <button class="btn btn-secondary dropdown-toggle kategoribtn" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                Düğün Mekanları
                                            </button>
                                            <ul class="dropdown-menu scrollbar style-3">
                                                <div class="force-overflow aramakategoriler">
                                                    <li class="ana"><a class="dropdown-item" href="javascript:void(0);" data-tur="mekan" data-id="0">Düğün Mekanları</a></li>
                                                    <?php
                                                    $kategoriler = $modul->arama_mekan_kategoriler();
                                                    foreach($kategoriler as $k)
                                                    {
                                                        ?>
                                                        <li><a class="dropdown-item" href="javascript:void(0);" data-tur="mekan" data-id="<?=$k->id?>"><?=$k->baslik?></a></li>
                                                        <?php
                                                    }
                                                    ?>
                                                    <li class="ana"><a class="dropdown-item" href="javascript:void(0);" data-tur="firma" data-id="0">Düğün Firmaları</a></li>
                                                    <?php
                                                    $kategoriler = $modul->arama_firma_kategoriler();
                                                    foreach($kategoriler as $k)
                                                    {
                                                        ?>
                                                        <li><a class="dropdown-item" href="javascript:void(0);" data-tur="firma" data-id="<?=$k->id?>"><?=$k->baslik?></a></li>
                                                        <?php
                                                    }
                                                    ?>
                                                </div>

                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Şehir</label>
                                        <div class="dropdown">
                                            <button class="btn btn-secondary dropdown-toggle sehirbtn" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                <?=$db->VeriOkuTek("sehirler","baslik","id",1)?>
                                            </button>
                                            <ul class="dropdown-menu  style-3">
                                                <div class="force-overflow sehirsecimi">
                                                    <?php
                                                    $sehirler = $db->VeriOkuCoklu("sehirler");
                                                    if($sehirler===false)
                                                    {

                                                    }else
                                                    {
                                                        foreach($sehirler as $s)
                                                        {
                                                            ?>
                                                            <li><a class="dropdown-item" href="javascript:void(0);" data-id="<?=$s->id?>"><?=$s->baslik?></a></li>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </div>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <button type="button" class="btn btn-primary aramabtn">Filtrele</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid konumlar">
        <div class="d-flex flex-row">
            <?php
            $sehirler = $modul->sehirler();
            if($sehirler===false)
            {

            }else
            {
                foreach($sehirler as $row)
                {
                    ?>
                    <div class="block ">
                        <img src="/<?=$dizin?><?=$row->resim?>" style="width: 100%">
                        <p><a href="<?=$siteURL?>/<?=$urller['kategori-url']?>/<?=$urller['firma-kategori-url']?>/<?=$row->url?>"><?=$row->baslik?></a> </p>
                        <p><a href="<?=$siteURL?>/<?=$urller['kategori-url']?>/<?=$urller['firma-kategori-url']?>/<?=$row->url?>"><?=$modul->sehir_firma($row->id)?> Firma Listele <i class="fa-solid fa-chevron-right fa-fw"></i> </a> </p>
                    </div>
                    <?php
                }
            }
            ?>


        </div>
    </div>

    <div class="container-fluid vitrindeki-mekanlar">
        <div class="container">
            <h3>Vitrindeki Düğün Mekanları</h3>
            <div class="row">
                <?php
                $vitrindeki_firmalar = $modul->vitrindeki_firmalar();
                if($vitrindeki_firmalar===false)
                {

                }else
                {
                    foreach($vitrindeki_firmalar as $row)
                    {
                        $resimler = explode(",",$row->resimler);
                        ?>
                        <div class="col-6 col-md-3">
                            <div class="bg">
                                <img src="/<?=$dizin?><?=$resimler[0]?>" style="width: 100%">
                                <p><a href="<?=$siteURL?>/detail/<?=$urller['firma-detay-url']?>/<?=$row->url?>/<?=$row->id?>"><?=$row->baslik?></a> </p>
                            </div>

                        </div>
                        <?php

                    }
                }
                ?>
            </div>
        </div>
    </div>

    <div class="container-fluid indirimleri-yakala ">
        <h3 class="text-center">Düğün mekanlarında indirimler</h3>
        <p class="text-center">Düğününü planlarken bütçeni de hesaba kat! Firma<br> indirimleri ve kampanyalar
            burada!</p>
        <br><br>

        <div class="container">
            <div class="col-md-12">
                <div class="row">

                    <div class="col-6 col-md-2 item hover15">
                        <figure>
                            <a href=""> <img src="../../images/gelinlikresim.png" style="width: 100%"></a>
                        </figure>
                        <p><span>Marka:</span> Yase Gelinlik</p>
                        <p><span>Model:</span> YS2022</p>
                        <p><span>Özellikler:</span> Klasik,Bohem</p>
                        <p>3.500TL-7.000TL</p>
                        <a class="btn btn-primary" href="#">İncele</a>
                    </div>
                    <div class="col-6 col-md-2 item hover15">
                        <figure>
                            <a href=""> <img src="../../images/gelinlikresim.png" style="width: 100%"></a>
                        </figure>
                        <p><span>Marka:</span> Yase Gelinlik</p>
                        <p><span>Model:</span> YS2022</p>
                        <p><span>Özellikler:</span> Klasik,Bohem</p>
                        <p>3.500TL-7.000TL</p>
                        <a class="btn btn-primary" href="#">İncele</a>
                    </div>
                    <div class="col-6 col-md-2 item hover15">
                        <figure>
                            <a href=""> <img src="../../images/gelinlikresim.png" style="width: 100%"></a>
                        </figure>
                        <p><span>Marka:</span> Yase Gelinlik</p>
                        <p><span>Model:</span> YS2022</p>
                        <p><span>Özellikler:</span> Klasik,Bohem</p>
                        <p>3.500TL-7.000TL</p>
                        <a class="btn btn-primary" href="#">İncele</a>
                    </div>
                    <div class="col-6 col-md-2 item hover15">
                        <figure>
                            <a href=""> <img src="../../images/gelinlikresim.png" style="width: 100%"></a>
                        </figure>
                        <p><span>Marka:</span> Yase Gelinlik</p>
                        <p><span>Model:</span> YS2022</p>
                        <p><span>Özellikler:</span> Klasik,Bohem</p>
                        <p>3.500TL-7.000TL</p>
                        <a class="btn btn-primary" href="#">İncele</a>
                    </div>
                    <div class="col-6 col-md-2 item hover15">
                        <figure>
                            <a href=""> <img src="../../images/gelinlikresim.png" style="width: 100%"></a>
                        </figure>
                        <p><span>Marka:</span> Yase Gelinlik</p>
                        <p><span>Model:</span> YS2022</p>
                        <p><span>Özellikler:</span> Klasik,Bohem</p>
                        <p>3.500TL-7.000TL</p>
                        <a class="btn btn-primary" href="#">İncele</a>
                    </div>
                    <div class="col-6 col-md-2 item hover15">
                        <figure>
                            <a href=""> <img src="../../images/gelinlikresim.png" style="width: 100%"></a>
                        </figure>
                        <p><span>Marka:</span> Yase Gelinlik</p>
                        <p><span>Model:</span> YS2022</p>
                        <p><span>Özellikler:</span> Klasik,Bohem</p>
                        <p>3.500TL-7.000TL</p>
                        <a class="btn btn-primary" href="#">İncele</a>
                    </div>
                    <div class="col-6 col-md-2 item hover15">
                        <figure>
                            <a href=""> <img src="../../images/gelinlikresim.png" style="width: 100%"></a>
                        </figure>
                        <p><span>Marka:</span> Yase Gelinlik</p>
                        <p><span>Model:</span> YS2022</p>
                        <p><span>Özellikler:</span> Klasik,Bohem</p>
                        <p>3.500TL-7.000TL</p>
                        <a class="btn btn-primary" href="#">İncele</a>
                    </div>
                    <div class="col-6 col-md-2 item hover15">
                        <figure>
                            <a href=""> <img src="../../images/gelinlikresim.png" style="width: 100%"></a>
                        </figure>
                        <p><span>Marka:</span> Yase Gelinlik</p>
                        <p><span>Model:</span> YS2022</p>
                        <p><span>Özellikler:</span> Klasik,Bohem</p>
                        <p>3.500TL-7.000TL</p>
                        <a class="btn btn-primary" href="#">İncele</a>
                    </div>
                    <div class="col-6 col-md-2 item hover15">
                        <figure>
                            <a href=""> <img src="../../images/gelinlikresim.png" style="width: 100%"></a>
                        </figure>
                        <p><span>Marka:</span> Yase Gelinlik</p>
                        <p><span>Model:</span> YS2022</p>
                        <p><span>Özellikler:</span> Klasik,Bohem</p>
                        <p>3.500TL-7.000TL</p>
                        <a class="btn btn-primary" href="#">İncele</a>
                    </div>
                    <div class="col-6 col-md-2 item hover15">
                        <figure>
                            <a href=""> <img src="../../images/gelinlikresim.png" style="width: 100%"></a>
                        </figure>
                        <p><span>Marka:</span> Yase Gelinlik</p>
                        <p><span>Model:</span> YS2022</p>
                        <p><span>Özellikler:</span> Klasik,Bohem</p>
                        <p>3.500TL-7.000TL</p>
                        <a class="btn btn-primary" href="#">İncele</a>
                    </div>
                    <div class="col-6 col-md-2 item hover15">
                        <figure>
                            <a href=""> <img src="../../images/gelinlikresim.png" style="width: 100%"></a>
                        </figure>
                        <p><span>Marka:</span> Yase Gelinlik</p>
                        <p><span>Model:</span> YS2022</p>
                        <p><span>Özellikler:</span> Klasik,Bohem</p>
                        <p>3.500TL-7.000TL</p>
                        <a class="btn btn-primary" href="#">İncele</a>
                    </div>
                    <div class="col-6 col-md-2 item hover15">
                        <figure>
                            <a href=""> <img src="../../images/gelinlikresim.png" style="width: 100%"></a>
                        </figure>
                        <p><span>Marka:</span> Yase Gelinlik</p>
                        <p><span>Model:</span> YS2022</p>
                        <p><span>Özellikler:</span> Klasik,Bohem</p>
                        <p>3.500TL-7.000TL</p>
                        <a class="btn btn-primary" href="#">İncele</a>
                    </div>

                </div>
            </div>


        </div>




    </div>

    <div class="container-fluid mekanlari-kesfet">
        <h3>Düğün Mekanını Keşfet</h3>
        <p>Hayalindeki düğünü planlamak için ilham al!</p>
        <div class="row">
            <div class="col-md-3">
                <a href="#">  <img src="../../images/incele3.png" style="width: 100%"></a>
                <p><a href="#">Sıcak ve Samimi Düğün Mekanları <span><i class="fa-regular fa-image fa-fw"></i> 24 Görsel</span></a> </p>
            </div>
            <div class="col-md-3">
                <a href="#"><img src="../../images/incele2.png" style="width: 100%"></a>
                <p><a href="#">Sıcak ve Samimi Düğün Mekanları <span><i class="fa-regular fa-image fa-fw"></i> 24 Görsel</span></a></p>
            </div>
            <div class="col-md-3">
                <a href="#"><img src="../../images/incele4.png" style="width: 100%"></a>
                <p><a href="#">Sıcak ve Samimi Düğün Mekanları <span><i class="fa-regular fa-image fa-fw"></i> 24 Görsel</span></a></p>
            </div>
            <div class="col-md-3">
                <a href="#"><img src="../../images/incele3.png" style="width: 100%"></a>
                <p><a href="#">Sıcak ve Samimi Düğün Mekanları <span><i class="fa-regular fa-image fa-fw"></i> 24 Görsel</span></a></p>
            </div>
            <div class="col-md-3">
                <a href="#"><img src="../../images/incele3.png" style="width: 100%"></a>
                <p><a href="#">Sıcak ve Samimi Düğün Mekanları <span><i class="fa-regular fa-image fa-fw"></i> 24 Görsel</span></a></p>
            </div>
            <div class="col-md-3">
                <a href="#"><img src="../../images/incele2.png" style="width: 100%"></a>
                <p><a href="#">Sıcak ve Samimi Düğün Mekanları <span><i class="fa-regular fa-image fa-fw"></i> 24 Görsel</span></a></p>
            </div>
            <div class="col-md-3">
                <a href="#"><img src="../../images/incele4.png" style="width: 100%"></a>
                <p><a href="#">Sıcak ve Samimi Düğün Mekanları <span><i class="fa-regular fa-image fa-fw"></i> 24 Görsel</span></a></p>
            </div>
            <div class="col-md-3">
                <a href="#"><img src="../../images/incele3.png" style="width: 100%"></a>
                <p><a href="#">Sıcak ve Samimi Düğün Mekanları <span><i class="fa-regular fa-image fa-fw"></i> 24 Görsel</span></a></p>
            </div>
        </div>
    </div>

    <div class="container-fluid cift-yorumlari ciftyorumlarimasaustu">
        <h3>Bu Kategoride Öne Çıkan Çift Yorumları</h3>
        <div class="loop-yorumlar owl-carousel owl-theme">
            <div class="item">
                <div class="row">
                    <div class="col-md-4 resim">
                        <img src="../../images/ciftyorumlari.png">
                    </div>
                    <div class="col-md-8 yorum">
                        <p>Busem & Ömer</p>
                        <p><a href="#">Svadba Maçka İstanbul</a> </p>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, survived not only five centuries,</p>
                        <p>

                            <i class="fa-solid fa-star fa-fw"></i>
                            <i class="fa-solid fa-star fa-fw"></i>
                            <i class="fa-solid fa-star fa-fw"></i>
                            <i class="fa-solid fa-star fa-fw"></i>
                            <i class="fa-regular fa-star fa-fw"></i>

                        </p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="row">
                    <div class="col-md-4 resim">
                        <img src="../../images/ciftyorumlari.png">
                    </div>
                    <div class="col-md-8 yorum">
                        <p>Busem & Ömer</p>
                        <p><a href="#">Svadba Maçka İstanbul</a> </p>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, survived not only five centuries,</p>
                        <p>

                            <i class="fa-solid fa-star fa-fw"></i>
                            <i class="fa-solid fa-star fa-fw"></i>
                            <i class="fa-solid fa-star fa-fw"></i>
                            <i class="fa-solid fa-star fa-fw"></i>
                            <i class="fa-regular fa-star fa-fw"></i>

                        </p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="row">
                    <div class="col-md-4 resim">
                        <img src="../../images/ciftyorumlari.png">
                    </div>
                    <div class="col-md-8 yorum">
                        <p>Busem & Ömer</p>
                        <p><a href="#">Svadba Maçka İstanbul</a> </p>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, survived not only five centuries,</p>
                        <p>

                            <i class="fa-solid fa-star fa-fw"></i>
                            <i class="fa-solid fa-star fa-fw"></i>
                            <i class="fa-solid fa-star fa-fw"></i>
                            <i class="fa-solid fa-star fa-fw"></i>
                            <i class="fa-regular fa-star fa-fw"></i>

                        </p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="row">
                    <div class="col-md-4 resim">
                        <img src="../../images/ciftyorumlari.png">
                    </div>
                    <div class="col-md-8 yorum">
                        <p>Busem & Ömer</p>
                        <p><a href="#">Svadba Maçka İstanbul</a> </p>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, survived not only five centuries,</p>
                        <p>

                            <i class="fa-solid fa-star fa-fw"></i>
                            <i class="fa-solid fa-star fa-fw"></i>
                            <i class="fa-solid fa-star fa-fw"></i>
                            <i class="fa-solid fa-star fa-fw"></i>
                            <i class="fa-regular fa-star fa-fw"></i>

                        </p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="row">
                    <div class="col-md-4 resim">
                        <img src="../../images/ciftyorumlari.png">
                    </div>
                    <div class="col-md-8 yorum">
                        <p>Busem & Ömer</p>
                        <p><a href="#">Svadba Maçka İstanbul</a> </p>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, survived not only five centuries,</p>
                        <p>

                            <i class="fa-solid fa-star fa-fw"></i>
                            <i class="fa-solid fa-star fa-fw"></i>
                            <i class="fa-solid fa-star fa-fw"></i>
                            <i class="fa-solid fa-star fa-fw"></i>
                            <i class="fa-regular fa-star fa-fw"></i>

                        </p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="row">
                    <div class="col-md-4 resim">
                        <img src="../../images/ciftyorumlari.png">
                    </div>
                    <div class="col-md-8 yorum">
                        <p>Busem & Ömer</p>
                        <p><a href="#">Svadba Maçka İstanbul</a> </p>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, survived not only five centuries,</p>
                        <p>

                            <i class="fa-solid fa-star fa-fw"></i>
                            <i class="fa-solid fa-star fa-fw"></i>
                            <i class="fa-solid fa-star fa-fw"></i>
                            <i class="fa-solid fa-star fa-fw"></i>
                            <i class="fa-regular fa-star fa-fw"></i>

                        </p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="row">
                    <div class="col-md-4 resim">
                        <img src="../../images/ciftyorumlari.png">
                    </div>
                    <div class="col-md-8 yorum">
                        <p>Busem & Ömer</p>
                        <p><a href="#">Svadba Maçka İstanbul</a> </p>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, survived not only five centuries,</p>
                        <p>

                            <i class="fa-solid fa-star fa-fw"></i>
                            <i class="fa-solid fa-star fa-fw"></i>
                            <i class="fa-solid fa-star fa-fw"></i>
                            <i class="fa-solid fa-star fa-fw"></i>
                            <i class="fa-regular fa-star fa-fw"></i>

                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid cift-yorumlari ciftyorumlarimobil">
        <h3>Bu Kategoride Öne Çıkan Çift Yorumları</h3>
        <div class="loop-yorumlar-mobil owl-carousel owl-theme">
            <div class="item">
                <div class="row">
                    <div class="col-md-4 resim">
                        <img src="../../images/ciftyorumlari.png">
                    </div>
                    <div class="col-md-8 yorum">
                        <p>Busem & Ömer</p>
                        <p><a href="#">Svadba Maçka İstanbul</a> </p>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, survived not only five centuries,</p>
                        <p>

                            <i class="fa-solid fa-star fa-fw"></i>
                            <i class="fa-solid fa-star fa-fw"></i>
                            <i class="fa-solid fa-star fa-fw"></i>
                            <i class="fa-solid fa-star fa-fw"></i>
                            <i class="fa-regular fa-star fa-fw"></i>

                        </p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="row">
                    <div class="col-md-4 resim">
                        <img src="../../images/ciftyorumlari.png">
                    </div>
                    <div class="col-md-8 yorum">
                        <p>Busem & Ömer</p>
                        <p><a href="#">Svadba Maçka İstanbul</a> </p>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, survived not only five centuries,</p>
                        <p>

                            <i class="fa-solid fa-star fa-fw"></i>
                            <i class="fa-solid fa-star fa-fw"></i>
                            <i class="fa-solid fa-star fa-fw"></i>
                            <i class="fa-solid fa-star fa-fw"></i>
                            <i class="fa-regular fa-star fa-fw"></i>

                        </p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="row">
                    <div class="col-md-4 resim">
                        <img src="../../images/ciftyorumlari.png">
                    </div>
                    <div class="col-md-8 yorum">
                        <p>Busem & Ömer</p>
                        <p><a href="#">Svadba Maçka İstanbul</a> </p>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, survived not only five centuries,</p>
                        <p>

                            <i class="fa-solid fa-star fa-fw"></i>
                            <i class="fa-solid fa-star fa-fw"></i>
                            <i class="fa-solid fa-star fa-fw"></i>
                            <i class="fa-solid fa-star fa-fw"></i>
                            <i class="fa-regular fa-star fa-fw"></i>

                        </p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="row">
                    <div class="col-md-4 resim">
                        <img src="../../images/ciftyorumlari.png">
                    </div>
                    <div class="col-md-8 yorum">
                        <p>Busem & Ömer</p>
                        <p><a href="#">Svadba Maçka İstanbul</a> </p>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, survived not only five centuries,</p>
                        <p>

                            <i class="fa-solid fa-star fa-fw"></i>
                            <i class="fa-solid fa-star fa-fw"></i>
                            <i class="fa-solid fa-star fa-fw"></i>
                            <i class="fa-solid fa-star fa-fw"></i>
                            <i class="fa-regular fa-star fa-fw"></i>

                        </p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="row">
                    <div class="col-md-4 resim">
                        <img src="../../images/ciftyorumlari.png">
                    </div>
                    <div class="col-md-8 yorum">
                        <p>Busem & Ömer</p>
                        <p><a href="#">Svadba Maçka İstanbul</a> </p>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, survived not only five centuries,</p>
                        <p>

                            <i class="fa-solid fa-star fa-fw"></i>
                            <i class="fa-solid fa-star fa-fw"></i>
                            <i class="fa-solid fa-star fa-fw"></i>
                            <i class="fa-solid fa-star fa-fw"></i>
                            <i class="fa-regular fa-star fa-fw"></i>

                        </p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="row">
                    <div class="col-md-4 resim">
                        <img src="../../images/ciftyorumlari.png">
                    </div>
                    <div class="col-md-8 yorum">
                        <p>Busem & Ömer</p>
                        <p><a href="#">Svadba Maçka İstanbul</a> </p>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, survived not only five centuries,</p>
                        <p>

                            <i class="fa-solid fa-star fa-fw"></i>
                            <i class="fa-solid fa-star fa-fw"></i>
                            <i class="fa-solid fa-star fa-fw"></i>
                            <i class="fa-solid fa-star fa-fw"></i>
                            <i class="fa-regular fa-star fa-fw"></i>

                        </p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="row">
                    <div class="col-md-4 resim">
                        <img src="../../images/ciftyorumlari.png">
                    </div>
                    <div class="col-md-8 yorum">
                        <p>Busem & Ömer</p>
                        <p><a href="#">Svadba Maçka İstanbul</a> </p>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, survived not only five centuries,</p>
                        <p>

                            <i class="fa-solid fa-star fa-fw"></i>
                            <i class="fa-solid fa-star fa-fw"></i>
                            <i class="fa-solid fa-star fa-fw"></i>
                            <i class="fa-solid fa-star fa-fw"></i>
                            <i class="fa-regular fa-star fa-fw"></i>

                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid sik-soru">
        <div class="container">
            <h3>Düğün Mekanları - Sıkça Sorulan Sorular</h3>
            <p>Müşterilerimiz tarafından en çok merak edilen soruları sizler için cevapladık.</p>
            <div class="accordion" id="sikca-sorulan-sorular">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="heading-soru-1">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#soru-1" aria-expanded="true" aria-controls="soru-1">
                                        En popüler düğün mekanı türleri nelerdir?

                                    </button>
                                </h2>
                                <div id="soru-1" class="accordion-collapse collapse" aria-labelledby="heading-soru-1" data-bs-parent="#sikca-sorulan-sorular">
                                    <div class="accordion-body">
                                        It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="heading-soru-2">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#soru-2" aria-expanded="true" aria-controls="soru-2">
                                        Düğün mekanları fiyatları ne kadar?

                                    </button>
                                </h2>
                                <div id="soru-2" class="accordion-collapse collapse" aria-labelledby="heading-soru-2" data-bs-parent="#sikca-sorulan-sorular">
                                    <div class="accordion-body">
                                        <strong>This is the first item's accordion body.</strong> It is shown by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables. It's also worth noting that just about any HTML can go within the <code>.accordion-body</code>, though the transition does limit overflow.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="heading-soru-3">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#soru-3" aria-expanded="true" aria-controls="soru-3">
                                        En popüler düğün mekanı türleri nelerdir?


                                    </button>
                                </h2>
                                <div id="soru-3" class="accordion-collapse collapse " aria-labelledby="heading-soru-3" data-bs-parent="#sikca-sorulan-sorular">
                                    <div class="accordion-body">
                                        <strong>This is the first item's accordion body.</strong> It is shown by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables. It's also worth noting that just about any HTML can go within the <code>.accordion-body</code>, though the transition does limit overflow.
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="heading-soru-3">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#soru-3" aria-expanded="true" aria-controls="soru-3">
                                        En popüler düğün mekanı türleri nelerdir?


                                    </button>
                                </h2>
                                <div id="soru-3" class="accordion-collapse collapse " aria-labelledby="heading-soru-3" data-bs-parent="#sikca-sorulan-sorular">
                                    <div class="accordion-body">
                                        <strong>This is the first item's accordion body.</strong> It is shown by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables. It's also worth noting that just about any HTML can go within the <code>.accordion-body</code>, though the transition does limit overflow.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
include "../../inc/footer.php";
?>
