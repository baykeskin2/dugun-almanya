<?php
include "../../inc/header.php";
?>
<section class="content ajandam-cont">
    <div class="container-fluid ajandambaslik">
        <p>Bütçem</p>
        <p><strong>0.00TL</strong> bütçeden <strong>0.00TL</strong>  harcandı.</p>
        <p>
            <i data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Tooltip on top" class="fa-solid fa-circle-question fa-fw"></i>
        </p>
    </div>

    <div class="container">
        <div class="col-md-12 block text-center filtreler">
            <p>
                <a class="active" href="#"><i class="fa-regular fa-circle fa-fw"></i>Tüm Kategoriler</a>
                <a href="#"><i class="fa-solid fa-circle fa-fw"></i>Harcama yapılanlar</a>
                <a href="#"><i class="fa-solid fa-circle fa-fw"></i> Harcama yapılmayanlar</a>
                <a href="#"><i class="fa-solid fa-circle fa-fw"></i> Bütçe belirlenmeyenleri gösterme</a>
            </p>
        </div>

        <div class="col-md-12 block yeni-gorev ">
            <a href="#"><i class="fa-solid fa-plus fa-fw"></i> Harcama Ekle </a>
        </div>


        <div class="col-md-12 block gorevler ajanda ">
            <div class="row">
                <div class="col-md-12">
                    <p><a href="#">Düğün Mekanları</a> </p>
                    <p><a href="#">Öngörülen ortalama bütçe 40.000.00TL</a> </p>
                    <p><a href="#">Harcanan 0TL</a> </p>
                </div>
            </div>
        </div>

        <div class="col-md-12 block gorevler ajanda ">
            <div class="row">
                <div class="col-md-12">
                    <p><a href="#">Düğün Mekanları</a> </p>
                    <p><a href="#">Öngörülen ortalama bütçe 40.000.00TL</a> </p>
                    <p><a href="#">Harcanan 0TL</a> </p>
                </div>
            </div>
        </div>

        <div class="col-md-12 block gorevler ajanda ">
            <div class="row">
                <div class="col-md-12">
                    <p><a href="#">Düğün Mekanları</a> </p>
                    <p><a href="#">Öngörülen ortalama bütçe 40.000.00TL</a> </p>
                    <p><a href="#">Harcanan 0TL</a> </p>
                </div>
            </div>
        </div>

        <div class="col-md-12 block gorevler ajanda ">
            <div class="row">
                <div class="col-md-12">
                    <p><a href="#">Düğün Mekanları</a> </p>
                    <p><a href="#">Öngörülen ortalama bütçe 40.000.00TL</a> </p>
                    <p><a href="#">Harcanan 0TL</a> </p>
                </div>
            </div>
        </div>
    </div>


</section>
<?php
include "../../inc/footer.php";
?>
<script>
    var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
    var tooltipList = tooltipTriggerList.map(function(tooltipTriggerEl)
    {
        return new bootstrap.Tooltip(tooltipTriggerEl)
    })
</script>