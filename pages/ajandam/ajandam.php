<?php
include "../../inc/header.php";
?>
<section class="content ajandam-cont">
    <div class="container-fluid ajandambaslik">
        <p>Ajandam</p>
        <p>5 görevden 0 tanesi tamamlandı!</p>
        <p>
            <i data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Tooltip on top" class="fa-solid fa-circle-question fa-fw"></i>
        </p>
    </div>

    <div class="container">
        <div class="col-md-12 block text-center filtreler">
            <p>
                <a class="active" href="#"><i class="fa-regular fa-circle fa-fw"></i>Tüm Görevler</a>
                <a href="#"><i class="fa-solid fa-circle fa-fw"></i>Tamamlananlar</a>
                <a href="#"><i class="fa-solid fa-circle fa-fw"></i> Tamamlanmayanlar</a>
            </p>
            <div class="col-md-12 d-flex justify-content-center">
                <div class="dropdown" style="float: left">
                    <button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Kategoriye Göre Filtrele
                    </button>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="#">Action</a></li>
                        <li><a class="dropdown-item" href="#">Another action</a></li>
                        <li><a class="dropdown-item" href="#">Something else here</a></li>
                    </ul>
                </div>
                <div class="dropdown" style="float: left">
                    <button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Zaman Aralığına Göre Filtrele
                    </button>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="#">Action</a></li>
                        <li><a class="dropdown-item" href="#">Another action</a></li>
                        <li><a class="dropdown-item" href="#">Something else here</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="col-md-12 block yeni-gorev ">

            <a href="#"><i class="fa-solid fa-plus fa-fw"></i> Yeni Görev </a>
        </div>




        <div class="col-md-12 block gorevler ">

            <div class="row">
                <div class="col-md-5 kategori d-flex flex-column  justify-content-center align-items-left">
                    <span>Mekan</span>
                </div>
                <div class="col-md-7">
                    <p>
                        <a href="#"><i class="fa-regular fa-circle fa-fw"></i> Düğün günü akışını incele  <span><i class="fa-solid fa-chevron-right fa-fw"></i> </span></a>
                    </p>
                    <p>
                        <a href="#"><i class="fa-regular fa-circle fa-fw"></i> Düğün günü akışını incele <span><i class="fa-solid fa-chevron-right fa-fw"></i> </span> </a>
                    </p>
                    <p>
                        <a href="#"><i class="fa-regular fa-circle fa-fw"></i> Düğün günü akışını incele <span><i class="fa-solid fa-chevron-right fa-fw"></i> </span> </a>
                    </p>
                </div>
            </div>
        </div>

        <div class="col-md-12 block gorevler ">

            <div class="row">
                <div class="col-md-5 kategori d-flex flex-column  justify-content-center align-items-left">
                    <span>Bunları Unutma</span>
                </div>
                <div class="col-md-7">
                    <p>
                        <a href="#"><i class="fa-regular fa-circle fa-fw"></i> Düğün günü akışını incele  <span><i class="fa-solid fa-chevron-right fa-fw"></i> </span></a>
                    </p>
                    <p>
                        <a href="#"><i class="fa-regular fa-circle fa-fw"></i> Düğün günü akışını incele <span><i class="fa-solid fa-chevron-right fa-fw"></i> </span> </a>
                    </p>
                    <p>
                        <a href="#"><i class="fa-regular fa-circle fa-fw"></i> Düğün günü akışını incele <span><i class="fa-solid fa-chevron-right fa-fw"></i> </span> </a>
                    </p>
                </div>
            </div>
        </div>

        <div class="col-md-12 block gorevler ">

            <div class="row">
                <div class="col-md-5 kategori d-flex flex-column  justify-content-center align-items-left">
                    <span>Gelinlik</span>
                </div>
                <div class="col-md-7">
                    <p>
                        <a href="#"><i class="fa-regular fa-circle fa-fw"></i> Düğün günü akışını incele  <span><i class="fa-solid fa-chevron-right fa-fw"></i> </span></a>
                    </p>
                    <p>
                        <a href="#"><i class="fa-regular fa-circle fa-fw"></i> Düğün günü akışını incele <span><i class="fa-solid fa-chevron-right fa-fw"></i> </span> </a>
                    </p>
                    <p>
                        <a href="#"><i class="fa-regular fa-circle fa-fw"></i> Düğün günü akışını incele <span><i class="fa-solid fa-chevron-right fa-fw"></i> </span> </a>
                    </p>
                </div>
            </div>
        </div>

        <div class="col-md-12 block gorevler ">

            <div class="row">
                <div class="col-md-5 kategori d-flex flex-column  justify-content-center align-items-left">
                    <span>Saç ve Makyaj</span>
                </div>
                <div class="col-md-7">
                    <p>
                        <a href="#"><i class="fa-regular fa-circle fa-fw"></i> Düğün günü akışını incele  <span><i class="fa-solid fa-chevron-right fa-fw"></i> </span></a>
                    </p>
                    <p>
                        <a href="#"><i class="fa-regular fa-circle fa-fw"></i> Düğün günü akışını incele <span><i class="fa-solid fa-chevron-right fa-fw"></i> </span> </a>
                    </p>
                    <p>
                        <a href="#"><i class="fa-regular fa-circle fa-fw"></i> Düğün günü akışını incele <span><i class="fa-solid fa-chevron-right fa-fw"></i> </span> </a>
                    </p>
                </div>
            </div>
        </div>
    </div>


</section>
<?php
include "../../inc/footer.php";
?>
<script>
    var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
    var tooltipList = tooltipTriggerList.map(function(tooltipTriggerEl)
    {
        return new bootstrap.Tooltip(tooltipTriggerEl)
    })
</script>